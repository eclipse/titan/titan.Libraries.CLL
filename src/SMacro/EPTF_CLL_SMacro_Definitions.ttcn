///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_SMacro_Definitions
// 
//  Purpose:
//    This module contains type definitions of the SMacro feature.
//
//    The SMacro feature provides functions to:
//      - (un)define a string-macro
//      - (de)register string-macro calculator function
//      - resolve a string that contains string-macros
//      - provide built-in string-macros, e.g. to evaluate mathematical expressions like 1*1
// 
//  Module Parameters:
//    -
//
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_HashMap_Definitions>
//    <EPTF_CLL_FBQ_Definitions>
//    <EPTF_CLL_Logging_Definitions>
// 
//  Current Owner:
//    Jozsef Gyurusi (ETHJGI)
// 
//  Last Review Date:
//    2007-12-06
//
//  Detailed Comments:
//    -
//
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_SMacro_Definitions {

import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_HashMap_Definitions all;
import from EPTF_CLL_FBQ_Definitions all;
import from EPTF_CLL_Logging_Definitions all;

modulepar boolean tsp_debug_EPTF_SMacro_Functions := false;

///////////////////////////////////////////////////////////
//  Type: f_EPTF_CLL_SMacro_calc_FT
// 
//  Purpose:
//    The type of the string-macro calculator function
// 
//  Elements:
//    pl_macroArgs - *in* <EPTF_CharstringList> - will contain the parameters of the macro
//    pl_userArgs - *in* <EPTF_IntegerList> - will contain the parameters specified by the user
//
//  Detailed Comments:
//    This type of function can be used as a string-macro calculator function
//    and can be registered by <f_EPTF_SMacro_registerCalcFn>.
//    This function is called when the value of the string-macro assigned
//    this function should be calculated.
//    For example:
//    The value of $(SMACRO,"a","b") will be calculated by as the return value of
//    the function call f_EPTF_CLL_SMacro_calc_FT({"a","b"}).
//
///////////////////////////////////////////////////////////
type function f_EPTF_CLL_SMacro_calc_FT(in EPTF_CharstringList pl_macroArgs, in EPTF_IntegerList pl_userArgs := {}) runs on self return charstring;

///////////////////////////////////////////////////////////
// Type: EPTF_SMacro_Definition
//
// Purpose:
//   Record for storing SMacro.
//
// Elements:
//   macro_name - *charstring* - name of the SMacro
//   macro_value - *charstring* - value of the SMacro
// 
// Detailed Comments:
//   -
//
///////////////////////////////////////////////////////////
type record EPTF_SMacro_Definition {
  charstring macro_name,
  charstring macro_value
}

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_Definition_init
// 
//  Purpose:
//    default value for SMacro definition
// 
//  Detailed Comments:
//    it is used to initialize
///////////////////////////////////////////////////////////
const EPTF_SMacro_Definition c_EPTF_SMacro_Definition_init := {
  macro_name := "",
  macro_value := ""
};

///////////////////////////////////////////////////////////
//  Type: EPTF_SMacro_Definitions
// 
//  Purpose:
//    List of <EPTF_SMacro_Definition> record
///////////////////////////////////////////////////////////
type record of EPTF_SMacro_Definition EPTF_SMacro_Definitions;

///////////////////////////////////////////////////////////
// Type: EPTF_SMacro_CalcFunction
//
// Purpose:
//   Record for storing SMacro calculator functions.
//
// Elements:
//   functionName - *charstring* - name of the SMacro calculator function
//   funcRef - <f_EPTF_CLL_SMacro_calc_FT> - reference of SMacro calculator function
//   userArgs - <EPTF_IntegerList> - user given parameters
// 
// Detailed Comments:
//   -
//
///////////////////////////////////////////////////////////
type record EPTF_SMacro_CalcFunction {
  charstring functionName,
  f_EPTF_CLL_SMacro_calc_FT funcRef,
  EPTF_IntegerList userArgs
}

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_CalcFunction_init
// 
//  Purpose:
//    default value for SMacro calculator function
// 
//  Detailed Comments:
//    it is used to initialize
///////////////////////////////////////////////////////////
const EPTF_SMacro_CalcFunction c_EPTF_SMacro_CalcFunction_init := {
  functionName := "",
  funcRef := null,
  userArgs := {}
};

///////////////////////////////////////////////////////////
//  Type: EPTF_SMacro_CalcFunctions
// 
//  Purpose:
//    List of <EPTF_SMacro_CalcFunction> record
///////////////////////////////////////////////////////////
type record of EPTF_SMacro_CalcFunction EPTF_SMacro_CalcFunctions;


///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_macroNameRegexp
// 
//  Purpose:
//    Regular expression to check if the name of the macro is valid
// 
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
const charstring c_EPTF_SMacro_macroNameRegexp := "([a-zA-Z0-9_]#(1,))" // allowed macro name: abcdABCD_0123

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_macroParameterRegexp
// 
//  Purpose:
//    Regular expression to check if the value of a macro parameter is valid
// 
//  Detailed Comments:
//
//   Regexp: matches a string which is enclosed in " signs. The string should match the pattern:
//
//    ([^\"\\]#(0,)((\\(\\\\)#(0,)\")|((\\)#(0,)[^\"]))#(0,))#(0,)(\\\\)#(0,)
//
//    This is the actual value of the regexp before converted to TTCN3 charstring
//    (string contains this value, it means that special chars are not escaped).
//    The value of c_EPTF_SMacro_macroParameterRegexp is the escaped version between the " signs.
//
//   This regexp does the following:
//   The string that matches the regexp should contain any number of 'Pattern A' followed by
//   even number of \ chars and a " sign:  "(Pattern A)#(0,)(\\\\)#(0,)"
//     Example:
//      "'Pattern A''PatternA'\\\\"
//      "assaasas\\\"sfaafffd\\\\\\a\\\\"
//
//   Pattern A:
//     Starts with ([^\"\\]#(0,) which eats all chars that are not " sign or \ char.
//     Then it should be followed by any number of these patterns:
//        1) (\\(\\\\)#(0,)\")
//           odd number of \ chars followed by " sign
//           example:  assaasas\\\"
//       OR
//
//        2) (\\)#(0,)[^\"])
//           any number of \ chars followed by a char which is not \ char or " sign.
//           Example: sfaafffd\\\\\\a
//
///////////////////////////////////////////////////////////
const charstring c_EPTF_SMacro_macroParameterRegexp := "\"([^\\\"\\\\]#(0,)((\\\\(\\\\\\\\)#(0,)\\\")|((\\\\)#(0,)[^\\\"]))#(0,))#(0,)(\\\\\\\\)#(0,)\""; // allowed macro parameter : "paramValue"
//const charstring c_EPTF_SMacro_macroParameterRegexp := "\"([^\\\"]#(0, ))\""; // allowed macro parameter : "paramValue"

const charstring c_EPTF_SMacro_everything := "(*)" // everything
 const charstring c_EPTF_SMacro_whitespace := "[ \t]#(,)" // whitespace




//---------------   MACROS  -----------------------------------------------

////////////////////////////////////////////////////
//          Built-in Macros
///////////////////////////////////////////////////


const charstring c_EPTF_SMacro_macroName_EVAL := "EVAL";


const charstring c_EPTF_SMacro_integerNumber := "([+-]#(0,1)[0-9]#(1,))"; // integer number: "01230102", "+1122", "-0121"
const charstring c_EPTF_SMacro_floatNumber := "([+-]#(0,1)[0-9]#(1,)[.]#(0,1)[0-9]#(0,)([eE][-+]#(0,1)[0-9]#(1,))#(0,1))"; // float number: "12.354", "-1.23", "+1.254", "-1e-2", 2.32e+4 Contains 2 groups!!! The first is the float number itself, the second is the exponent


// supported operators ordered by precedence
const charstring c_EPTF_SMacro_paranthesesOp := "(";

const charstring c_EPTF_SMacro_multiplicationOp := "*";
const charstring c_EPTF_SMacro_divisionOp := "/";
const charstring c_EPTF_SMacro_remainderOp := "%";

const charstring c_EPTF_SMacro_additionOp := "+";
const charstring c_EPTF_SMacro_subtractionOp := "-";


const charstring c_EPTF_SMacro_multiplicationOps := c_EPTF_SMacro_multiplicationOp&c_EPTF_SMacro_divisionOp&c_EPTF_SMacro_remainderOp;
const charstring c_EPTF_SMacro_additionOps := c_EPTF_SMacro_additionOp&c_EPTF_SMacro_subtractionOp;


// regexp-s for single expression
// multiplication: 'floatNumber'[*/%]'floatNumber'
const charstring c_EPTF_SMacro_multiplicationExpr := c_EPTF_SMacro_floatNumber&"([*/%])"&c_EPTF_SMacro_floatNumber;

// addition: 'floatNumber'[+-]'floatNumber'
const charstring c_EPTF_SMacro_additionExpr := c_EPTF_SMacro_floatNumber&"([+-])"&c_EPTF_SMacro_floatNumber;





//---------------   LOGGING  --------------------------------------------


///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_loggingComponentMask
// 
//  Purpose:
//    logging component name for SMacro
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////
const charstring c_EPTF_SMacro_loggingComponentMask := "EPTF_SMacro";

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used in SMacro
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
///////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_SMacro_loggingEventClasses := { "Warning", "Debug" };

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_SMacro_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Warning
// 
//  Detailed Comments:
//    Its value is *0*
///////////////////////////////////////////////////////////
const integer c_EPTF_SMacro_loggingClassIdx_Warning := 0;

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_ExecCtrl_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Debug
// 
//  Detailed Comments:
//    Its value is *1*
///////////////////////////////////////////////////////////
const integer c_EPTF_SMacro_loggingClassIdx_Debug := 1;

///////////////////////////////////////////////////////////
//  Component: EPTF_SMacro_CT
// 
//  Purpose:
//    The component that implements the EPTF SMacro feature
// 
//  Elements:
//    v_EPTF_SMacro_initialized - *boolean* - prevents multiple init by calling <f_EPTF_SMacro_init_CT> several times
//    v_EPTF_SMacro_definitions - <EPTF_SMacro_Definitions> - stores the defined SMacros
//    v_SMacro_definitionStr2IntHashMapId - *integer* - hashmap to store SMacro indexes
//    v_SMacro_definitionFBQId - *integer* - free busy queue to maintain the macro definition DB efficiently
//    v_EPTF_SMacro_calcFunctions - <EPTF_SMacro_CalcFunctions> - stores the registered SMacro calculator functions
//    v_SMacro_calcFuntionStr2IntHashMapId - *integer* - hashmap to store SMacro calculator function indexes
//    v_SMacro_calcFuntionFBQId - *integer* - free busy queue to maintain the calculator function DB efficiently
//    v_SMacro_loggingMaskId - *integer* - logging component mask ID
//
//  Detailed Comments:
//   To use the any EPTF SMacro Feature, extend your component with <EPTF_SMacro_CT>
//
///////////////////////////////////////////////////////////
public type component EPTF_SMacro_CT extends EPTF_Base_CT, EPTF_HashMap_CT, EPTF_FBQ_CT, EPTF_Logging_CT {
  private var boolean v_EPTF_SMacro_initialized := false;
  
  private var EPTF_SMacro_Definitions v_EPTF_SMacro_definitions := {};
  private var integer v_SMacro_definitionStr2IntHashMapId := -1;
  private var integer v_SMacro_definitionFBQId := -1;

  private var EPTF_SMacro_CalcFunctions v_EPTF_SMacro_calcFunctions := {};
  private var integer v_SMacro_calcFuntionStr2IntHashMapId := -1;
  private var integer v_SMacro_calcFuntionFBQId := -1;
  
  private var integer v_SMacro_loggingMaskId := c_EPTF_Logging_invalidMaskId;
}

} // end of module
