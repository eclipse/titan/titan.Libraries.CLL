///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_CLL_CSLB_Functions
//
// Purpose:
//   This TTCN-3 module contains function definitions for EPTF
//   central scheduling load balancing feature
//
// Module Parameters:
//    -
//
//  Module depends on:
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_FBQ_Functions>
//    <EPTF_CLL_Variable_Functions>
//    <EPTF_CLL_CS_Definitions>
//    <EPTF_CLL_HostAdmin_Definitions>
//    <EPTF_CLL_HostAdmin_Functions>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    2008-02-09
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////

module EPTF_CLL_CSLB_Functions
{

import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Functions all;
import from EPTF_CLL_FBQ_Functions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_CS_Definitions all;
import from EPTF_CLL_HostAdmin_Definitions all;
import from EPTF_CLL_HostAdmin_Functions all;

modulepar boolean tsp_debug_EPTF_CLL_CSLB_Functions := false;

friend module EPTF_CLL_CSAdmin_Functions;
friend module EPTF_CLL_CSUIHandler_Functions;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_LB_selectLoadGen
// 
//  Purpose:
//    Selects a the loadgen with the lowest load from the free LGen pool. If successfull,
//    composed trigger message, sets the status of the selected LGen
//    it returns the slaveIndex, otherwise it returns -1.
//
//  Parameters:
//    -
// 
//  Return Value:
//    *integer* - index in v_EPTF_CS_db_LGens.loadgens list of the LGen with lowest load
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
friend function f_EPTF_LB_selectLoadGen()
runs on EPTF_CS_ApplAdminBase_CT
return integer
{
  // go though the free queue of slaves
  var float load_min := -1.0;
  var float load;
  var integer i_min := -1;
  var integer slaveIdx := f_EPTF_FBQ_getFreeSlot(v_EPTF_CS_db_LGens.queue); // < EPTF_CS_ApplAdminBase_CT
  if (slaveIdx==-1) {
    // no free slots
    f_EPTF_Common_warning("f_EPTF_LB_selectLoadGen: WARNING: No free LGens found");
    return -1;
  }
//  log("slaveIdx: ", slaveIdx);
//  log("f_EPTF_FBQ_getLengthOfFreeChain(v_EPTF_CS_db_LGens.queue): ", f_EPTF_FBQ_getLengthOfFreeChain(v_EPTF_CS_db_LGens.queue));
  // f_EPTF_FBQ_getLengthOfFreeChain(vc_EPTF_CS_LGens)
  var boolean vl_fwdIsValid := true;
  for (var integer i:=0; i<f_EPTF_FBQ_getLengthOfFreeChain(v_EPTF_CS_db_LGens.queue) and vl_fwdIsValid; i:=i+1) {
    // find lowest load level for i
      if(v_EPTF_CS_db_LGens.loadgens[i].hostIdx != -1) {

        load := f_EPTF_Var_getFloatValue(v_loadLevel_KeyList[v_EPTF_CS_db_LGens.loadgens[i].hostIdx]);
     
    //    log("i: ",i," slaveIdx: ",slaveIdx," load: ", load);
    //    log("v_EPTF_slaves[slaveIdx]: ", v_EPTF_slaves[slaveIdx]);
        if (load_min<0.0) {
          load_min := load;
          i_min := slaveIdx;
        }
        if (load<load_min) {
          load_min := load;
          i_min := slaveIdx;
        }
        vl_fwdIsValid := f_EPTF_FBQ_getFwdFreeItemIdx(slaveIdx, v_EPTF_CS_db_LGens.queue);     
      }
  }
  if(tsp_debug_EPTF_CLL_CSLB_Functions and slaveIdx >= 0) {
    f_EPTF_Common_user(log2str(%definitionId& ": selected lgenId: ", i_min, " host: ",tsp_EPTF_CS_LGenHostnameList[v_EPTF_CS_db_LGens.loadgens[slaveIdx].hostIdx]," min load: ", load_min));
  }
  return i_min;
}


///////////////////////////////////////////////////////////
//  Function: f_EPTF_LB_init
// 
//  Purpose:
//    Initializes the loadBalancing feature.
//
//  Parameters:
//    - pl_selfName - *in* *integer* - EPTF self-name
// 
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
friend function f_EPTF_LB_init(in charstring pl_selfName)
runs on EPTF_CS_ApplAdminBase_CT
{
  if(v_EPTF_CS_loadBalancingInitialized) { return; }

  f_EPTF_Var_init_CT(pl_selfName);

  for (var integer i:=0; i<sizeof(tsp_EPTF_CS_LGenHostnameList); i:=i+1) {
    var charstring vl_hostAdminName := f_EPTF_Base_selfName() & ".HostAdmin." & int2str(i);
    v_hostAdmins[i] := EPTF_HostAdmin_CT.create(-, tsp_EPTF_CS_LGenHostnameList[i]);
    v_hostAdmins[i].start(f_EPTF_HostAdmin_behavior(vl_hostAdminName));
    f_EPTF_LB_subscribeToHostAdminLoadVar(i, vl_hostAdminName, v_hostAdmins[i]);
  }

  v_EPTF_CS_loadBalancingInitialized := true;
  f_EPTF_Base_registerCleanup(refers(f_EPTF_CS_LB_cleanup));
}

friend function f_EPTF_LB_getInitialized()
runs on EPTF_CS_ApplAdminBase_CT
return boolean
{
  return v_EPTF_CS_loadBalancingInitialized;
}

friend function f_EPTF_LB_setInitialized(in boolean pl_initialized)
runs on EPTF_CS_ApplAdminBase_CT
{
  v_EPTF_CS_loadBalancingInitialized := pl_initialized;
}

friend function f_EPTF_LB_subscribeToHostAdminLoadVar(
  in integer pl_hostAdminIdx,
  in charstring pl_hostAdminName,
  in EPTF_HostAdmin_CT pl_hostAdmin)
runs on EPTF_CS_ApplAdminBase_CT
{
  v_loadLevel_KeyList[pl_hostAdminIdx]:= -1;
  f_EPTF_Var_subscribeRemote(
    pl_hostAdmin,
    pl_hostAdminName & "." & c_EPTF_HostAdmin_testerHostLoad_name,
    realtime,
    v_loadLevel_KeyList[pl_hostAdminIdx]);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_CS_LB_cleanup
// 
//  Purpose:
//    Cleanup function for loadBalancing feature.
//
//  Parameters:
//    -
// 
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_CS_LB_cleanup() runs on EPTF_CS_ApplAdminBase_CT
{
// Note: no need to stop host admins explicitly.
/*  for(var integer i := 0; i < sizeof(v_hostAdmins); i := i + 1) {
    f_EPTF_Var_unsubscribe(v_loadLevel_KeyList[i]);
    f_EPTF_Base_stopRemote(v_hostAdmins[i]);
    v_hostAdmins[i].done;
  }*/
  v_EPTF_CS_loadBalancingInitialized := false;
}

} // end of module
