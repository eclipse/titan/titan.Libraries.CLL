///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  Module: EPTF_CLL_Transport_IPsecHandler_Logging_Server_Functions
//
//  Purpose:
//    This module contains the implementation of IPSec parameters logging server functions.
//
//  Module Parameters:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////////////////////
module EPTF_CLL_Transport_IPsecHandler_Logging_Server_Functions {
import from EPTF_CLL_Transport_IPsecHandler_Logging_CommonDefinitions all;
import from EPTF_CLL_Transport_IPsecHandler_Logging_Server_Definitions all;
import from EPTF_CLL_Transport_IPsecHandler_Logging_Functions all;
import from TCCFileIO_Functions all;
import from EPTF_CLL_Base_Functions all;
import from TCCDateTime_Functions all;
import from TCCConversion_Functions all;
import from TCCInterface_Functions all;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_init
//
//  Purpose:
//    Initialises the EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT component
//
//  Parameters:
//    - pl_selfName - *in* *charstring* - The name of the component.
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_init(
  in charstring pl_selfName,
  in charstring pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path,
  in charstring pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name)
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
{
  v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path := pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path;
  v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name := pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name;

  if (not v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_initialized)
  {
    f_EPTF_CLL_Transport_IPsecHandler_Logging_init(pl_selfName, cg_EPTF_CLL_Transport_IPsecHandler_Logging_Server_loggingMaskName);

    v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_fileDesc := f_FIO_open_append_wronly (f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Path());

    if(v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_fileDesc == -1){
      f_EPTF_CLL_Transport_IPsecHandler_Logging_error("Logging IPSec Server : Failed to open the file: " & f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Path());
      return;
    }

    v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_eventAlt := activate(as_EPTF_CLL_Transport_IPsecHandler_Logging_Server_Event_Behaviour());
    f_EPTF_Base_registerCleanup(refers(f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_cleanup_CT));

    f_EPTF_CLL_Transport_IPsecHandler_Logging_debugDev("The Logging IPSec Server component was initialized.");
    v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_initialized := true;
  }
}

///////////////////////////////////////////////////////////
//  Altstep: as_EPTF_CLL_Transport_IPsecHandler_Logging_Server_Event_Behaviour
//
//  Purpose:
//    Handler for EventVector internal port communication
///////////////////////////////////////////////////////////
private altstep as_EPTF_CLL_Transport_IPsecHandler_Logging_Server_Event_Behaviour()
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
{
  var EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Event       vl_recvEvent;

  [] EPTF_CLL_Transport_IPsecHandler_Logging_Server_IPSec_PCO.receive(EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Event:?) -> value vl_recvEvent
  {
    f_EPTF_CLL_Transport_IPsecHandler_Logging_debug("Logging IPSec Server : Event Vector received: " & log2str(vl_recvEvent));

    var integer vl_result := f_Save_Event_Data_to_File(vl_recvEvent);

    if(vl_result == -1)
    {
      f_EPTF_CLL_Transport_IPsecHandler_Logging_warning("Logging IPSec Server : Failed to log received event: "
        & log2str(vl_recvEvent) & "to a file: " & f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Path());
    }

    repeat;
  }

  [] EPTF_CLL_Transport_IPsecHandler_Logging_Server_IPSec_PCO.receive
  {
    f_EPTF_CLL_Transport_IPsecHandler_Logging_warning("Logging IPSec Server : Unexpected message received from EPTF_CLL_Transport_IPsecHandler_Logging_Server_IPSec_PCO: dropping...");
    repeat;
  }
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_behaviour
//
//  Purpose:
//    Implements the EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT behaviour
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
public function f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_behaviour(
  in charstring pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path,
  in charstring pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name
)
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
{
  f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_init(cg_EPTF_CLL_Transport_IPsecHandler_Logging_Server_loggingMaskName, pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path, pl_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name);

  f_EPTF_Base_wait4Shutdown();
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_cleanup_CT
//
//  Purpose:
//    This function should be called before the EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT component shuts down.
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_CLL_Transport_IPsecHandler_Logging_Server_cleanup_CT()
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
{
  deactivate(v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_eventAlt);
  var integer vl_result := f_FIO_close (v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_fileDesc);

  if(vl_result == -1)
  {
    f_EPTF_CLL_Transport_IPsecHandler_Logging_warning("Logging IPSec Server : Failed to close the file: " & f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Path());
  }

  v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_initialized := false;

  f_EPTF_CLL_Transport_IPsecHandler_Logging_debugDev("The Logging IPSec server component cleanup finished.");
}

///////////////////////////////////////////////////////////
//  Function: f_Save_Event_Data_to_File
//
//  Purpose:
//    Saves IPSec data to file in wireshark format.
//
//  Parameters:
//    - pl_event - *in* *EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Event* - data to be written to a file
//
//  Return Value:
//    - *in* - operation status
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_Save_Event_Data_to_File(EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Event pl_event)
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
return integer
{
  var hexstring vl_hexCK := oct2hex(pl_event.CK);
  var hexstring vl_hexIK := oct2hex(pl_event.IK);
  var hexstring vl_hexSPI := int2hex(pl_event.SPI, 8);

  var charstring vl_CK := f_putInLowercase( cg_Hex_Prefix & hex2str(vl_hexCK) );
  var charstring vl_IK := f_putInLowercase( cg_Hex_Prefix & hex2str(vl_hexIK) );
  var charstring vl_SPI := f_putInLowercase( cg_Hex_Prefix & hex2str(vl_hexSPI) );
  
  var charstring vl_ealgo := pl_event.ealgo;
  var charstring vl_aalgo := pl_event.aalgo;

  var charstring vl_protocol;

  if(f_verifyIpAddr(pl_event.srcAddr, IPv6))
  {
    vl_protocol := cg_Protocol_Type.IPv6;
  }
  else if(f_verifyIpAddr(pl_event.srcAddr, IPv4))
  {
    vl_protocol := cg_Protocol_Type.IPv4;
  }
  else
  {
    vl_protocol := cg_Protocol_Type.IPv4;
    log("ERROR: In function f_Save_Event_Data_to_File: invalid IP address in EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Event pl_event: ", pl_event.srcAddr);
  }

  var charstring vl_text := "\""& vl_protocol & "\","
  & "\"" &  pl_event.srcAddr & "\","
  & "\"" &  pl_event.dstAddr & "\","
  & "\"" &  vl_SPI           & "\","
  & "\"" &  vl_ealgo         & "\","
  & "\"" &  vl_CK            & "\","
  & "\"" &  vl_aalgo         & "\","
  & "\"" &  vl_IK            & "\"\n";

  var integer vl_result := f_FIO_write_text_flush (v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_fileDesc, vl_text);

  return vl_result;
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Name
//
//  Purpose:
//    Return path to log file.
//
//  Parameters:
//    -
//
//  Return Value:
//    - *charstring* - Path to log file.
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_Get_LogFile_Path()
runs on EPTF_CLL_Transport_IPsecHandler_Logging_Server_CT
return charstring
{
  return (v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Path
    & f_getTimeFormatted(float2int(f_EPTF_Base_getRelTimeOffsetInSecs()), v_EPTF_CLL_Transport_IPsecHandler_Logging_IPSec_File_Name));
}

}

