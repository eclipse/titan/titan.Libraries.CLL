///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatReplay_Execution
// 
//  Purpose:
//    This module contains tests of EPTF_CLL_StatReplay_CT.
// 
//  Module depends on:
//    <EPTF_CLL_StatReplay_Definitions>
//	  <EPTF_CLL_StatReplay_Functions>
//	  <XTDPasp_PortType>
// 
//  Current Owner:
//    Norbert PintÃ©r (ENORPIN)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//	  -
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_StatReplay_Execution{

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_StatReplay_Definitions all;
import from EPTF_CLL_StatReplay_Functions all;

//=========================================================================
// Test Components
//=========================================================================
type component StatReplayDemo_CT extends EPTF_CLL_StatReplay_CT
{
}

type component SYSTEM_CT{
} 

//=========================================================================
// Test Cases
//=========================================================================
testcase tc_Demo() runs on StatReplayDemo_CT system SYSTEM_CT{
  
  f_EPTF_CLL_StatReplay_init_CT("StatReplay_CT");
  
  setverdict(pass);
}

//=========================================================================
// Control
//=========================================================================
control{
  execute(tc_Demo());
}

}
