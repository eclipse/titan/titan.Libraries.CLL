///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_ExecCtrl_LoggingFunctions
// 
//  Purpose:
//    This module contains the implementation of logging for EPTF_CLL_ExecCtrl.
// 
//  Module depends on:
//    <EPTF_ExecCtrl_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Logging_Functions>
//
//  Module Parameters:
//    tsp_debug_EPTF_ExecCtrl_Functions - boolean
//
//  Current Owner:
//    Gabor Tatarka (EGBOTAT)
// 
//  Last Review Date:
//    2008-02-11
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_ExecCtrl_LoggingFunctions {

import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Logging_Functions all;

friend module EPTF_CLL_ExecCtrl_Functions, EPTF_CLL_ExecCtrl_ScenarioFunctions, EPTF_CLL_ExecCtrl_PhaseFunctions, EPTF_CLL_ExecCtrlClient_Functions, EPTF_CLL_ExecCtrl_DSFunctions;

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrl_error
  // 
  //  Purpose:
  //    Function to log an error from ExecCtrl feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrl_error(in charstring pl_message)
  runs on EPTF_ExecCtrl_CT
  {
    f_EPTF_Logging_error(true, tsp_EPTF_ExecCtrl_loggingComponentMask&": "&pl_message);
    f_EPTF_Base_stopAll();
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrl_warning
  // 
  //  Purpose:
  //    Function to log a warning from ExecCtrl feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrl_warning(in @lazy charstring pl_message)
  runs on EPTF_ExecCtrl_CT
  {
    f_EPTF_Logging_warningV2(pl_message, v_ExecCtrl_loggingMaskId, {c_EPTF_ExecCtrl_loggingClassIdx_Warning});
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrl_debug
  // 
  //  Purpose:
  //    Function to log a debug message from ExecCtrl feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrl_debug(in @lazy charstring pl_message)
  runs on EPTF_ExecCtrl_CT
  {
    f_EPTF_Logging_debugV2(pl_message, v_ExecCtrl_loggingMaskId, {c_EPTF_ExecCtrl_loggingClassIdx_Debug});
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrl_debugEnabled
  // 
  //  Purpose:
  //    Function to check if debug is enabled for ExecCtrl
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    *boolean* - true if debug enalbed
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrl_debugEnabled()
  runs on EPTF_ExecCtrl_CT
  return boolean
  {
    return f_EPTF_Logging_isEnabled(v_ExecCtrl_loggingMaskId, c_EPTF_ExecCtrl_loggingClassIdx_Debug);
  }

group ExecCtrlClient {

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrlClient_error
  // 
  //  Purpose:
  //    Function to log an error from ExecCtrlClient feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrlClient_error(in charstring pl_message)
  runs on EPTF_ExecCtrlClient_CT
  {
    f_EPTF_Logging_error(true, tsp_EPTF_ExecCtrlClient_loggingComponentMask&": "&pl_message);
    f_EPTF_Base_stopAll();
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrlClient_warning
  // 
  //  Purpose:
  //    Function to log a warning from ExecCtrlClient feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrlClient_warning(in @lazy charstring pl_message)
  runs on EPTF_ExecCtrlClient_CT
  {
    f_EPTF_Logging_warningV2(pl_message, v_ExecCtrlClient_loggingMaskId, {c_EPTF_ExecCtrl_loggingClassIdx_Warning});
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrlClient_debug
  // 
  //  Purpose:
  //    Function to log a debug message from ExecCtrlClient feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrlClient_debug(in @lazy charstring pl_message)
  runs on EPTF_ExecCtrlClient_CT
  {
    f_EPTF_Logging_debugV2(pl_message, v_ExecCtrlClient_loggingMaskId, {c_EPTF_ExecCtrl_loggingClassIdx_Debug});
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_ExecCtrlClient_debugEnabled
  // 
  //  Purpose:
  //    Function to check if debug is enabled for ExecCtrlClient
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    *boolean* - true if debug enalbed
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  friend function f_EPTF_ExecCtrlClient_debugEnabled()
  runs on EPTF_ExecCtrlClient_CT
  return boolean
  {
    return f_EPTF_Logging_isEnabled(v_ExecCtrlClient_loggingMaskId, c_EPTF_ExecCtrl_loggingClassIdx_Debug);
  }

} // group ExecCtrlClient
  
} // module
