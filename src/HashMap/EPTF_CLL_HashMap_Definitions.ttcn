///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////  Detailed Comments:
//    Defines the component type of the HashMap feature.
//    For more detailed on the functionality see <EPTF_CLL_HashMap_Functions>.
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_HashMap_Definitions {

import from EPTF_CLL_Base_Definitions all;

///////////////////////////////////////////////////////////
//  Type: EPTF_HashMap_CT
// 
//  Purpose:
//   The component that implements the EPTF HashMap feature
// 
//  Elements:
//    v_HashMap_initialized - *boolean* - prevents multiple initialization
//
//  Detailed Comments:
//    To use the EPTF HashMap, extend your component with <EPTF_HashMap_CT>
//
///////////////////////////////////////////////////////////
type component EPTF_HashMap_CT extends EPTF_Base_CT {
  private var boolean v_HashMap_initialized := false;
}

}  // end of module EPTF_CLL_HashMap_Definitions
