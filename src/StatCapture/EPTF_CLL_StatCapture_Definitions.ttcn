///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatCapture_Definitions
// 
//  Purpose:
//    This module contains common type definitions that should be used in all Statistics Capture Controlling Components.
// 
//  Module Parameters:
//    tsp_EPTF_StatCapture_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_StatCapture"
//
//  Module depends on:
//    <StatMeasure>
//    <Scheduler>
//    <RBTScheduler>
//    <Base>
//    <HashMap>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_HashMap_Definitions>
// 
//  Current Owner:
//    JÃÂ¡nos SvÃÂ¡ner (EJNOSVN)
// 
//  Last Review Date:
//    2007-11-16
//
//  Detailed Comments:
//    This module contains common type definitions that should be used in all Statistics Capture Controlling Components.
//    
///////////////////////////////////////////////////////////////

module EPTF_CLL_StatCapture_Definitions {

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_StatMeasure_Definitions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Definitions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_HashMap_Definitions all;

modulepar charstring tsp_EPTF_StatCapture_loggingComponentMask := "EPTF_StatCapture";

//=========================================================================
// Data Types
//=========================================================================


///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_Statistics
// 
//  Purpose:
//    Record of useful statistics info
// 
//  Elements:
//    statID - *integer* - unique statistics identifier (reference to a statistics data)
//    statname - *charstring* - statistics name
//    activestat - *boolean* - state of statistics, active/inactive (only active statistics are captured)
//    periodicreset - *boolean* - whether the statistics should be reset at capture intervals
//    
// Detailed Comments:
//    We store useful information on statistics (ID, name, state, must be reset or not)
//    in this record.
// 
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_Statistics {

  integer         statID,    // statID is a reference to a statistics 
  charstring      statname,
  boolean         activestat,
  boolean         periodicreset
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_GroupStatistics
// 
//  Purpose:
//    An array of EPTF_StatCapture_Statistics (Statistics info). 
// 
//  Elements:
//    record of <EPTF_StatCapture_Statistics>
// 
// Detailed Comments:
//    This is an array of EPTF_StatCapture_Statistics. It is used to store statistics in capture groups.
//    Note: One statistical data might be present in different capture groups as well.
// 
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_Statistics EPTF_StatCapture_GroupStatistics;

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_FdIdx
// 
//  Purpose:
//    Record assigning file descriptor and its index in the EPTF_StatCapture_FileList database
// 
//  Elements:
//    fd - *integer* - file descriptor
//    idx - *integer* - the index belonging to the file descriptor
//    
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_FdIdx {

     integer fd,
     integer idx
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_FdIdxList
// 
//  Purpose:
//    An array of tfdidx, stored in each capture group, containing idx to fd
//    for lookup in the internal tFileList database 
// 
//  Elements:
//    record of <EPTF_StatCapture_FdIdx>
// 
// Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_FdIdx EPTF_StatCapture_FdIdxList;

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_FileFormat
// 
//  Purpose:
//     Declares the possible types of logfile formats for statistics
//
//  Elements:
//    readable - readable, legacy format (.txt)
//    gnuplot - format defined for Gnuplot (.gpl)
//
//  Detailed Comments:
//   -
//    
///////////////////////////////////////////////////////////
type enumerated EPTF_StatCapture_FileFormat{
      
      readable,
      gnuplot
}
///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_ResetStats
// 
//  Purpose:
//    Record containing statistics ID (only stats with periodicreset==true) and whether they are already in a capture group
// 
//  Elements:
//    statID - *integer* - statistics ID
//    groupID - *integer* - the group ID, where the statistics is placed
//    
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_ResetStats {

     integer statID,
     integer groupID 
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_resetStatsList
// 
//  Purpose:
//    A record of resetStats, for lookup 
//    whether a given statID is already placed in a Capture Group 
// 
//  Elements:
//    record of <EPTF_StatCapture_resetStats>
// 
// Detailed Comments:
//    Aim is to check the constraint with this structure, that
//    a statistics with periodic reset == true should only
//    be contained in one and only one Capture Group.
//    Periodicly resettable statistics are added to the record with the capture groupID.
// 
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_ResetStats EPTF_StatCapture_ResetStatsList;


///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_Group
// 
//  Purpose:
//    Record of statistics data defining a capture group
// 
//  Elements:
//    groupName - *charstring* - unique group name
//    timerPeriod - *float* - the timer period for refreshing group data
//    groupStatistics - <EPTF_StatCapture_groupStatistics> - statistics record belonging to the capture group
//    logFiles - <EPTF_StatCapture_IntList> - stores file descriptors of output files for logging
//    deleted - *boolean* - set to true when the group is deleted
//    eventID - *integer* - the eventID in the Scheduler belonging to the group (to handle timing)
//    measurementID - *integer* - measurement ID (integer counter) belonging to the measurement
//    statAdded - *boolean* - if true, write value header on next write
//    
// Detailed Comments:
//    A capture group is a group of statistics, with a common timerPeriod describing the timeout event period.
//    It contains a statistics array with values and/or statistical data, has a unique group name, 
//    an internal deleted flag and an eventID for the scheduling of actions
// 
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_Group  {

     charstring                           groupName,
     float                                timerPeriod,
     EPTF_StatCapture_GroupStatistics     groupStatistics,
     EPTF_StatCapture_FdIdxList           logFiles,
     boolean                              deleted,          //internal parameter, marks group as deleted  
     integer                              eventID,           //internal parameter, the ID of the registered event belonging to the group (if no event: -1, else >=0)
     integer                              measurementID,
     boolean                              statAdded // if true, write value header on next write
}   

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_CaptureGroups
// 
//  Purpose:
//    An array of EPTF_StatCapture_tgroups (i.e. capture groups) 
// 
//  Elements:
//    record of <EPTF_StatCapture_tgroup>
// 
// Detailed Comments:
//    This is an array of EPTF_StatCapture_tgroups. It is used to store capture groups.
//    Capture groups can be found by indexing this array.
// 
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_Group EPTF_StatCapture_CaptureGroups;


///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_FileData
// 
//  Purpose:
//    Record containing file data for capture files
// 
//  Elements:
//    startfd - *integer* - the first file descriptor, where the data stream has been dumped 
//    actfd - *integer* - the actual file descriptor, where the data stream is dumped (after the first file is full)
//    nrofBytes - *integer* - the nr of Bytes in the actual file, where we currently dump to
//    nrofFiles - *integer* - the nr of files opened for this data stream
//    startDate - *charstring* - the date when the dumping was started to the first file
//   
// Detailed Comments:
//    
// 
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_FileData  {
     
     integer            startfd,
     integer            actfd,
     integer            nrofBytes,
     integer            nrofFiles,
     charstring         startDate       
}   

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_FileList
// 
//  Purpose:
//    An array of EPTF_StatCapture_tFileData
//
//  Elements:
//    record of <EPTF_StatCapture_FileData>
// 
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_FileData EPTF_StatCapture_FileList;

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_StringList
// 
//  Purpose:
//    An array of charstring
//
//  Elements:
//    record of *charstring*
// 
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record of charstring EPTF_StatCapture_StringList;


///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_StatConfig
// 
//  Purpose:
//    A type assigned to a config file parameter for defining statistics for capture groups.
//
//  Elements:
//    varName - *charstring* - name of the EPTF Variable, for which the EPTF Statistics will be created
//    statType - <EPTF_StatMeasure_statType> - type of the EPTF Statistics (eg. min, max, delta, etc.) to be created
//    statName - *charstring* - name of the EPTF Statistics to be created
//    periodicReset - *boolean* - whether the Statistics should be reset in each capture period
//    activeStat - *boolean* - whether the Statistics shold be active (printed out) or inactive ("-" printed instead)
//     
// Detailed Comments:
//    This type of module parameter is used to read in statistics definitions
//    from the configuration file. This way, it is possible to add/remove statistics
//    from capture groups or to change statistics settings in an easy way,
//    without program recompilation.
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_StatConfig {

      charstring                        varName,
      EPTF_StatMeasure_statType         statType,
      charstring                        statName,
      boolean                           periodicReset,
      boolean                           activeStat 
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_StatisticsConfig
// 
//  Purpose:
//    An array of EPTF_StatCapture_StatConfig
//
//  Elements:
//    record of <EPTF_StatCapture_StatConfig>
// 
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_StatConfig EPTF_StatCapture_StatisticsConfig;

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_CaptureGroupConfig
// 
//  Purpose:
//    A type assigned to a config file parameter for defining capture groups.
//
//  Elements:
//    groupName - *charstring* - name of the capture group, for which the Statistics will be added
//    timerPeriod - *float* - the capture period of the capture group
//    logFileNames - <EPTF_StatCapture_StringList> - the logfiles, to which capture group content will be printed out
//    statistics - <EPTF_StatCapture_StatisticsConfig> - the Statistics configuration belonging to the capture group
//     
// Detailed Comments:
//    This type of module parameter is used to read in capture group definitions
//    from the configuration file. This way, it is possible to add/remove statistics
//    from capture groups or to change capture group settings in an easy way,
//    without program recompilation.
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_CaptureGroupConfig {

      charstring                        groupName,
      float                             timerPeriod,
      EPTF_StatCapture_StringList       logFileNames optional,
      EPTF_StatCapture_StatisticsConfig statistics      

}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_CaptureGroupsConfig
// 
//  Purpose:
//    An array of EPTF_StatCapture_CaptureGroupConfig
//
//  Elements:
//    record of <EPTF_StatCapture_CaptureGroupConfig>
// 
// Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record of EPTF_StatCapture_CaptureGroupConfig EPTF_StatCapture_CaptureGroupsConfig;

///////////////////////////////////////////////////////////
//  Template: t_StatCaptureClient_startGroup
// 
//  Purpose:
//    A template over EPTF_StatCapture_OpMsg - this kind of message tells the server to start the group named pl_name 
//    (pl_forall should be false then) or if pl_forall = true then all the groups should be started. 
//
///////////////////////////////////////////////////////////
template EPTF_StatCapture_OpMsg t_StatCaptureClient_startGroup(in charstring pl_name, in boolean pl_forall) := 
{startGroup := {
   groupName := pl_name,
   forall := pl_forall
 }
}

///////////////////////////////////////////////////////////
//  Template: t_StatCaptureClient_stopGroup
// 
//  Purpose:
//    A template over EPTF_StatCapture_OpMsg - this kind of message tells the server to stop the group named pl_name 
//    (pl_forall should be false then) or if pl_forall = true then all the groups should be stopped. 
//
///////////////////////////////////////////////////////////
template EPTF_StatCapture_OpMsg t_StatCaptureClient_stopGroup(in charstring pl_name, in boolean pl_forall) :=
{stopGroup := {
   groupName := pl_name,
   forall := pl_forall
 }
}

///////////////////////////////////////////////////////////
//  Template: t_StatCaptureClient_deleteGroup
// 
//  Purpose:
//    A template over EPTF_StatCapture_OpMsg - this kind of message tells the server to delete the group named pl_name 
//    (pl_forall should be false then) or if pl_forall = true then all the groups should be stopped. 
//
///////////////////////////////////////////////////////////
template EPTF_StatCapture_OpMsg t_StatCaptureClient_deleteGroup(in charstring pl_name, in boolean pl_forall) :=
{deleteGroup := {
   groupName := pl_name,
   forall := pl_forall
 }
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_SubscribeAndCreateGroupOP
// 
//  Purpose:
//   Type for the message sent by the client to the server in order to subcribe a Statistic to the Variable on the client.  
//
//  Elements:
//    groupName - *charstring* - Statistic Group name - new to be created or an existing one. 
//    statNames - <EPTF_CharstringList> - the names of the statistics to subscribe on.
//    subscriptionMode - <EPTF_Var_SubscriptionMode> - subscription mode of the variable
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_SubscribeAndCreateGroupOP{
  charstring groupName,
  EPTF_CharstringList statNames,
  EPTF_Var_SubscriptionMode subscriptionMode
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_GroupOP
// 
//  Purpose:
//   Type for the message sent to start or stop a specific group of all of them.  
//
//  Elements:
//    groupName - *charstring* - existing statistic group name.
//    forall - *boolean* - if not one group but all should do something.
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_GroupOP{ 
  charstring groupName,
  boolean forall
}

///////////////////////////////////////////////////////////
// Type: EPTF_StatCapture_Operation
//
// Purpose: a union for the available statistic capture operations between client and server
//
// Elements:
//   subsAndCreateGroup - <EPTF_StatCapture_SubscribeAndCreateGroupOP> - subscribe operation 
//   startGroup - <EPTF_StatCapture_GroupOP> - start operation
//   stopGroup - <EPTF_StatCapture_GroupOP> - stop operation
///////////////////////////////////////////////////////////
type union EPTF_StatCapture_Operation{
  EPTF_StatCapture_SubscribeAndCreateGroupOP subsAndCreateGroup,
  EPTF_StatCapture_GroupOP startGroup,
  EPTF_StatCapture_GroupOP stopGroup,
  EPTF_StatCapture_GroupOP deleteGroup
}

///////////////////////////////////////////////////////////
// Type: EPTF_StatCapture_OpMsg
//
// Purpose:
//   Data type for message sent through the internal port between client and server
//
// Elements:
//   Type alias for <EPTF_StatCapture_Operation>
///////////////////////////////////////////////////////////
type EPTF_StatCapture_Operation EPTF_StatCapture_OpMsg;

///////////////////////////////////////////////////////////
// Type: EPTF_StatCapture_SubscribeVar_Req_Param
//
// Purpose:
//   Store request parameters before calling variable subscribe
//
// Elements:
//   request parameters
///////////////////////////////////////////////////////////
type record EPTF_StatCapture_SubscribeVar_Req_Param {
 EPTF_Var_CT                remoteCompRef,
 charstring                 remoteProviderVarName,
 EPTF_Var_SubscriptionMode  subscriptionMode,
 charstring                 localName,
 integer                    refreshRate,
 charstring                 groupName
}

type record of EPTF_StatCapture_SubscribeVar_Req_Param EPTF_StatCapture_SubscribeVar_Req_Param_List;

///////////////////////////////////////////////////////////
// Type: EPTF_StatCapture_PT
//
// Purpose:
//   Internal port for communicating between client and server
///////////////////////////////////////////////////////////
type port EPTF_StatCapture_PT message{
  inout EPTF_StatCapture_OpMsg
} with {extension "internal"}

//=========================================================================
//Component Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCaptureClient_CT
// 
//  Purpose:
//   The component that implements the Statistics Capture Control Client feature
// 
//  Elements:
//    v_StatCaptureClient_initialized - *boolean* - prevents multiple init by calling <f_EPTF_StatCaptureClient_init> several times
//    v_StatCaptureClient_server - <EPTF_StatCapture_CT> - the server component to connect to.
//    v_StatCaptureClient_PCO - <EPTF_StatCapture_PT> - Internal port
//
//  Detailed Comments:
//   To use the Statictics Capture Control Client Feature, extend your component with <EPTF_StatCaptureClient_CT>
//
///////////////////////////////////////////////////////////
type component EPTF_StatCaptureClient_CT extends EPTF_Var_CT{
    private var boolean v_StatCaptureClient_initialized := false;
    private var EPTF_StatCapture_CT v_StatCaptureClient_server := null;
    private port EPTF_StatCapture_PT v_StatCaptureClient_PCO;
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCapture_CT
// 
//  Purpose:
//   The component that implements the Statistics Capture Control feature
// 
//  Elements:
//    v_StatCapture_initialized - *boolean* - prevents multiple init by calling <f_StatCapture_init> several times
//    v_captureGroups - <EPTF_StatCapture_CaptureGroups> - contains groups, where all groups consist of groupName, timerPeriod (timeout), 
//                                        statistics, deleted flag and eventID (belonging scheduler event to group) 
//    v_StatCapture_FileDataList - <EPTF_StatCapture_FileList> - contains file descriptors opened when starting capture, file sizes and other useful data
//                                                for handling oversized log files
//    v_StatCapture_deffd - *integer* - default capture file to print out statistics data
//    v_StatCapture_eventHandler - <EPTF_Scheduler_ActionHandler> - event handler for the Scheduler 
//    v_StatCapture_inthashmap_id - *integer* - int2int hashmap containing opened filename indexes and file descriptors
//    v_StatCapture_logfilenames - <EPTF_StatCapture_StringList> - array storing opened filenames
//    v_StatCapture_resetStatList - <EPTF_StatCapture_ResetStatList> - array of statistics with periodic reset == true inserted to a capture group
//    v_StatCapture_serverAlt - <default> - default altstep to handle client requests
//    v_StatCapture_PCO - <EPTF_StatCapture_PT> - Internal port
//
//  Detailed Comments:
//   To use the Statictics Capture Control Feature, extend your component with <EPTF_StatCapture_CT>
//
///////////////////////////////////////////////////////////
type component EPTF_StatCapture_CT extends EPTF_Base_CT, EPTF_StatMeasure_CT,EPTF_Scheduler_CT, EPTF_Logging_CT, EPTF_HashMap_CT {
  private var boolean v_StatCapture_initialized := false;
  private var EPTF_StatCapture_CaptureGroups v_StatCapture_captureGroups := {};
  private var EPTF_StatCapture_FileList v_StatCapture_FileDataList := {};
  private var integer v_StatCapture_deffd;
  private var EPTF_Scheduler_ActionHandler v_StatCapture_eventHandler := null;
  private var integer v_StatCapture_inthashmap_id;
  private var EPTF_StatCapture_StringList v_StatCapture_logfilenames; 
  private var EPTF_StatCapture_ResetStatsList v_StatCapture_resetStatList := {};
  private var default v_StatCapture_serverAlt := null;
  private port EPTF_StatCapture_PT v_StatCapture_PCO;
  // logging
  private var integer v_StatCapture_loggingMaskId := c_EPTF_Logging_invalidMaskId;
  private var charstring v_EPTF_StatCapture_GroupStat_directory := ""; // group statistics directory
  // call back db
  private var EPTF_StatCapture_SubscribeVar_Req_Param_List v_StatCapture_subscribeVarReqParamList := {};
}

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatCapture_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used on the StatCapture
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
///////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_StatCapture_loggingEventClasses := { "Warning", "Debug" };

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatCapture_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////
const integer c_EPTF_StatCapture_loggingClassIdx_Warning := 0;
///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatCapture_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *1*
///////////////////////////////////////////////////////////
const integer c_EPTF_StatCapture_loggingClassIdx_Debug := 1;

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatCapture_remoteVarPrefix
// 
//  Purpose:
//    prefix for the remote variable created during subscribe
// 
//  Detailed Comments:
//    *StatCapture.remoteVar.*
///////////////////////////////////////////////////////////
const charstring c_EPTF_StatCapture_remoteVarPrefix := "StatCapture.remoteVar.";

} // end of module
