///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatCaptureControl_Definitions
// 
//  Purpose:
//    This module contains common type definitions that should be used in all Statistics Capture Controlling Components.
// 
//  Module Parameters:
//    tsp_EPTF_StatCaptureControl_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_StatCaptureControl"
//
//  Module depends on:
//    <EPTF_CLL_StatCapture_Definitions>
//    <EPTF_CLL_NameService_Definitions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Logging_Definitions>
// 
//  Current Owner:
//    Andrea Darabos (EANDDAR)
// 
//  Last Review Date:
//    2008-01-17
//
//  Detailed Comments:
//    This module contains common type definitions that should be used in all Statistics Capture Controlling Components.
//    
///////////////////////////////////////////////////////////////

module EPTF_CLL_StatCaptureControl_Definitions {

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_StatCapture_Definitions all;
import from EPTF_CLL_NameService_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Logging_Definitions all;

modulepar charstring tsp_EPTF_StatCaptureControl_loggingComponentMask := "EPTF_StatCaptureControl";

//=========================================================================
// Data Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCaptureControl_CaptureMode
// 
//  Purpose:
//     Declares the possible modes of statistics capture control
//
//  Elements:
//    manual - manual start capture and stop capture
//    loadBased - starting capture if the load (or a similar parameter) has been stable for a while, stopping it on timer timeout
//    complete - starting capture on load generation start, stopping it on load gen stop
//
//  Detailed Comments:
//   -
//    
///////////////////////////////////////////////////////////
type enumerated EPTF_StatCaptureControl_CaptureControlMode {

  manual,
  loadBased,
  complete
}

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCaptureControl_controlVariable
// 
//  Purpose:
//     Data structure for the lookup of a Variable for subscription
//
//  Elements:
//    componentName - *integer* - The name of the component, the subscribed Variable belongs to
//    varName - *charstring* - The name of the Variable for subscription
//
//  Detailed Comments:
//   This Variable can signal, when the user pressed the start/stop button.
//   This Variable can signal load generation start/stop as well.
//   In load-based mode, the load stability is measured according to this Variable content.
//   The user can set which Variable he wants to use by giving these data in the config file.
//   This Variable should have a boolean content.
//    
///////////////////////////////////////////////////////////
type record EPTF_StatCaptureControl_controlVariable{

  charstring componentName,
  charstring varName
}

//=========================================================================
//Component Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Type: EPTF_StatCaptureControl_CT
// 
//  Purpose:
//   The component that implements the Statistics Capture Control feature
// 
//  Elements:
//    v_StatCaptureControl_initialized - *boolean* - prevents multiple init by calling <f_StatCapture_init> several times
//    v_StatCaptureControl_def - *default* - default altstep for load-based mode
//    t_loadStableToStart - *timer* - 
//    t_loadStableCapturePeriod - *timer* - 
//    t_loadStableCaptureDelay - *timer* - 
//    v_StatCaptureControl_loadStableToStartL - *float* - component variable containing module parameter value
//    v_StatCaptureControl_loadStableCapturePeriodL - *float* - component variable containing module parameter value
//    v_StatCaptureControl_loadStableCaptureDelayL - *float* - component variable containing module parameter value
//    v_StatCaptureControl_nrOfCaptures - *integer* -
//    v_StatCaptureControl_UserVariableID - *integer* - the ID of the Control Variable for manual capture mode
//    v_StatCaptureControl_LoadVariableID - *integer* -  the ID of the Control Variable for load-based capture mode
//    v_StatCaptureControl_ExecutionVariableID - *integer* - the ID of the Control Variable for complete capture mode
//
//  Detailed Comments:
//   To use the extended Statictics Capture Control Feature with automatic capture control,
//   extend your component with <EPTF_StatCaptureControl_CT>
//
///////////////////////////////////////////////////////////
type component EPTF_StatCaptureControl_CT extends EPTF_StatCapture_CT, EPTF_NS_Client_CT, EPTF_Var_CT, EPTF_Logging_CT {
  private var boolean v_StatCaptureControl_initialized := false;
  private var default v_StatCaptureControl_def;
  private timer t_loadStableToStart;
  private timer t_loadStableCapturePeriod;
  private timer t_loadCaptureDelay;
  private var float v_StatCaptureControl_loadStableToStartL;
  private var float v_StatCaptureControl_loadStableCapturePeriodL;
  private var float v_StatCaptureControl_loadStableCaptureDelayL;
  private var integer v_StatCaptureControl_nrOfCaptures := 0;
  private var integer v_StatCaptureControl_UserVariableID;  
  private var integer v_StatCaptureControl_LoadVariableID; 
  private var integer v_StatCaptureControl_ExecutionVariableID;
  
  // logging
  private var integer v_StatCaptureControl_loggingMaskId := c_EPTF_Logging_invalidMaskId;  
}

} // end of module
