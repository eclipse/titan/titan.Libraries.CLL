///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//  Module: EPTF_UIHandler_Test_Definitions
// 
//  Purpose:
//    This module contains tests of EPTF_CLL_UIHandler_CT.
// 
//  Module depends on:
//   TBD
// 
//  Current Owner:
//    Jeno Balasko (ETHBAAT)
// 
//  Last Review Date:
//    2008-xx-xx
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////
module EPTF_UIHandler_Test_ASN1_Definitions 
{
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_UIHandler_Test_Definitions all;

//const EPTF_UIHandler_WidgetIdString c_EPTF_GUI_Main_Tabbox_WidgetId := "EPTF_Main_Tabbox";
//const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiExitButtonWidgetId := "EPTF_exit_ttcn_button";
//const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiSnapshotButtonWidgetId := "EPTF_snapshot_button";

//GUI elements

const charstring c_MainTabBox := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<tabpages id='EPTF_Main_Tabbox'>\n\t\t<tabpage id='TestTab1' label='test tab 1' orientation='vertical'/>\n\t\t<tabpage id='testTab2' label='test tab 2' orientation='vertical'/>\n\t</tabpages>\n</Widgets>\n\n"

const charstring c_MainHBox := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox id='EPTF_Main_hbox_Textboxes' orientation='vertical'/>\n</Widgets>\n\n"

//used?
const charstring c_MainWidgetsTest1 := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox flex='0.000000' orientation='horizontal'>\n\t\t<hbox flex='1.000000' orientation='vertical'>\n\t\t\t<spacer flex='0.000000'/>\n\t\t\t<hbox disabled='false' flex='1.000000' id='EPTF_Main_hbox_all' orientation='horizontal'>\n\t\t\t\t<hbox disabled='false' flex='1.000000' id='EPTF_Main_hbox_labels' orientation='vertical'>\n\t\t\t\t\t<textbox flex='1.000000' id='EPTF_timeElapsed' multiline='false' readonly='true' value='Time elapsed since Test was started: 0.0'/>\n\t\t\t\t\t<label disabled='false' flex='1.000000' id='status' value='This text will be replaced runtime.'/>\n\t\t\t\t</hbox>\n\t\t\t\t<hbox disabled='false' flex='0.000000' id='EPTF_Main_hbox' orientation='horizontal'>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_start_test_button' label='Start Test' type='checkbox'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_stop_test_button' label='Stop Test'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='false' flex='1.000000' id='EPTF_run_test_button' label='Enable Test' type='checkbox'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='false' flex='1.000000' id='EPTF_snapshot_button' label='Snapshot'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='false' flex='1.000000' id='EPTF_exit_ttcn_button' label='Exit'/>\n\t\t\t\t</hbox>\n\t\t\t</hbox>\n\t\t</hbox>\n\t</hbox>\n</Widgets>\n\n"

//used?:
const charstring c_MainWidgetsTest := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox id='EPTF_Main_hbox' orientation='horizontal'>\n\t\t<spacer flex='0.000000'/>\n\t\t<hbox id='EPTF_Main_hbox_labels' orientation='vertical'>\n\t\t\t<label id='refreshClock' value='Time elapsed since Test was started: 0.0'/>\n\t\t\t<label id='status' value='This text will be replaced runtime.'/>\n\t\t</hbox>\n\t\t<hbox id='EPTF_Main_hbox_buttons' orientation='horizontal'>\n\t\t\t<spacer flex='1.000000'/>\n\t\t\t<button disabled='true' id='EPTF_snapshot_button' label='Snapshot'/>\n\t\t\t<spacer flex='0.000000'/>\n\t\t\t<button disabled='true' id='EPTF_exit_ttcn_button' label='Exit TTCN'/>\n\t\t</hbox>\n\t</hbox>\n</Widgets>\n\n"

const charstring c_MainWidgets_sec := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox id='EPTF_sec_hbox' orientation='horizontal'>\n\t\t<hbox id='EPTF_Main_sec_labels' orientation='vertical'>\n\t\t\t<label id='refreshClock2' value='Time elapsed since Test was started: 0.0 blabla'/>\n\t\t\t<label id='status2' value='This text will be replaced runtime balacle.'/>\n\t\t\t<listbox disabled='false' id='flistbox' rows='2' seltype='multiple'>\n\t\t\t\t<listitem id='item1' label='Item1' selected='false'/>\n\t\t\t\t<listitem id='item2' label='Item2' selected='false'/>\n\t\t\t</listbox>\n\t\t\t<menulist editable='true' id='fmenulist' label='Label'>\n\t\t\t\t<menupopup>\n\t\t\t\t\t<menuitem id='menu1' label='Menu1' selected='false'/>\n\t\t\t\t\t<menuitem id='menu2' label='Menu2' selected='true'/>\n\t\t\t\t</menupopup>\n\t\t\t</menulist>\n\t\t</hbox>\n\t</hbox>\n</Widgets>\n\n"


const charstring c_wndXul := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<window height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>\n\t\t<hbox id='EPTF_Main_hbox_Textboxes' orientation='vertical'/>\n\t\t<tabpages id='EPTF_Main_Tabbox'>\n\t\t\t<tabpage id='TabpanelId' label='bubu' orientation='vertical'>\n\t\t\t\t<hbox id='EPTF_Main_hbox' orientation='horizontal'>\n\t\t\t\t\t<hbox id='EPTF_Main_hbox_labels' orientation='vertical'>\n\t\t\t\t\t\t<label id='refreshClock' value='Time elapsed since Test was started: 0.0'/>\n\t\t\t\t\t\t<label id='status' value='User set EPTF_snapshot_button to Snapshot'/>\n\t\t\t\t\t\t<textbox disabled='false' id='bubuTextBox' multiline='false' readonly='false' rows='1.000000' value='bubu' widgetType='console' wrap='false'/>\n\t\t\t\t\t</hbox>\n\t\t\t\t\t<hbox id='EPTF_Main_hbox_buttons' orientation='horizontal'>\n\t\t\t\t\t\t<spacer flex='1.000000'/>\n\t\t\t\t\t\t<button id='EPTF_snapshot_button' label='Snapshot'/>\n\t\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t\t<button id='EPTF_exit_ttcn_button' label='Exit TTCN'/>\n\t\t\t\t\t</hbox>\n\t\t\t\t\t<tree id='tree1' rows='4.000000'>\n\t\t\t\t\t\t<treecols>\n\t\t\t\t\t\t\t<treecol editable='true' flex='1.000000' label='column' widgetType='floatField'/>\n\t\t\t\t\t\t\t<treecol editable='true' flex='1.000000' label='column' widgetType='checkBox'/>\n\t\t\t\t\t\t</treecols>\n\t\t\t\t\t\t<treechildren>\n\t\t\t\t\t\t\t<treeitem>\n\t\t\t\t\t\t\t\t<treerow>\n\t\t\t\t\t\t\t\t\t<treecell id='cellFloat' label='bubuCellFloat'/>\n\t\t\t\t\t\t\t\t\t<treecell label='false'/>\n\t\t\t\t\t\t\t\t</treerow>\n\t\t\t\t\t\t\t\t<treerow>\n\t\t\t\t\t\t\t\t\t<treecell label=''/>\n\t\t\t\t\t\t\t\t\t<treecell id='cellBoxxx' label='false' tooltiptext='cell to connect data'/>\n\t\t\t\t\t\t\t\t</treerow>\n\t\t\t\t\t\t\t\t<treerow>\n\t\t\t\t\t\t\t\t\t<treecell label='' tooltiptext='This is an unnamed cell connected to the cell two rows upper'/>\n\t\t\t\t\t\t\t\t\t<treecell label=''/>\n\t\t\t\t\t\t\t\t</treerow>\n\t\t\t\t\t\t\t\t<treerow>\n\t\t\t\t\t\t\t\t\t<treecell label='' tooltiptext='This is an unnamed cell connected to the cell two rows upper'/>\n\t\t\t\t\t\t\t\t\t<treecell label=''/>\n\t\t\t\t\t\t\t\t</treerow>\n\t\t\t\t\t\t\t</treeitem>\n\t\t\t\t\t\t</treechildren>\n\t\t\t\t\t</tree>\n\t\t\t\t\t<chart axisXType='linear' axisYType='linear' backgroundColor='RGB:180:200:200' disabled='true' foregroundColor='Black' gridColor='black' gridX='true' gridY='false' id='charte' title='' zoomable='true'>\n\t\t\t\t\t\t<trace color='RGB:0:0:255' id='trace02' name='Blue line'/>\n\t\t\t\t\t\t<trace color='RGB:255:0:0' id='trace01' name='Red line'/>\n\t\t\t\t\t</chart>\n\t\t\t\t</hbox>\n\t\t\t</tabpage>\n\t\t</tabpages>\n\t</window>\n</Widgets>\n\n"

const charstring c_Tree :="<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<tree id='tree1' rows='0.000000'>\n\t\t<treecols>\n\t\t\t<treecol id='col1' label='column' widgetType='floatField'/>\n\t\t\t<treecol id='col2' label='column' widgetType='checkBox'/>\n\t\t\t<treecol id='intCol' label='integer' widgetType='integerField'/>\n\t\t</treecols>\n\t\t<treechildren>\n\t\t\t<treeitem>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell id='cellFloat' label='bubuCellFloat'/>\n\t\t\t\t\t<treecell label=''/>\n\t\t\t\t</treerow>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell label=''/>\n\t\t\t\t\t<treecell id='cellBoxxx' label='bubuCell' tooltiptext='cell to connect data'/>\n\t\t\t\t</treerow>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell label='' tooltiptext='This is an unnamed cell connected to the cell two rows upper'/>\n\t\t\t\t</treerow>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell label='' tooltiptext='This is an unnamed cell connected to the cell two rows upper'/>\n\t\t\t\t</treerow>\n\t\t\t</treeitem>\n\t\t</treechildren>\n\t</tree>\n</Widgets>\n\n"

const charstring c_EPTF_Test_tree := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<tree id='test_table' rows='0.000000'>\n\t\t<treecols>\n\t\t\t<treecol editable='false' flex='1.000000' id='coloumn1Id' label='GroupID' widgetType='string'/>\n\t\t\t<treecol editable='true' flex='1.000000' id='coloumn2Id' label='Expected number of incoming calls' widgetType='string'/>\n\t\t</treecols>\n\t\t<treechildren>\n\t\t\t<treeitem>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell id='apple1' label='alma1'/>\n\t\t\t\t\t<treecell id='bean1' label='alma2'/>\n\t\t\t\t</treerow>\n\t\t\t\t<treerow>\n\t\t\t\t\t<treecell id='apple2' label='korte1'/>\n\t\t\t\t\t<treecell id='bean2' label='korte2'/>\n\t\t\t\t</treerow>\n\t\t\t</treeitem>\n\t\t</treechildren>\n\t</tree>\n</Widgets>\n\n"

/////////////////////// constants for set focus function test ////////////////////////
// Buttons            
const charstring c_EPTF_Test_buttons := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox disabled='false' flex='0.000000' id='Buttons' orientation='horizontal'>\n\t\t<spacer flex='0.000000'/>\n\t\t<button disabled='false' flex='1.000000' id='Tab1' label='Tab1'/>\n\t\t<spacer flex='0.000000'/>\n\t\t<button disabled='false' flex='1.000000' id='Tab2' label='Tab2'/>\n\t\t<spacer flex='0.000000'/>\n\t\t<button disabled='false' flex='1.000000' id='EPTF_exit_ttcn_button' label='Exit'/>\n\t</hbox>\n</Widgets>\n\n" 

// textlabel for 1st tab
const charstring c_EPTF_Test_label1 := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<label disabled='false' flex='1.000000' id='Label1' value='1stTabLabel'/>\n</Widgets>\n\n"

// textlabel for 2nd tab
const charstring c_EPTF_Test_label2 := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<label disabled='false' flex='1.000000' id='Label2' value='2ndTabLabel'/>\n</Widgets>\n\n"

//////////////// end of set focus test constants


template ASP_XSD_XTDP tr_ASP_XTDP_snapshotButtonPressed :=
{
  client_id := ?,
  data := {
    //noNamespaceSchemaLocation := *,
    transactionID := omit,
    choice := {
      xTDP_Requests := {
        ackRequested := omit,
        xTDP_Request_list := {
          { requestId := ?,
            action_ := { choice := {
                put := {
                  widget := { widgetId := c_EPTF_runtimeGuiSnapshotButtonWidgetId, widgetType := ? },
                  argument_list := ?
                }
              }}
          }
        }
      }
    }
  }
}

template charstring t_MainTabBox := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<tabpages id='"&tsp_EPTF_GUI_Main_Tabbox_WidgetId&"'>\n\t\t<tabpage id='"&c_testTab1Id&"' label='test tab 1' orientation='vertical'/>\n\t\t<tabpage id='"&c_testTab2Id&"' label='test tab 2' orientation='vertical'/>\n\t</tabpages>\n</Widgets>\n\n"

///////////////////////////////////////////////////////////////////////////////
// Constant: c_MainWidgets
//
// Purpose:
//   XML constant for automated GUI elements for UI
//
// Detailed comments:
//   Defines a hbox that containts automated GUI elements.
//   Automated elements:
//     - execution control default tabbox
//     - test time counter: "Time elapsed since Test was started: <X.Y>"
//     - status: "This text will be replaced runtime."
//     - run test button
//     - snapshot button
//     - exit ttcn button
//    
///////////////////////////////////////////////////////////////////////////////
const charstring c_MainWidgets := "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\n\t<hbox flex='0.000000' orientation='horizontal'>\n\t\t<hbox flex='1.000000' orientation='vertical'>\n\t\t\t<spacer flex='0.000000'/>\n\t\t\t<hbox disabled='false' flex='1.000000' id='EPTF_Main_hbox_all' orientation='horizontal'>\n\t\t\t\t<hbox disabled='false' flex='1.000000' id='EPTF_Main_hbox_labels' orientation='vertical'>\n\t\t\t\t\t<textbox flex='1.000000' id='EPTF_timeElapsed' multiline='false' readonly='true' value='Time elapsed since Test was started: 0.0'/>\n\t\t\t\t\t<label disabled='false' flex='1.000000' id='status' value='This text will be replaced runtime.'/>\n\t\t\t\t</hbox>\n\t\t\t\t<hbox disabled='false' flex='0.000000' id='EPTF_Main_hbox' orientation='horizontal'>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_start_test_button' imageid='image_play' label='Start Test'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_stop_test_button' imageid='image_stop' label='Stop Test'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_snapshot_button' imageid='image_save' label='Snapshot'/>\n\t\t\t\t\t<spacer flex='0.000000'/>\n\t\t\t\t\t<button disabled='true' flex='1.000000' id='EPTF_exit_ttcn_button' imageid='image_exit' label='Exit'/>\n\t\t\t\t</hbox>\n\t\t\t</hbox>\n\t\t</hbox>\n\t</hbox>\n</Widgets>\n\n"
  
}//module
