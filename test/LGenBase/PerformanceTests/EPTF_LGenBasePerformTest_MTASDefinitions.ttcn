///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBasePerformTest_MTASDefinitions
//
//  Purpose:
//    This module provides functions for testing LGenBase
//
//  Module depends on:
//    -
//
//  Current Owner:
//    Laszlo Skumat (ELSZSKU)
//
//  Last Review Date:
//    2008-09-11
//
//  Detailed Comments:
//    Provide functions to test LGenBase
//
//
///////////////////////////////////////////////////////////////
module EPTF_LGenBasePerformTest_MTASDefinitions
{
//import from EPTF_CLL_LGenBase_Definitions all
import from EPTF_CLL_LGenBaseStats_Definitions all
import from TestResultGen all;

group CAI3G {
  const charstring c_EPTF_CAI3G_fsmName_connection := "FSM for CAI3G Component Connection";
  const integer c_EPTF_CAI3G_fsmState_idle := 0;
  const integer c_EPTF_CAI3G_fsmState_waitingForResponse := 1;
  const integer c_EPTF_CAI3G_fsmState_holdoff := 2; 
  const integer c_EPTF_CAI3G_fsmTimer_holdoff := 0;
  const integer c_EPTF_CAI3G_inputIdx_sendSessionRequest := 0;
  const charstring c_EPTF_CAI3G_inputName_sendSessionRequest := "Send CAI3G Session Request message";
  const integer c_EPTF_CAI3G_inputIdx_receiveSessionResponse := 1;
  const charstring c_EPTF_CAI3G_inputName_receiveSessionResponse := "Receive CAI3G Session Response message";
  const integer c_EPTF_CAI3G_inputIdx_receive503ErrorResponse := 2;
  const charstring c_EPTF_CAI3G_inputName_receive503ErrorResponse := "Receive CAI3G '503' Error Response message";
  const integer c_EPTF_CAI3G_inputIdx_receive500ErrorResponse := 3;
  const charstring c_EPTF_CAI3G_inputName_receive500ErrorResponse := "Receive CAI3G '500' Error Response message";
  const integer c_EPTF_CAI3G_inputIdx_receiveUndefinedResponse := 4;
  const charstring c_EPTF_CAI3G_inputName_receiveUndefinedResponse := "Receive a CAI3G Undefined Response message";
  const integer c_EPTF_CAI3G_inputIdx_responseSocketError := 5;
  const charstring c_EPTF_CAI3G_inputName_responseSocketError := "Receive CAI3G Socket Error";
  const integer c_EPTF_CAI3G_stepIdx_sendSessionRequest := 0; 
  const charstring c_EPTF_CAI3G_stepName_sendSessionRequest := "CAI3G - Send a Session Request message";
  const integer c_EPTF_CAI3G_stepIdx_receiveSessionResponse := 1; 
  const charstring c_EPTF_CAI3G_stepName_receiveSessionResponse := "CAI3G - Receive a Session Response message";
  const integer c_EPTF_CAI3G_stepIdx_receive503ErrorResponse := 2; 
  const charstring c_EPTF_CAI3G_stepName_receive503ErrorResponse := "CAI3G - Receive a '503' Error Response message";
  const integer c_EPTF_CAI3G_stepIdx_receive500ErrorResponse := 3; 
  const charstring c_EPTF_CAI3G_stepName_receive500ErrorResponse := "CAI3G - Receive a '500' Error Response message";
  const integer c_EPTF_CAI3G_stepIdx_handleTimerTimeout_holdoff := 4; 
  const charstring c_EPTF_CAI3G_stepName_handleTimerTimeout_holdoff := "CAI3G - 'Holdoff' Timer timeout step";
  const charstring c_EPTF_CAI3G_entity2IndexHashmapName := "CAI3G - Entity index Hashmap";
  const charstring c_EPTF_CAI3G_fsm2IndexHashmapName := "CAI3G - FSM index Hashmap";
  const charstring c_EPTF_CAI3G_loginIndexHashmapName := "CAI3G - Login index Hashmap";
  const charstring c_EPTF_CAI3G_portIndexHashmapName := "CAI3G - Port index Hashmap";
  const charstring c_EPTF_CAI3G_entityPropertiesxHashmapName := "CAI3G - Entity Properties HashMap";
  const charstring c_EPTF_CAI3G_myBName :="CAI3G Behavior";
  const charstring c_EPTF_CAI3G_statName_CAI3GFaults :="Number of CAI3G Faults in CAI3G component";
  const charstring c_EPTF_CAI3G_statName_HTTPFaults :="Number of HTTP Faults in CAI3G component";
  const charstring c_EPTF_CAI3G_statName_ConcurrentSessions :="Number of Concurrent Sessions in CAI3G component";
  const charstring c_EPTF_CAI3G_statName_Creates :="Number of CAI3G Creates";
  const charstring c_EPTF_CAI3G_statName_GETs :="Number of CAI3G GETs";
  const charstring c_EPTF_CAI3G_statName_SETs :="Number of CAI3G SETs";
  const charstring c_EPTF_CAI3G_statName_Deletes :="Number of CAI3G Deletes";
  const charstring c_EPTF_CAI3G_statName_Requests :="Number of CAI3G Requests";
  const charstring c_EPTF_CAI3G_statName_CreateRate :="Rate of CAI3G Creates";
  const charstring c_EPTF_CAI3G_statName_GETRate :="Rate of CAI3G GETs";
  const charstring c_EPTF_CAI3G_statName_SETRate :="Rate of CAI3G SETs";
  const charstring c_EPTF_CAI3G_statName_DeleteRate :="Rate of CAI3G Deletes";
  const charstring c_EPTF_CAI3G_templName_CreateHeader :="CREATE_HEADER";
  const charstring c_EPTF_CAI3G_templName_CreateBody :="CREATE_BODY";
  const charstring c_EPTF_CAI3G_templName_GetHeader :="GET_HEADER";
  const charstring c_EPTF_CAI3G_templName_GetBody :="GET_BODY";
  const charstring c_EPTF_CAI3G_templName_SetHeader :="SET_HEADER";
  const charstring c_EPTF_CAI3G_templName_SetBody :="SET_BODY";
  const charstring c_EPTF_CAI3G_templName_DeleteHeader :="DELETE_HEADER";
  const charstring c_EPTF_CAI3G_templName_DeleteBody :="DELETE_BODY";
  const charstring c_EPTF_CAI3G_templName_LoginHeader :="LOGIN_HEADER";
  const charstring c_EPTF_CAI3G_templName_LoginBody :="LOGIN_BODY";
  const charstring c_EPTF_CAI3G_templName_LogoutHeader :="LOGOUT_HEADER";
  const charstring c_EPTF_CAI3G_templName_LogoutBody :="LOGOUT_BODY"
}

group Diameter{
  const charstring c_LGen_DiameterBase_Behaviour_name := 
  "DiameterBase_LGen_Behaviour";
  const charstring c_DiameterBase_inputName_getCER := "Received CER";
  const charstring c_DiameterBase_inputName_getCEA := "Received CEA";
  const charstring c_DiameterBase_inputName_getDWR := "Received DWR";
  const charstring c_DiameterBase_inputName_getDWA := "Received DWA";
  const charstring c_DiameterBase_inputName_getDPR := "Received DPR";
  const charstring c_DiameterBase_inputName_getDPA := "Received DPA";
  
  const charstring c_DiameterBase_inputName_getACR := "Received ACR";
  const charstring c_DiameterBase_inputName_getACA := "Received ACA";
  
  const charstring c_DiameterBase_inputName_connInitialized := "Connection is initialized";
  const charstring c_DiameterBase_inputName_connOpened := "Peer conn. opened";
  const charstring c_DiameterBase_inputName_connClosed := "Peer conn. closed";
  const charstring c_DiameterBase_inputName_getRequest := "Received Diameter request";
  const charstring c_DiameterBase_inputName_getAnswer := "Received Diameter answer";
  
  const charstring c_DiameterBase_inputName_timeoutTw := "Timeout: Tw";
  const charstring c_DiameterBase_inputName_connTimeout := "Connection timeout"
  const charstring c_DiameterBase_inputName_missingAVP := "Missing AVP";
  const charstring c_DiameterBase_inputName_unknownUser := "Unknown user";
  const charstring c_DiameterBase_inputName_identitiesDontMatch := "Identities don't match";
  const charstring c_DiameterBase_inputName_unknownHopByHop := "Unknown hop-by-hop id";
  const charstring c_LGen_Diameter3GPP_Behaviour_name := 
  "Diameter3gpp_LGen_Behaviour";
  const charstring c_Diameter3gpp_eTypeName_3gpp := "3gpp"
  const charstring c_DiameterHSS_handleUnknowUser := "handle unknowUser";
  const charstring c_Diameter3gpp_inputName_getUAR := "received UAR";
  const charstring c_Diameter3gpp_inputName_getUAA := "received UAA";
  const charstring c_Diameter3gpp_inputName_getSAR := "received SAR";
  const charstring c_Diameter3gpp_inputName_getSAA := "received SAA";
  const charstring c_Diameter3gpp_inputName_getLIR := "received LIR";
  const charstring c_Diameter3gpp_inputName_getLIA := "received LIA";
  const charstring c_Diameter3gpp_inputName_getMAR := "received MAR";
  const charstring c_Diameter3gpp_inputName_getMAA := "received MAA";
  const charstring c_Diameter3gpp_inputName_getRTR := "received RTR";
  const charstring c_Diameter3gpp_inputName_getRTA := "received RTA";
  const charstring c_Diameter3gpp_inputName_getPPR := "received PPR";
  const charstring c_Diameter3gpp_inputName_getPPA := "received PPA";
  const charstring c_Diameter3gpp_inputName_getRAR := "received RAR";
  const charstring c_Diameter3gpp_inputName_getRAA := "received RAA";
  const charstring c_Diameter3gpp_inputName_getLUR := "received LUR";
  const charstring c_Diameter3gpp_inputName_getLUA := "received LUA";
  const charstring c_Diameter3gpp_inputName_getUDR := "received UDR";
  const charstring c_Diameter3gpp_inputName_getUDA := "received UDA";
  const charstring c_Diameter3gpp_inputName_getEricssonMAR := "received Ericsson specific MAR";
  const charstring c_Diameter3gpp_inputName_getEricssonMAA := "received Ericsson specific MAA";
  const charstring c_Diameter3gpp_inputName_getSNR := "received SNR"
  const charstring c_Diameter3gpp_inputName_getPNR := "received PNR"
  const charstring c_Diameter3gpp_inputName_getPUR := "received PUR"
  
  const charstring c_LGen_DiameterCC_Behaviour_name := 
  "DiameterCC_LGen_Behaviour";
  const charstring c_DiameterCC_inputName_getCCR := "received CCR"
  
}

group h248{
  const charstring c_EPTF_H248_LGen_fsmName := "FSM for H248 calls";
  const charstring c_LGen_H248_Behaviour_name  := "H248_LGen_Behaviour";
  
  const integer c_EPTF_H248_LGen_inputIdx_recvMessage := 0;
  const charstring c_EPTF_H248_LGen_inputName_recvMessage := 
  "Received H248 PDU";
  
  const integer c_EPTF_H248_LGen_inputIdx_sendInitialSC := 1;
  const charstring c_EPTF_H248_LGen_inputName_sendInitialSC := 
  "Trigger occured to send initial Service Change";
  
  const integer c_EPTF_H248_LGen_inputIdx_sendDisconnectSC := 2;
  const charstring c_EPTF_H248_LGen_inputName_sendDisconnectSC := 
  "Trigger occured to send disconnect Service Change";
  
  const integer c_EPTF_H248_LGen_inputIdx_sendEventNotify := 3;
  const charstring c_EPTF_H248_LGen_inputName_sendEventNotify := 
  "Trigger occured sending Event Notify";
  
  const integer c_EPTF_H248_LGen_inputIdx_reconnect := 4;
  const charstring c_EPTF_H248_LGen_inputName_reconnect := 
  "Trigger occured to try reconnecting to SUT";
  
  const integer c_EPTF_H248_LGen_inputIdx_sendServiceChange := 5;
  const charstring c_EPTF_H248_LGen_inputName_sendServiceChange := 
  "Trigger occured to send user defined Service Change";
}

group xcap{
  const charstring c_EPTF_XCAP_myBName := 
  "XCAP Behavior";
  
  const integer c_EPTF_XCAP_inputIdx_start := 0;
  const charstring c_EPTF_XCAP_inputName_start := "Send XCAP message";
  const integer c_EPTF_XCAP_inputIdx_responseSuccessful := 1;
  const charstring c_EPTF_XCAP_inputName_responseSuccessful := "XCAP Message arrived, Result Code Ok";
  const integer c_EPTF_XCAP_inputIdx_responseUnsuccessful := 2;
  const charstring c_EPTF_XCAP_inputName_responseUnsuccessful := "XCAP Message arrived, Result Code Not Ok";
  const integer c_EPTF_XCAP_inputIdx_responseError := 3;
  const charstring c_EPTF_XCAP_inputName_responseError := "XCAP Error in message sending";
  const integer c_EPTF_XCAP_inputIdx_responseUnauthorized := 4;
  const charstring c_EPTF_XCAP_inputName_responseUnauthorized := "XCAP Message arrived, Unauthorized";
  const integer c_EPTF_XCAP_inputIdx_retryAfter := 5;
  const charstring c_EPTF_XCAP_inputName_retryAfter := "XCAP Message arrived, Retry-After";
  const integer c_EPTF_XCAP_inputIdx_responseSocketError := 6;
  const charstring c_EPTF_XCAP_inputName_responseSocketError := "XCAP Socket Error";
}

type component EPTF_LGenBasePerformTest_MTAS_CT extends EPTF_LGenBaseStats_CT, TestResultGen_CT{
  var integer v_EPTF_CAI3G_myBIdx := -1
  var integer v_DiameterBase_myBIdx := -1;
  var integer v_Diameter3gpp_myBIdx := -1;
  
  var integer c_DiameterBase_inputIdx_getCER := -1;
  var integer c_DiameterBase_inputIdx_getCEA := -1;
  var integer c_DiameterBase_inputIdx_getDWR := -1;
  var integer c_DiameterBase_inputIdx_getDWA := -1;
  var integer c_DiameterBase_inputIdx_getDPR := -1;
  var integer c_DiameterBase_inputIdx_getDPA := -1;
  var integer c_DiameterBase_inputIdx_getACR := -1;
  var integer c_DiameterBase_inputIdx_getACA := -1;
  var integer c_DiameterBase_inputIdx_connInitialized := -1;
  var integer c_DiameterBase_inputIdx_connOpened := -1;
  var integer c_DiameterBase_inputIdx_connClosed := -1;
  var integer c_DiameterBase_inputIdx_getRequest := -1;
  var integer c_DiameterBase_inputIdx_getAnswer := -1;
  var integer c_DiameterBase_inputIdx_timeoutTw := -1; //watchdog timer timeout
  var integer c_DiameterBase_inputIdx_connTimeout := -1;
  var integer c_DiameterBase_inputIdx_missingAVP := -1;
  var integer c_DiameterBase_inputIdx_unknownUser := -1;
  var integer c_DiameterBase_inputIdx_identitiesDontMatch := -1;
  var integer c_DiameterBase_inputIdx_unknownHopByHop := -1;
  
  var integer c_Diameter3gpp_inputIdx_getUDR := -1;
  var integer c_Diameter3gpp_inputIdx_getSNR := -1;
  var integer c_Diameter3gpp_inputIdx_getPNR := -1;
  var integer c_Diameter3gpp_inputIdx_getPUR := -1;
  
  var integer v_DiameterCC_myBIdx;
  var integer c_DiameterCC_inputIdx_getCCR := -1;
  
  var integer v_H248_LGen_myBIdx := -1;
  var integer v_EPTF_XCAP_myBIdx := -1
}

}  // end of module
