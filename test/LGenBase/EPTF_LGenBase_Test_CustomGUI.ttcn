///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// 
//  Purpose:
//    This module provides functions for testing R11 functionalities and bugfixes
//    of LGenBase
// 
//  Module depends on:
//    -
// 
//  Current Owner:
//    Janos Zoltan Svaner (EJNOSVN)
// 
//  Last Review Date:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_LGenBase_Test_CustomGUI
// [.objid{ itu_t(0) identified_organization(4) etsi(0)
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_LGenBaseStats_Definitions all;
import from EPTF_CLL_LGenBaseStats_Functions all;
import from EPTF_LGenBase_Test_Functions all;
import from EPTF_CLL_LGenBase_StepFunctions all;
import from EPTF_LGenBase_Test_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_LGenBase_Test_TestcasesR3 all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from TCCMaths_Functions all;
import from EPTF_CLL_LGenBase_ExternalFunctions all;
import from EPTF_CLL_StatMeasure_Definitions all;
import from EPTF_CLL_StatMeasure_Functions { function f_EPTF_StatMeasure_getStat_max };
import from EPTF_CLL_RBTScheduler_Functions all;
import from EPTF_CLL_LGenBase_PhaseDefinitions all;
import from EPTF_CLL_LGenBase_PhaseFunctions all;
import from EPTF_CLL_LGenBase_PhaseConfigFunctions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_LGenBase_TemplateDefinitions all;
import from EPTF_LGenBase_Test_TestcasesR11 all;
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from paramDemoAppLib_Definitions all;
import from paramDemoAppLib_Functions all;

modulepar EPTF_LGenBase_TcMgmt_EntityTypeDeclaratorList 	 tsp_modulePar_entityTypes := {};
modulepar EPTF_LGenBase_TcMgmt_EntityGrpDeclaratorList 		 tsp_modulePar_entityGroups := {};
modulepar EPTF_LGenBase_TcMgmt_CompactFsmTableDeclaratorList tsp_modulePar_compactFsmTables := {};
modulepar EPTF_LGenBase_TcMgmt_tcTypeDeclaratorList 		 tsp_modulePar_trafficCases := {};
modulepar EPTF_LGenBase_TcMgmt_ScenarioDeclaratorList  		 tsp_modulePar_scenarios := {};
modulepar EPTF_LGenBase_TcMgmt_Scenarios2GrpList       		 tsp_modulePar_Scenarios2Grps := {};

modulepar float tsp_testDuration := 42.0;
modulepar integer tsp_numEntities := 100;

  type component EPTF_LGenBase_UIHandlerTest_CT extends LGenBaseTest_CT, EPTF_UIHandler_CT {
    var boolean v_ready := false;   
  }
  
function f_LGenBaseRegisterTest_groupRegisterFinishFunction2_FT(in integer pl_idx)
runs on EPTF_LGenBase_UIHandlerTest_CT{
  log("--- Traffic case ",f_EPTF_LGenBase_getTcUniqueNameByTcIdx(pl_idx)," has finished.")
}
  
function f_LGenBaseRegisterTest_doTrafficFinishedFn(in integer pl_idx)
runs on EPTF_LGenBase_UIHandlerTest_CT{
  log("--- DoTraffic_finished of TC ",f_EPTF_LGenBase_getTcUniqueNameByTcIdx(pl_idx))
}

function f_EPTF_LGenBase_Test_DataSourceClientReady(
  in charstring pl_source,
  in charstring pl_ptcName)
runs on EPTF_LGenBase_UIHandlerTest_CT{
  v_ready := true;
}

///////////////////////////////////////////////////////////
//  Testcase: tc_EPTF_LGenBaseStatsUIRegister
// 
//  Purpose:
//    Tests the display and GUI management of the register-reregister-traffic generation 
//    scenario.
///////////////////////////////////////////////////////////
testcase tc_EPTF_LGenBase_CustomGui() 
runs on EPTF_LGenBase_UIHandlerTest_CT{
  const charstring c_LGenBase_DS_guiXmlName := "EPTF_LGenBase_Test_CustomGUI.xml";
  f_EPTF_UIHandler_init_CT("LGenBase_CustomGui",true);
  f_EPTF_DataSource_registerReadyCallback(refers(f_EPTF_LGenBase_Test_DataSourceClientReady ));  
  f_EPTF_LGenBase_init("LGenBase_CustomGui", 0, "LGenBase_Entity#",tsp_LGenBase_BustCalcMethod,tsp_LGenBase_extTemplLoadList,null,self,true);  
  v_dummyInt := f_EPTF_LGenBase_declareBehaviorType("bhTest1", -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareBehaviorType("bhTest2", -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_InvalidEvent", {testStepFunction := refers(f_LGenBaseTest_InvalidEvent)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_register", {testStepFunction := refers(f_LGenBaseTest_register)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_reRegister", {testStepFunction := refers(f_LGenBaseTest_reRegister)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_doTraffic", {testStepFunction := refers(f_LGenBaseTest_doTraffic)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_stopReRegisterIdle", {testStepFunction := refers(f_LGenBaseTest_stopReRegisterIdle)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_stopReRegisterBusy", {testStepFunction := refers(f_LGenBaseTest_stopReRegisterBusy)});
  
  f_EPTF_LGenBase_declareFunction("successGroupRegister", {customFinishFunction := refers(f_LGenBaseRegisterTest_groupRegisterFinishFunction2_FT)});
  f_EPTF_LGenBase_declareFunction("doTrafficFinished",    {customFinishFunction := refers(f_LGenBaseRegisterTest_doTrafficFinishedFn)});
  f_EPTF_LGenBase_TcMgmt_declareEntityTypes(tsp_modulePar_entityTypes)
  f_EPTF_LGenBase_TcMgmt_declareEntityGroups(tsp_modulePar_entityGroups);
  f_EPTF_LGenBase_TcMgmt_declareCompactFsmTables(tsp_modulePar_compactFsmTables);
  f_EPTF_LGenBase_TcMgmt_declareTrafficCases(tsp_modulePar_trafficCases);
  f_EPTF_LGenBase_TcMgmt_declareScenarios(tsp_modulePar_scenarios);
  f_EPTF_LGenBase_TcMgmt_createScenarios2EntityGroup(tsp_modulePar_Scenarios2Grps);
  f_EPTF_LGenBase_declareWeightedScenarioType({"scWeight",false,9.5,false,{false},{
        {
          tcName := "reregister",
          tcWeight := 0.25,
          enableEntities := true,
          enable := true,
          ranges := {},
          params := {},
          groupFinishConditions := c_EPTF_LGenBase_TcMgmt_emptyGroupFinishConditions2,
          entityFinishConditions := c_EPTF_LGenBase_TcMgmt_emptyEntityFinishConditions,
          entityFinishActions := {},
          tcFinishActions := {}
        },
        {
          tcName := "doTraffic",
          tcWeight := 0.75,
          enableEntities := true,
          enable := true,
          ranges := {},
          params := {},
          groupFinishConditions := c_EPTF_LGenBase_TcMgmt_emptyGroupFinishConditions2,
          entityFinishConditions := c_EPTF_LGenBase_TcMgmt_emptyEntityFinishConditions,
          entityFinishActions := {},
          tcFinishActions := {}
        }
      }})
  f_EPTF_LGenBase_createScenario2EntityGroup({"eg1","scWeight"}, false);
  //Init GUI
  f_EPTF_LGenBase_debugLogListeners()
  
  f_EPTF_LGenBase_startTrafficCase("eg0", "sc1", "register");
  f_EPTF_LGenBase_startTrafficCase("eg0", "sc1", "reregister");

  timer T_guard, T_alt;
  T_guard.start( tsp_testDuration );
  T_alt.start( 0.0 );
  alt{
    [] T_guard.timeout{
      setverdict(fail,"Timeout during config");
      f_EPTF_Base_stopAll();
    }
    [v_ready] T_alt.timeout{}
  };
  f_EPTF_UIHandler_createGUIFromFile(c_LGenBase_DS_guiXmlName);
 
  timer T_dummy;
  T_dummy.start( tsp_testDuration );
  T_dummy.timeout;
   
  f_EPTF_Base_cleanup_CT();
}
 
  control{
  	execute(tc_EPTF_LGenBase_CustomGui());
  }

}  // end of module
