///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLI_Test_Definitions
// 
//  Purpose:
//    This module contains the implementation of generic EPTF_CLL_CLI functions.
// 
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Commmon_Functions>
// 
//  Current Owner:
//    ethbaat
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    This module contains the functions for testing EPTF_CLL_CLI
//
//
///////////////////////////////////////////////////////////////

module EPTF_CLI_Test_Definitions {

import from TELNETasp_PortType all;
import from EPTF_CLL_Base_Definitions all;


group TerminalEmulation {
  
  //============================================
  //  Datatypes
  //============================================
  //Command and the expected answer
  type record CmdAndAnswer { 
    charstring command,
    charstring expectedAnswer 
  };

  type record of CmdAndAnswer CmdAndAnswerList;

  //============================================
  //  Component definitions
  //============================================
  type component EPTF_CLI_Test_MTC  extends EPTF_Base_CT { };

  type component EPTF_CLI_Test_terminal extends EPTF_Base_CT{  
    private port TELNETasp_PT EPTF_CLI_Test_terminal_PCO;
    private var charstring v_EPTF_CLI_Test_terminal_rcvdMsg:="";
    private var integer v_EPTF_CLI_Test_terminal_rcvdInt:=-1;
    private var boolean v_EPTF_CLI_Test_terminal_initiated:=false;
    private var default vd_alt:=null;
    private var charstring v_EPTF_CLI_Test_terminal_prompt:="";

    //private var 
  };

  type component EPTF_CLI_Test_display extends EPTF_Base_CT {
    private port TELNETasp_PT EPTF_CLI_Test_display_PCO;    
    private var charstring v_EPTF_CLI_Test_display_rcvdMsg:="";
    private var integer v_EPTF_CLI_Test_display_rcvdInt:=-1;
    private var boolean v_EPTF_CLI_Test_display_initiated:=false;
    private var default vd_alt:=null;
    private var charstring v_EPTF_CLI_Test_display_prompt:="";
  };

  //============================================
  // Templates
  //============================================
  template charstring t_allalias_none:=pattern "*No aliases are registered yet! See 'help alias'!*";
  
  template charstring t_alias_illegalArg:=pattern "*Illegal argument error!\n*";
  
  template charstring t_alias_ok:=pattern "*Alias registered! Command: *, alias: *";
  
  template charstring t_alias_used:=pattern "*Alias already used for command: help*";
  
  template charstring t_lsalias:=pattern "*Alias h is registered to command: help*";
  
  template charstring t_aliasNotReg:=pattern "*is not a registered alias!*";
  
  template charstring t_allalias:=pattern "*List of all aliases registered in the system\(alias - command\): \n\th - help*";
  
  template charstring t_unalias:=pattern "*Alias removed!*";
  
  template charstring t_help_answer:= pattern
    "EPTF Command Line Interface version: *\nUsage: <command> \[arguments\]\n\nExecutes the given command with the specified arguments.\nLines starting with '\#' or '//' are treated as comments and are ignored.\n\nAvailable commands:\n\n  alias - creates an alias to a command\n  unalias - removes an alias\n  lsalias - prints information about an alias\n  allalias - lists all alias - command pair\n  help - print this help message\n  exit - close the terminal\n  quit - close the terminal\n  stop - terminate execution, stop all TitanSim components\n  stopAll - terminate execution, stop all TitanSim components\n  echo - echoes its arguments\n\nFor more detailed information on a command try:\nhelp <command>";

  template charstring t_help_help_answer:= pattern
    "Help information about command: help\nSyntax: help \[<command name>\|help\]\n  Parameters:\n  <command name> - the name of the command to get help about.\n  help - prints this help message.\n\nIf called without arguments general help message is printed\n\nThis command is owned by component CLI_server \([\d]#(1,)\)";

  template charstring t_help_exit_answer:= pattern "Help information about command: exit\nSyntax: exit\n  exit - exit from the terminal session.\n\nThis command is owned by component CLI_server \([\d]#(1,)\)";

  template charstring t_help_quit_answer:= pattern "Help information about command: quit\nSyntax: quit\n  quit - exit from the terminal session.\n\nThis command is owned by component CLI_server \([\d]#(1,)\)\nThe command name is case insensitive!"

  template charstring t_help_stop_answer:= pattern "Help information about command: stop\nSyntax: stop\n  stop - stops all TitanSim components and terminates test execution.\n\nThis command is owned by component CLI_server \([\d]#(1,)\)\nThe command name is case insensitive!"   
  
  template charstring t_help_stopAll_answer:= pattern "Help information about command: stopAll\nSyntax: stopAll\n  stopAll - stops all TitanSim components and terminates test execution.\n\nThis command is owned by component CLI_server \([\d]#(1,)\)"
  
  template charstring t_help_echo_answer:=  pattern "Help information about command: echo\n  echo - echoes its arguments\n\nThis command is owned by component CLI_client \([\d]#(1,)\)\nThe command name is case insensitive!\n\nFor more detailed information try:\necho help"
  
  template charstring t_help_alias_answer:=  pattern "*Help information about command: alias\nSyntax: alias <command name> <alias>\n  Parameters:\n  <command name> - the name of the command - or another alias - for which the alias will be registered.\n  <alias> - a string without any whitespaces that can be used insted of <command name>.\n  It is possible to create alias for command parts also, but you have to signal them when used.\n  example: alias <xml format of data description> mydata\n  ds get $mydata$\n\nThis command is owned by component*"
  
  template charstring t_help_unalias_answer:=  pattern "*Help information about command: unalias\nSyntax: unalias <alias>\n  Parameters:\n  <alias> - Alias to be removed.\n\nThis command is owned by component*";
  
  template charstring t_help_lsalias_answer:=  pattern "*Help information about command: lsalias\nSyntax: lsalias <alias>\n  Parameters:\n  <alias> - Alias to be listed.\n\nThis command is owned by component*";
  
  template charstring t_help_allalias_answer:=  pattern "*Help information about command: allalias\nSyntax: allalias\n  List all aliases registered in the system.\nThis command is owned by component*";
  
  template charstring t_help_echo_answer2:="echo - prints out its parameters\nThis is the detailed help message of the echo command\nIf called with \"ERROR!\" argument it exits with code 1\nIf called with -t <duration> as the first argument\nthe result is printed after that time has passed (in seconds)."

  template charstring t_help_ECHO2_answer:= "echo - prints out its parameters\nThis is the detailed help message of the echo command\nIf called with \"ERROR!\" argument it exits with code 1\nIf called with -t <duration> as the first argument\nthe result is printed after that time has passed (in seconds)."

}//group TerminalEmulation


}//module
