<!--
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
-->
<!DOCTYPE TITAN_GUI_FileGroup_file>
<File_Group name="EPTF_Applibs" >
    <File_Group name="EPTF_Applib_AIN" >
        <File path="../../../EPTF_Applib_AIN_CNL113594/src/LoadGen/EPTF_AIN_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_AIN_CNL113594/src/LoadGen/EPTF_AIN_LGen_Functions.ttcn" />
        <File path="../../../EPTF_Applib_AIN_CNL113594/src/Logger/EPTF_AIN_Logger_Functions.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_CAI3G" >
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/LoadGen/EPTF_CAI3G_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/LoadGen/EPTF_CAI3G_Functions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/LoadGen/EPTF_CAI3G_Session_DataTypeRingBuffer_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/LoadGen/EPTF_CAI3G_Templates.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/LoadGen/EPTF_CAI3G_Transaction_DataTypeRingBuffer_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/Logger/EPTF_CAI3G_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/Logger/EPTF_CAI3G_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/Transport/EPTF_CAI3G_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/src/Transport/EPTF_CAI3G_Transport_Functions.ttcn" />
        <File path="../../../EPTF_Applib_CAI3G_CNL113566/demo/EPTF_CAI3G_Demo.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_DNS" >
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/LoadGen/EPTF_DNS_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/LoadGen/EPTF_DNS_LGen_Functions.ttcn" />
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/Logger/EPTF_DNS_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/Logger/EPTF_DNS_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/Transport/EPTF_DNS_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_DNS_CNL113597/src/Transport/EPTF_DNS_Transport_Functions.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_Diameter" >
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_Diameter3gppSh_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_Diameter3gppSh_LGen_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_DiameterBase_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_DiameterBase_LGen_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_DiameterCC_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/LoadGen/EPTF_DiameterCC_LGen_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/Logger/EPTF_Diameter_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/Logger/EPTF_Diameter_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/Transport/EPTF_Diameter_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/src/Transport/EPTF_Diameter_Transport_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/demo/DIAMETER_Types.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/demo/IMS_Test_Diameter_ComponentType.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/demo/IMS_Test_Diameter_Functions_Redirect.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/demo/IMS_Test_Diameter_Templates.ttcn" />
        <File path="../../../EPTF_Applib_Diameter_CNL113521/demo/IMS_Test_Parameters.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_GGSNSim" >
        <File path="../../../EPTF_Applib_GGSNSim_CNL113562/src/EPTF_GGSNSim_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_GGSNSim_CNL113562/src/EPTF_GGSNSim_EncDec.cc" />
        <File path="../../../EPTF_Applib_GGSNSim_CNL113562/src/EPTF_GGSNSim_Functions.ttcn" />
        <File path="../../../EPTF_Applib_GGSNSim_CNL113562/src/EPTF_GGSNSim_Interface_Definitions.asn" />
        <File path="../../../EPTF_Applib_GGSNSim_CNL113562/demo/GGSNSim_demo.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_H248" >
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_Database_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_Database_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_LGen_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_SDP_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_Scheduler_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_Scheduler_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/LoadGen/EPTF_H248_Templates.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Logger/EPTF_H248_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Logger/EPTF_H248_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Mapping_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Mapping_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Router_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Router_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_API_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_API_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_Local_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_Local_Function.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_RemoteClient_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_RemoteClient_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_RemoteServer_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_RemoteServer_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_SCTP_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_SCTP_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_UDP_Definitions.ttcnpp" />
        <File path="../../../EPTF_Applib_H248_CNL113523/src/Transport/EPTF_H248_Transport_UDP_Functions.ttcnpp" />
    </File_Group>
    <File_Group name="EPTF_Applib_INAP" >
        <File path="../../../EPTF_Applib_INAP_CNL113596/src/LoadGen/EPTF_INAP_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_INAP_CNL113596/src/LoadGen/EPTF_INAP_LGen_Functions.ttcn" />
        <File path="../../../EPTF_Applib_INAP_CNL113596/src/Logger/EPTF_INAP_Logger_Functions.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_MAP" >
        <File path="../../../EPTF_Applib_MAP_CNL113595/src/LoadGen/EPTF_MAP_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MAP_CNL113595/src/LoadGen/EPTF_MAP_LGen_Functions.ttcn" />
        <File path="../../../EPTF_Applib_MAP_CNL113595/src/Logger/EPTF_MAP_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_MAP_CNL113595/demo/MAP_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MAP_CNL113595/demo/MAP_LGen_Functions.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_MLSim" >
        <File path="../../../EPTF_Applib_MLSim_CNL113568/src/EPTF_MLSim_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_MLSim_CNL113568/src/EPTF_MLSim_General_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MLSim_CNL113568/src/EPTF_MLSim_Request_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MLSim_CNL113568/src/EPTF_MLSim_Response_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MLSim_CNL113568/demo/EPTF_MLSim_Demo.ttcn" />
        <File path="../../../EPTF_Applib_MLSim_CNL113568/demo/MLSIM_test.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_MSRP" >
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/LoadGen/EPTF_MSRP_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/LoadGen/EPTF_MSRP_FSMs.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/LoadGen/EPTF_MSRP_Functions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/Logger/EPTF_MSRP_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/Logger/EPTF_MSRP_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/Transport/EPTF_MSRP_EncDec.cc" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/Transport/EPTF_MSRP_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/src/Transport/EPTF_MSRP_Transport_Functions.ttcn" />
        <File path="../../../EPTF_Applib_MSRP_CNL113564/demo/EPTF_MSRP_demo.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_Radius" >
        <File path="../../../EPTF_Applib_Radius_CNL113567/src/EPTF_RadiusAcct_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Radius_CNL113567/src/EPTF_RadiusAcct_Functions.ttcn" />
        <File path="../../../EPTF_Applib_Radius_CNL113567/src/EPTF_RadiusBase_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_Radius_CNL113567/src/EPTF_RadiusBase_Functions.ttcn" />
        <File path="../../../EPTF_Applib_Radius_CNL113567/demo/RadiusServer.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_SIP" >
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Common_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Common_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_CreateRemove_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Dialog_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Dialog_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_EventNotification_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_EventNotification_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Events.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_LGen_ExternalFunctions.cc" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_LGen_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_MessageCreator_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_MessageHandler_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Publish_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Publish_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_StateHandler_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Templates.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_TestSteps.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Transaction_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_Transaction_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_UserDatabase_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/LoadGen/EPTF_SIP_UserDatabase_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/Logger/EPTF_SIP_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/Logger/EPTF_SIP_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/Transport/EPTF_SIP_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/src/Transport/EPTF_SIP_Transport_Functions.ttcnpp" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_BasicCall_Functions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Cancel.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Common_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Common_Functions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Message_Functions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Refer.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_RegDereg_Functions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_SIP_CNL113522/demo/SIP_Demo_Update.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_VXML" >
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/LoadGen/EPTF_VXML_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/LoadGen/EPTF_VXML_Functions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/LoadGen/EPTF_VXML_Templates.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/Logger/EPTF_VXML_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/Logger/EPTF_VXML_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/Transport/EPTF_VXML_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/src/Transport/EPTF_VXML_Transport_Functions.ttcn" />
        <File path="../../../EPTF_Applib_VXML_CNL113565/demo/VXML_demo.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_XCAP" >
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/LoadGen/EPTF_XCAP_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/LoadGen/EPTF_XCAP_Functions.ttcn" />
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/Logger/EPTF_XCAP_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/Logger/EPTF_XCAP_Logger_Functions.ttcn" />
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/Transport/EPTF_XCAP_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_Applib_XCAP_CNL113534/src/Transport/EPTF_XCAP_Transport_Functions.ttcn" />
    </File_Group>
    <File_Group name="EPTF_Applib_CommonTrasport_TCAP" >
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/LoadGen/EPTF_TCAP_LGen_Definitions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/LoadGen/EPTF_TCAP_LGen_Functions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/LoadGen/EPTF_TCAP_UIHandler_Definitions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/LoadGen/EPTF_TCAP_UIHandler_Functions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Logger/EPTF_TCAP_Logger_Definitions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Logger/EPTF_TCAP_Logger_Functions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Transport/EPTF_TCAP_Transport_Definitions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Transport/EPTF_TCAP_Transport_Functions.ttcnpp" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Transport/EPTF_Transport_CommPort_TCAP_Definitions.ttcn" />
        <File path="../../../EPTF_CommonTransport_TCAP_CNL113592/src/Transport/EPTF_Transport_CommPort_TCAP_Functions.ttcn" />
    </File_Group>
    <File_Group name="TestPorts and ProtocolModules for AppLibs" >
        <File_Group name="AIN" >
            <File path="../../../../ProtocolModules/AIN_v2.0_CNL113556/src/AIN_EncDec.cc" />
            <File path="../../../../ProtocolModules/AIN_v2.0_CNL113556/src/AIN_Errors.asn" />
            <File path="../../../../ProtocolModules/AIN_v2.0_CNL113556/src/AIN_Operations.asn" />
            <File path="../../../../ProtocolModules/AIN_v2.0_CNL113556/src/AIN_Parameters.asn" />
            <File path="../../../../ProtocolModules/AIN_v2.0_CNL113556/src/AIN_Types.ttcn" />
        </File_Group>
        <File_Group name="DNS" >
            <File path="../../../../ProtocolModules/DNS_CNL113429/src/DNS_EncDec.cc" />
            <File path="../../../../ProtocolModules/DNS_CNL113429/src/DNS_Types.ttcn" />
        </File_Group>
        <File_Group name="General_Types" >
            <File path="../../../../ProtocolModules/COMMON/src/General_Types.ttcn" />
        </File_Group>
        <File_Group name="Ericsson_INAP_CS1plus_CNL113356" >
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_ApplicationContexts.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_Codes.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_DataTypes.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_EncDec.cc" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_Errors.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_Operations.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_CS1_Types.ttcn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Core_INAP_PDU_Defs.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_ApplicationContexts.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_Codes.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_Datatypes.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_Errors.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_Operations.asn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/Ericsson_INAP_CS1plus_Types.ttcn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_CS1plus_ANSIasp_Types.ttcn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_CS1plus_Detailed_EncDec.cc" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_CS1plus_EncDec.cc" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_CS1plus_ITUasp_Types.ttcn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_CS1plus_Types.ttcn" />
            <File path="../../../../ProtocolModules/Ericsson_INAP_CS1plus_CNL113356/src/INAP_PDU_Defs.asn" />
        </File_Group>
        <File_Group name="H248" >
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_EncDec.cc" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_la.cc" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_la.l" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_p.cc" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_p.hh" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_p.y" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_p_types.hh" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_SDP_EncDec.cc" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_SDP_Types.ttcn" />
            <File path="../../../../ProtocolModules/H248_v2_CNL113424/src/H248_Types.ttcn" />
        </File_Group>
        <File_Group name="HTTPmsg_Types" >
            <File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_Types.ttcn" />
        </File_Group>
        <File_Group name="MAP_v6.11.0" >
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_Types.ttcn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_SupplementaryServiceOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_ApplicationContexts.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_BS_Code.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_CallHandlingOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_CH_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_CommonDataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_DialogueInformation.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_ER_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_Errors.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_ExtensionDataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_GR_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_Group_Call_Operations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_LCS_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_LocationServiceOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_MobileServiceOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_MS_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_OM_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_OperationAndMaintenanceOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_PDU_Defs.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_Protocol.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_SecureTransportOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_ShortMessageServiceOperations.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_SM_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_SS_Code.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_SS_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_ST_DataTypes.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MAP_TS_Code.asn" />
            <File path="../../../../ProtocolModules/MAP_v6.11.0_CNL113500/src/MobileDomainDefinitions.asn" />
        </File_Group>
        <File_Group name="MLSIM" >
            <File path="../../../../TestPorts/MLSIMasp_CNL113538/src/MLSIMasp_PT.cc" />
            <File path="../../../../TestPorts/MLSIMasp_CNL113538/src/MLSIMasp_PT.hh" />
	    <File path="../../../../TestPorts/MLSIMasp_CNL113538/src/MLSIMasp_functions.ttcn" />
	    <File path="../../../../TestPorts/MLSIMasp_CNL113538/src/MLSIMasp_Types.ttcn" />
	    <File path="../../../../TestPorts/MLSIMasp_CNL113538/src/MLSIMasp_PortType.ttcn" />
        </File_Group>
        <File_Group name="MSRP PM" >
            <File path="../../../../ProtocolModules/MSRP_CNL113467/src/MSRP_EncDec.cc" />
            <File path="../../../../ProtocolModules/MSRP_CNL113467/src/MSRP_Templates.ttcn" />
            <File path="../../../../ProtocolModules/MSRP_CNL113467/src/MSRP_Types.ttcn" />
        </File_Group>
        <File_Group name="Radius" >
            <File path="../../../../TestPorts/RADIUSmsg_CNL113311/src/RADIUSmsg_PortType.ttcn" />
            <File path="../../../../TestPorts/RADIUSmsg_CNL113311/src/RADIUSmsg_PT.cc" />
            <File path="../../../../TestPorts/RADIUSmsg_CNL113311/src/RADIUSmsg_PT.hh" />
            <File path="../../../../TestPorts/RADIUSmsg_CNL113311/src/RADIUSmsg_Types.ttcn" />
        </File_Group>
        <File_Group name="ROSE" >
            <File path="../../../../ProtocolModules/ROSE_CNL113369/src/Remote_Operations_Generic_ROS_PDUs.asn" />
            <File path="../../../../ProtocolModules/ROSE_CNL113369/src/Remote_Operations_Information_Objects.asn" />
        </File_Group>
        <File_Group name="SIPmsg_CNL113319" >
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/lex.SIP_parse_.c" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIP_parse.h" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIP_parse_.tab.c" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIP_parse_.tab.h" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIPmsg_PortType.ttcn" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIPmsg_PT.cc" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIPmsg_PT.hh" />
            <File path="../../../../TestPorts/SIPmsg_CNL113319/src/SIPmsg_Types.ttcn" />
        </File_Group>
        <File_Group name="TCAPasp_CNL11334" >
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/Socket_Component.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/Socket_Component.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/SS7Common.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/SS7Common.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/SS7DDefs.h" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/SS7DUtils.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/SS7DUtils.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PortType.ttcn" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT_Daemon_Interface.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT_Daemon_Interface.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT_EIN_Interface.cc" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_PT_EIN_Interface.hh" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPasp_Types.ttcn" />
            <File path="../../../../TestPorts/TCAPasp_CNL113349/src/TCAPPackage.asn" />
        </File_Group>
        <File_Group name="TCCUsefulFunctions_CNL113472" >
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCConversion_Functions.ttcn" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCConversion.cc" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCMessageHandling_Functions.ttcn" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCMessageHandling.cc" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCSecurity_Functions.ttcn" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCSecurity.cc" />
            <File path="../../../../Libraries/TCCUsefulFunctions_CNL113472/src/TCCTemplate_Functions.ttcn" />
        </File_Group>
        <File_Group name="TCAP_CNL113342" >
            <File path="../../../../ProtocolEmulations/TCAP_CNL113342/src/TCAP_ANSI_PDU_Defs.asn" />
        </File_Group>
    </File_Group>
</File_Group>
