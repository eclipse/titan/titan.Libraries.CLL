///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Transport_Test_Definitons
// 
//  Purpose:
//    This module contains the test functions of generic EPTF Transport.
// 
//  Module depends on:
//   <EPTF_CLL_TransportRouting_Definitions> 
//   <EPTF_CLL_TransportMessageBufferManager_Definitions> 
//   <EPTF_CLL_TransportIPL4_Definitions> 
//   <EPTF_CLL_TransportCommPortUDP_Definitions>
//   <IPL4asp_PortType>
//	 <IPL4asp_Types>
//   <EPTF_CLL_Transport_Definitions>
//   <EPTF_CLL_Common_Definitions>
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////

module EPTF_Transport_Test_Definitions {
//=========================================================================
// Import part
//=========================================================================
import from EPTF_CLL_TransportRouting_Definitions all;
import from EPTF_CLL_TransportMessageBufferManager_Definitions all;
import from EPTF_CLL_TransportIPL4_Definitions all;
import from EPTF_CLL_TransportIPL2_Definitions all;
import from IPL4asp_PortType all;
import from EPTF_CLL_Transport_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from IPL4asp_Types all;
import from EPTF_CLL_Base_Definitions all;
import from LANL2asp_PortType all;


//=========================================================================
// Type Definitions
//=========================================================================
type record of octetstring Test_OctetstringList;

modulepar {
  charstring tsp_LocalHostIP := "159.107.193.33"
  charstring tsp_RemoteHostIP := "159.107.193.33"
  integer tsp_LocalHostPort := 10005
  integer tsp_RemoteHostPort := 10006
  float tsp_PerfTestDuration := 10.0;
}

//=========================================================================
// Component Type
//=========================================================================
type component IPL4Demo_CT extends EPTF_TransportIPL4_CT {

}

type component MessageBufferDemo_CT extends EPTF_MessageBufferManager_CT {

}
type component RoutingDemo_CT extends EPTF_Routing_CT {

}
type component EPTF_Transport_Test_CT extends EPTF_Routing_CT, EPTF_TransportIPL4_CT,
                                              EPTF_TransportIPL2_CT, EPTF_Transport_CT {
  port IPL4asp_PT incom;
  var boolean v_Test_inCalled := false;
  var boolean v_Test_outCalled := false;
  var integer v_msgCount := 0;
  var integer v_eventCount := 0;
  var integer v_lastCreatedConnId := -1;
  var float v_lastSendCallRelTimeInSecs := 0.0;  // artf384340
  var float v_countTimeDistanceRelTimeInSecsInReceive := 0.0;
  var float v_countTimeDistanceRelTimeInSecsInCallback := 0.0;
  var octetstring v_setMsg := ''O;
  var octetstring v_recvBuff := ''O;
  var ASP_RecvFrom v_expected_ASP_recvFrom :={
    connId:=-1,
    remName:="",
    remPort:=-1,
    locName:="",
    locPort:=-1,
    proto:={sctp:={
      sinfo_stream :=omit,
   	  sinfo_ppid :=omit,
      remSocks :=omit,
      assocId :=omit
    }},
    userData:=-1,
    msg:=''O}
}

group TemporarlyUnavailableCheck {
  type component EPTF_Transport_Test_TemporarlyUnavailableCheck_CT extends EPTF_TransportIPL4_CT, EPTF_Transport_CT {
      var integer v_eventCounter := 0;
      var integer v_msgCounter := 0;
      var boolean v_errorAvailableTriggered := false;
  }
} // ~group TemporarlyUnavailableCheck

type component EPTF_Transport_Test_CT2 extends EPTF_Base_CT
{
  private port LANL2asp_PT                                    IPL2_PCO;
  private var default                                         vd_EPTF_Transport_Test_defaultReceive := null;
}

  group IPL2_PerfTest {

    type record LocalAddrConfigElem {
      charstring startIPAddr,
      integer nofAddr,
      integer startPort,
      integer nofPorts
    }

    type record of LocalAddrConfigElem LocalAddrConfigList;

    type record RemoteAddrConfig {
      charstring IPAddr,
      integer portNumber
    }

    type record of RemoteAddrConfig RemoteAddrConfigList;

    type port PerfTestSyncPort message{
      inout integer
    } with { extension "internal"}

    type component PerfTestClient_CT extends EPTF_Routing_CT, EPTF_TransportIPL4_CT,
                                             EPTF_TransportIPL2_CT, EPTF_Transport_CT {
      var EPTF_IntegerList v_connectionIds := {};
      var EPTF_IntegerList v_tcpListenIds := {};
      var EPTF_IntegerList v_pendingMsgs := {};
      var integer v_sendCounter := 0;
      var integer v_recvCounter := 0;
      var integer v_lossCounter := 0;
      var boolean v_loop := true;
      var boolean v_responseReceived := false;
      port PerfTestSyncPort sync_PCO;
      var charstring v_expectedConnId := "";
    }

    type record of PerfTestClient_CT PerfTestClient_CTList;

    type component PerfTestServer_CT extends EPTF_Routing_CT, EPTF_TransportIPL4_CT,
                                             EPTF_TransportIPL2_CT, EPTF_Transport_CT {
      var EPTF_IntegerList v_connectionIds := {};
      port PerfTestSyncPort sync_PCO;

    }

    type record of PerfTestServer_CT PerfTestServer_CTList;


    type EPTF_IntegerList TestContexts; //database model for test; generally i.item = record for i.entity; now: only a number
    type component SIPTransport_Client_CT extends EPTF_Routing_CT, EPTF_TransportIPL4_CT, //EPTF_LGenBase_CT,
                                                  EPTF_TransportIPL2_CT, EPTF_Transport_CT {
      var integer v_myBCtxIdx:=-1;
      var TestContexts v_behaviorContexts:={}
      var EPTF_IntegerList v_connectionIds := {};
      var EPTF_IntegerList v_tcpListenIds := {};
      var EPTF_IntegerList v_pendingMsgs := {};
      var integer v_sendCounter := 0;
      var integer v_recvCounter := 0;
      var integer v_lossCounter := 0;
      var boolean v_loop := true;
      var boolean v_responseReceived := false;
      port PerfTestSyncPort sync_PCO;
      var charstring v_expectedConnId := "";
      var boolean v_LGenBase_testFinished := false;
    }
    type record of SIPTransport_Client_CT SIPTransport_Client_CTList;


   }// group IPL2_PerfTest

group ReconnectDisabled {
  type component EPTF_Transport_Test_ReconnectDisabled_CT extends EPTF_Transport_Test_CT {
    var integer v_serverConnId := -1;
    var integer v_clientConnId := -1;
    var integer v_nofClientCommUps := 0; // number of COMM_UP-s received for the client connection
    var integer v_nofClientCon_opens := 0;   // number of con_open-s received for the client connection
    var boolean v_clientCommLostDetected := false;
    var boolean v_clientSCTPShutdownDetected := false;
    var boolean v_clientConnClosedDetected := false;
  }
} //group ReconnectDisabled

group Test_defaultConnResultEventHandler{

type component  Test_MessageBuffer_defaultConnResultEventHandler_CT extends MessageBufferDemo_CT,EPTF_TransportIPL4_CT {
}

} //group Test_defaultConnResultEventHandler

group DTE_Handling {
type component EPTF_Transport_Test_dte_CT extends EPTF_Transport_Test_CT {
  var boolean v_expectedWarningWasLogged := false;
}
}//group DTE_Handling {

group RetransmissionAfterClose {
type component EPTF_Transport_Test_RetransmissionAfterClose_CT extends EPTF_Transport_Test_CT {
}
}//group RetransmissionAfterClose

}
