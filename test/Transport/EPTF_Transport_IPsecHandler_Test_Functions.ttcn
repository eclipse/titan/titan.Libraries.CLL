///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_Transport_IPsecHandler_Test_Functions {

import from EPTF_Transport_IPsecHandler_Test_Definitions all;
import from TCCIPsec_Definitions all;
import from TCCIPsec_XFRM_Definitions all;
import from EPTF_CLL_Transport_IPsecHandler_Functions all;

import from EPTF_CLL_Transport_CommonDefinitions all;
import from IPL4asp_Types all;
import from EPTF_CLL_Transport_Functions all;


  function f_EPTF_Transport_IPsecHandler_Test_createConnection(
    in EPTF_Transport_TransportType pl_transportType := IPL4,
    in ProtoTuple pl_proto := {tcp := {}},
    in charstring pl_srcAddr := "127.0.0.1",
    in integer pl_srcPort := 44332,
    in charstring pl_dstAddr := "127.0.0.1",
    in integer pl_dstPort := 44333,
    out integer pl_listenId,
    out integer pl_connId
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    timer TL_wait;
    var Result vl_result;
    var integer vl_connId;
    var integer vl_listenId;
    var integer vl_msgCount := 2;
    var boolean vl_res;

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_init(pl_transportType,"EPTF_Transport_IPsecHandler_Test_CT");

//     EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgLenCallback4LGenType(
//       pl_transportType,
//       refers(f_EPTF_Transport_Test_getMsgLen), 
//       {2,3},
//       "type0");

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgCallback(
      pl_transportType,
      "type0",
      refers(f_EPTF_Transport_Test_MsgHandler2),
      refers(f_EPTF_Transport_Test_EventHandler1) ); 

    vl_listenId := EPTF_CLL_Transport_Functions.f_EPTF_Transport_listen (
      pl_transportType,
      pl_proto, 
      pl_dstAddr, 
      pl_dstPort,
      "type0",
      vl_result );  


    action("*** listen result: ", vl_result);
    vl_connId := EPTF_CLL_Transport_Functions.f_EPTF_Transport_connect ( 
      pl_transportType,
      pl_proto,
      pl_srcAddr, 
      pl_srcPort,
      pl_dstAddr, 
      pl_dstPort, 
      "type0",
      vl_result,
      true
    );
    action("*** connect result: ", vl_result);
    TL_wait.start(2.0);
    TL_wait.timeout;

    pl_connId := vl_connId;
    pl_listenId := vl_listenId;
  }

  function f_EPTF_Transport_IPsecHandler_Test_listen(
    in EPTF_Transport_TransportType pl_transportType := IPL4,
    in ProtoTuple pl_proto := {tcp := {}},
    in charstring pl_srcAddr := "127.0.0.1",
    in integer pl_srcPort := 44332,
    in charstring pl_dstAddr := "127.0.0.1",
    in integer pl_dstPort := 44333,
    out integer pl_listenId
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    timer TL_wait;
    var Result vl_result;
    var integer vl_listenId;
    var integer vl_msgCount := 2;
    var boolean vl_res;

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_init(pl_transportType,"EPTF_Transport_IPsecHandler_Test_CT");

//     EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgLenCallback4LGenType(
//       pl_transportType,
//       refers(f_EPTF_Transport_Test_getMsgLen), 
//       {2,3},
//       "type0");

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgCallback(
      pl_transportType,
      "type0",
      refers(f_EPTF_Transport_Test_MsgHandler2),
      refers(f_EPTF_Transport_Test_EventHandler1) ); 

    vl_listenId := EPTF_CLL_Transport_Functions.f_EPTF_Transport_listen (
      pl_transportType,
      pl_proto, 
      pl_dstAddr, 
      pl_dstPort,
      "type0",
      vl_result );  


    action("*** listen result: ", vl_result);
    TL_wait.start(2.0);
    TL_wait.timeout;

    pl_listenId := vl_listenId;
  }

  function f_EPTF_Transport_IPsecHandler_Test_connect(
    in EPTF_Transport_TransportType pl_transportType := IPL4,
    in ProtoTuple pl_proto := {tcp := {}},
    in charstring pl_srcAddr := "127.0.0.1",
    in integer pl_srcPort := 44332,
    in charstring pl_dstAddr := "127.0.0.1",
    in integer pl_dstPort := 44333,
    out integer pl_connId
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    timer TL_wait;
    var Result vl_result;
    var integer vl_connId;
    var integer vl_msgCount := 2;
    var boolean vl_res;

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_init(pl_transportType,"EPTF_Transport_IPsecHandler_Test_CT");

//     EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgLenCallback4LGenType(
//       pl_transportType,
//       refers(f_EPTF_Transport_Test_getMsgLen), 
//       {2,3},
//       "type0");

    EPTF_CLL_Transport_Functions.f_EPTF_Transport_registerMsgCallback(
      pl_transportType,
      "type0",
      refers(f_EPTF_Transport_Test_MsgHandler2),
      refers(f_EPTF_Transport_Test_EventHandler1) ); 

    vl_connId := EPTF_CLL_Transport_Functions.f_EPTF_Transport_connect ( 
      pl_transportType,
      pl_proto,
      pl_srcAddr, 
      pl_srcPort,
      pl_dstAddr, 
      pl_dstPort, 
      "type0",
      vl_result,
      true
    );
    action("*** connect result: ", vl_result);
    TL_wait.start(2.0);
    TL_wait.timeout;

    pl_connId := vl_connId;
  }

// send:
  function f_EPTF_Transport_IPsecHandler_Test_sendMsg(
    in EPTF_Transport_TransportType pl_transportType := IPL4,
    in integer pl_connId,
    in octetstring pl_msg,
    in ProtoTuple pl_proto
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    
      var Result vl_result;
      var boolean vl_res := EPTF_CLL_Transport_Functions.f_EPTF_Transport_send( pl_transportType, pl_connId, pl_msg, vl_result, false, pl_proto);
      timer TL_wait;
      TL_wait.start(2.0);
      TL_wait.timeout;
  }

  function f_EPTF_Transport_IPsecHandler_Test_closeConnection(
    in EPTF_Transport_TransportType pl_transportType := IPL4,
    in integer pl_connId,
    in integer pl_listenId
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    var Result vl_result;
    EPTF_CLL_Transport_Functions.f_EPTF_Transport_close ( pl_transportType, pl_connId, vl_result);
    EPTF_CLL_Transport_Functions.f_EPTF_Transport_close ( pl_transportType, pl_listenId, vl_result);
    timer TL_wait;
    TL_wait.start(2.0);
    TL_wait.timeout;
  }

  function f_EPTF_Transport_Test_MsgHandler2(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_IPsecHandler_Test_CT {
    action("Received Message, connId: ", pl_connId, " msg: ", pl_msg);
    //v_msgCount := v_msgCount + 1;
    setverdict(pass)
  }

  function f_EPTF_Transport_Test_EventHandler1(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event)
  runs on EPTF_Transport_IPsecHandler_Test_CT {
    //v_eventCount := v_eventCount + 1;
    action("Received event, connId: ", pl_connId, " Event: ", pl_event);
  }




  function f_IMS_Logging_warning(in charstring pl_msg) {
    action("WARNING: ",pl_msg);
  }

  function f_IMS_Logging_error(in charstring pl_msg) {
    action("ERROR: ",pl_msg);
  }

} // module
