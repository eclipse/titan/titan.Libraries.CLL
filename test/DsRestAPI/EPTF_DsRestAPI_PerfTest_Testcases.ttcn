///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_DsRestAPI_PerfTest_Testcases
//
// Purpose:
//   This module contains testcases for testing the performance of EPTF DsRestAPI.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_DsRestAPI_PerfTest_Definitions>
//    <EPTF_CLL_DsRestAPI_Definitions>
//    <EPTF_CLL_DsRestAPI_Functions>
//    <EPTF_DsRestAPI_PerfTest_Functions>
//    <EPTF_CLL_Base_Functions>
//
// Current Owner:
//    Daniel Gobor (ednigbo)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_DsRestAPI_PerfTest_Testcases {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_DsRestAPI_PerfTest_Definitions all;
import from EPTF_DsRestAPI_PerfTest_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_DsRestAPI_DSServer_Definitions all;
import from EPTF_CLL_DsRestAPI_DSServer_Functions all;
import from TestResultGen all;
import from TCCConversion_Functions all;
import from TCCMaths_Functions all
import from TCCMaths_GenericTypes all;
import from EPTF_DsRestAPI_Test_Functions all;

private modulepar charstring tsp_DsRestAPI_PerfTest_message := "requestFromHelp.request";

private const charstring c_DsRestAPI_PerfTest_generalPurpose := "To measure the execution time of f_EPTF_DsRestAPI_DSServer_processJSONRequest";

//=========================================================================
// Functions
//=========================================================================

private external function ef_DsRestAPI_dec_Requests(in octetstring pl_par) return EPTF_DsRestAPI_RqWrapper with { extension "prototype(convert) decode(JSON) errorbehavior(ALL:WARNING)" }
private external function ef_DsRestAPI_enc_ContentList(in EPTF_DsRestAPI_ContentAndChildrenListWrapper pl_par) return octetstring with { extension "prototype(convert) encode(JSON) errorbehavior(ALL:WARNING)" }
  
private function f_EPTF_DsRestAPI_PerfTest_getRequestWrapper() runs on DsRestAPI_PerfTest_CT return EPTF_DsRestAPI_RqWrapper {
  var charstring vl_requestFileContent := f_EPTF_DsRestAPI_Test_getFileContent(tsp_DsRestAPI_PerfTest_message);
  var charstring vl_requestJSON := ef_EPTF_DsRestAPI_Test_getJSON(vl_requestFileContent);
  var octetstring vl_requestOctet := char2oct(vl_requestJSON);
  return ef_DsRestAPI_dec_Requests(vl_requestOctet);
}

private function f_EPTF_DsRestAPI_PerfTest_initTestResult(in charstring p_name, in charstring p_purpose,
  in integer p_iterations, in integer p_measurements) runs on DsRestAPI_PerfTest_CT {

  v_testResult.name := p_name;
  v_testResult.purpose := c_DsRestAPI_PerfTest_generalPurpose & ": " & p_purpose;
  f_TestResultGen_getEnvironment(v_testResult.environment);
  v_testResult.parameter_list := {
    {name := "Number of iterations", unit := "-", base := log2str(p_iterations) },
    {name := "Number of measurements", unit := "-", base := log2str(p_measurements) }
  };
  v_testResult.start_time :=  f_TestResultGen_getCurrentTime();
  v_testResult.result_list := {};  
}

private function f_EPTF_DsRestAPI_PerfTest_closeGetDataTestResult(in FloatList p_durations, in integer p_iterations) runs on DsRestAPI_PerfTest_CT {

  // statistics
  var float vl_averageDuration := f_averageFL(p_durations);
  var float vl_averageError := f_stdFL(p_durations);
  var integer vl_resultNum := sizeof(v_testResult.result_list);
  v_testResult.result_list[vl_resultNum] := {
    name := "Execution time",
    unit := "microsec",
    base := log2str(vl_averageDuration/int2float(p_iterations)*1e6)&" +-"&log2str(100.0*vl_averageError/vl_averageDuration)&"%"
  };

  v_testResult.end_time := f_TestResultGen_getCurrentTime();
  action(log2str(vl_averageDuration/int2float(p_iterations)*1e6)&" +-"&log2str(100.0*vl_averageError/vl_averageDuration)&"%");

  f_TestResultGen_appendResult(%testcaseId&"_"&f_unichar2charstr(v_testResult.start_time)&".xml", v_testResult);
}

private function f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_enc(
  in EPTF_DsRestAPI_RqWrapper p_getDataRequest,
  in charstring p_name,
  in charstring p_purpose,
  in integer p_iterations,
  in integer p_measurements
) runs on DsRestAPI_PerfTest_CT {

  f_EPTF_DsRestAPI_PerfTest_initTestResult(p_name, p_purpose, p_iterations, p_measurements);

  var octetstring vl_getDataRequestOct := ef_DsRestAPI_Test_enc_Requests(p_getDataRequest);
  var octetstring vl_getDataResponseOct := f_EPTF_DsRestAPI_DSServer_processJSONRequest(vl_getDataRequestOct);
  var EPTF_DsRestAPI_ContentAndChildrenListWrapper vl_getDataResponseWrap := ef_DsRestAPI_Test_dec_ContentList(vl_getDataResponseOct);

  var FloatList vl_durations;
  for (var integer j := 0; j < p_measurements; j := j + 1) {
    var float start_time := f_EPTF_Base_getAbsTimeInSecs();
    for (var integer i := 0; i < p_iterations; i := i + 1) {
      ef_DsRestAPI_enc_ContentList(vl_getDataResponseWrap);
    }
    var float end_time := f_EPTF_Base_getAbsTimeInSecs() - start_time;
    vl_durations[j] := end_time;
  }

  f_EPTF_DsRestAPI_PerfTest_closeGetDataTestResult(vl_durations, p_iterations);
}

private function f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_dec(
  in EPTF_DsRestAPI_RqWrapper p_getDataRequest,
  in charstring p_name,
  in charstring p_purpose,
  in integer p_iterations,
  in integer p_measurements
) runs on DsRestAPI_PerfTest_CT {

  f_EPTF_DsRestAPI_PerfTest_initTestResult(p_name, p_purpose, p_iterations, p_measurements);

  var octetstring vl_getDataRequestOct := ef_DsRestAPI_Test_enc_Requests(p_getDataRequest);

  var FloatList vl_durations;
  for (var integer j := 0; j < p_measurements; j := j + 1) {
    var float start_time := f_EPTF_Base_getAbsTimeInSecs();
    for (var integer i := 0; i < p_iterations; i := i + 1) {
      ef_DsRestAPI_dec_Requests(vl_getDataRequestOct);
    }
    var float end_time := f_EPTF_Base_getAbsTimeInSecs() - start_time;
    vl_durations[j] := end_time;
  }

  f_EPTF_DsRestAPI_PerfTest_closeGetDataTestResult(vl_durations, p_iterations);
}

private function f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed(
  in EPTF_DsRestAPI_RqWrapper p_getDataRequest,
  in charstring p_name,
  in charstring p_purpose,
  in integer p_iterations,
  in integer p_measurements
) runs on DsRestAPI_PerfTest_CT {

  f_EPTF_DsRestAPI_PerfTest_initTestResult(p_name, p_purpose, p_iterations, p_measurements);

  // run the test
  var octetstring vl_getDataRequestOct := ef_DsRestAPI_Test_enc_Requests(p_getDataRequest);

  var FloatList vl_durations;
  for (var integer j := 0; j < p_measurements; j := j + 1) {
    var float start_time := f_EPTF_Base_getAbsTimeInSecs();
    for (var integer i := 0; i < p_iterations; i := i + 1) {
      f_EPTF_DsRestAPI_DSServer_processJSONRequest(vl_getDataRequestOct);
    }
    var float end_time := f_EPTF_Base_getAbsTimeInSecs() - start_time;
    vl_durations[j] := end_time;
  }

  f_EPTF_DsRestAPI_PerfTest_closeGetDataTestResult(vl_durations, p_iterations);
}

///////////////////////////////////////////////////////////
// Testcases: tc_EPTF_DsRestAPI_PerfTest_*GetData*
// 
// Purpose:
//   to test the performance of f_EPTF_DsRestAPI_DSServer_processJSONRequest() function
//   measures the execution time of encoding, decoding and the actual processJSONRequest function
//   it uses a simple int request, the help request, and the request created from the help
// 
// Requirement:
//   -
//
// Action:
//   - calls f_EPTF_DsRestAPI_PerfTest_init_CT() function
//   - calls f_EPTF_Base_stop() function
//
// Expected Result:
//   - pass
///////////////////////////////////////////////////////////

testcase tc_EPTF_DsRestAPI_PerfTest_encGetDataIntRes() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running encContentList";
  const charstring vl_purpose := "Running encContentList for a simple int request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_intRequestEnc");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_enc(cg_getDataRequestInt, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_decGetDataIntReq() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running decRequests";
  const charstring vl_purpose := "Running decRequests for a simple int request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_intRequestDec");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_dec(cg_getDataRequestInt, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_getDataInt() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running processJSONRequest";
  const charstring vl_purpose := "Running processJSONRequest for a simple int request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_intRequest");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed(cg_getDataRequestInt, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}


testcase tc_EPTF_DsRestAPI_PerfTest_encGetDataHelpRes() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running encContentList";
  const charstring vl_purpose := "Running encContentList for the help request";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_helpRequestEnc");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_enc(cg_getDataRequestHelp, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_decGetDataHelpReq() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running decRequests";
  const charstring vl_purpose := "Running decRequests for the help request";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_helpRequestDec");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_dec(cg_getDataRequestHelp, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_getDataHelp() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running processJSONRequest";
  const charstring vl_purpose := "Running processJSONRequest for the help request";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_helpRequest");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed(cg_getDataRequestHelp, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}


testcase tc_EPTF_DsRestAPI_PerfTest_encGetDataRequestRes() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running encContentList";
  const charstring vl_purpose := "Running encContentList for the request created from help";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequestEnc");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_enc(f_EPTF_DsRestAPI_PerfTest_getRequestWrapper(), vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_decGetDataRequestReq() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running decRequests";
  const charstring vl_purpose := "Running decRequests for the request created from help";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequestDec");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_dec(f_EPTF_DsRestAPI_PerfTest_getRequestWrapper(), vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_getDataRequest() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running processJSONRequest";
  const charstring vl_purpose := "Running processJSONRequest for the request created from help";
  const integer vl_iterations := 50;
  const integer vl_measurements := 6;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequest");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed(f_EPTF_DsRestAPI_PerfTest_getRequestWrapper(), vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_encGetDataSaneRes() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running encContentList";
  const charstring vl_purpose := "Running encContentList for sane request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequestEnc");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_enc(cg_getDataRequestSane, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_decGetDataSaneReq() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running decRequests";
  const charstring vl_purpose := "Running decRequests for sane request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequestDec");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed_dec(cg_getDataRequestSane, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_DsRestAPI_PerfTest_getDataSane() runs on DsRestAPI_PerfTest_CT {
  const charstring vl_name := "Running processJSONRequest";
  const charstring vl_purpose := "Running processJSONRequest for sane request";
  const integer vl_iterations := 5000;
  const integer vl_measurements := 20;

  f_EPTF_DsRestAPI_PerfTest_init_CT("DsRestAPI_PerfTest_bigRequest");
  f_EPTF_DsRestAPI_PerfTest_testGetDataSpeed(cg_getDataRequestSane, vl_name, vl_purpose, vl_iterations, vl_measurements);
  f_EPTF_Base_stop(pass);
}

private external function ef_DsRestAPI_Test_enc_Requests(in EPTF_DsRestAPI_RqWrapper pl_request) return octetstring with { extension "prototype(convert) encode(JSON) errorbehavior(ALL:WARNING)" };
private external function ef_DsRestAPI_Test_dec_ContentList(in octetstring pl_contentList) return EPTF_DsRestAPI_ContentAndChildrenListWrapper with { extension "prototype(convert) decode(JSON) errorbehavior(ALL:WARNING)" };

//=========================================================================
// Control
//=========================================================================
control {
  // int request
  execute(tc_EPTF_DsRestAPI_PerfTest_encGetDataIntRes());
  execute(tc_EPTF_DsRestAPI_PerfTest_decGetDataIntReq());
  execute(tc_EPTF_DsRestAPI_PerfTest_getDataInt());

  // help request
  execute(tc_EPTF_DsRestAPI_PerfTest_encGetDataHelpRes());
  execute(tc_EPTF_DsRestAPI_PerfTest_decGetDataHelpReq());
  execute(tc_EPTF_DsRestAPI_PerfTest_getDataHelp());

  // request from help
  execute(tc_EPTF_DsRestAPI_PerfTest_encGetDataRequestRes());
  execute(tc_EPTF_DsRestAPI_PerfTest_decGetDataRequestReq());
  execute(tc_EPTF_DsRestAPI_PerfTest_getDataRequest());
  
  // sane request
  execute(tc_EPTF_DsRestAPI_PerfTest_encGetDataSaneRes());
  execute(tc_EPTF_DsRestAPI_PerfTest_decGetDataSaneReq());
  execute(tc_EPTF_DsRestAPI_PerfTest_getDataSane());
}

}  // module
