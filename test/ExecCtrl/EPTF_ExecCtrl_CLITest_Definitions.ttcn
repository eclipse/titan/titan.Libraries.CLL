///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_ExecCtrl_CLITest_Definitions
// 
//  Purpose:
//    This module contains data types for the testcases of the EPTF ExecCtrl CLI test enviroment.
// 
//  Module depends on:
// 
//  Current Owner:
//    Laszlo Skumat (elszsku)
// 
//  Last Review Date:
//    2010-03-29
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////

module EPTF_ExecCtrl_CLITest_Definitions
{
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_UIHandlerCLI_Definitions all;

import from EPTF_CLL_ExecCtrl_ScenarioDefinitions all;
import from EPTF_CLL_LGenBase_PhaseDefinitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_ExecCtrl_PhaseDefinitions all;
//import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_ExecCtrlUIHandler_Definitions all
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from TELNETasp_PortType all;
//import from EPTF_CLL_Base_Definitions all;


const charstring c_EPTF_ExecCtrl_CLITest_behaviorName := "ExectrTesterBehavior"
const charstring c_EPTF_ExecCtrl_CLITest_eTypeName := "ExectrTesterEType"
const EPTF_LGenBase_FsmTableDeclarator c_EPTF_ExecCtrl_CLITest_simpleResponseFSM :=  
{
  name := "simpleResponseFSM",
  fsmParams := {
    {stateList := {"idle"}}
  },
  table := {
    classicTable := {
      { eventToListen := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_startTC,fsm},
        cellRow := {{{
              {c_EPTF_LGenBase_stepName_trafficSuccess, omit}
            }, omit, omit}}
      },        
      { eventToListen := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_stopTC,fsm},
        cellRow := {{{
              {c_EPTF_LGenBase_stepName_entityStopped, omit}
            }, omit, omit}}
      },        
      { eventToListen := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_abortTC,fsm},
        cellRow := {{{
              {c_EPTF_LGenBase_stepName_entityAborted, omit}
            }, omit, omit}}
      }        
    }
  }
}

const charstring c_EPTF_ExecCtrl_CLITest_eGrpName := "eg0"
const charstring c_EPTF_ExecCtrl_CLITest_scNameNormal := "normalScenario"
const charstring c_EPTF_ExecCtrl_CLITest_scNameWeighted := "weightedScenario"
const charstring c_EPTF_ExecCtrl_CLITest_tcNameTc1 := "tcII"
const charstring c_EPTF_ExecCtrl_CLITest_tcNameTc2 := "tcI"

const charstring c_EPTF_ExecCtrl_CLITest_tcNorm1Name := c_EPTF_ExecCtrl_CLITest_eGrpName&"."&c_EPTF_ExecCtrl_CLITest_scNameNormal&"."&c_EPTF_ExecCtrl_CLITest_tcNameTc1
const charstring c_EPTF_ExecCtrl_CLITest_tcNorm2Name := c_EPTF_ExecCtrl_CLITest_eGrpName&"."&c_EPTF_ExecCtrl_CLITest_scNameNormal&"."&c_EPTF_ExecCtrl_CLITest_tcNameTc2
const charstring c_EPTF_ExecCtrl_CLITest_tcWeighted1Name := c_EPTF_ExecCtrl_CLITest_eGrpName&"."&c_EPTF_ExecCtrl_CLITest_scNameWeighted&"."&c_EPTF_ExecCtrl_CLITest_tcNameTc1
const charstring c_EPTF_ExecCtrl_CLITest_tcWeighted2Name := c_EPTF_ExecCtrl_CLITest_eGrpName&"."&c_EPTF_ExecCtrl_CLITest_scNameWeighted&"."&c_EPTF_ExecCtrl_CLITest_tcNameTc2

const EPTF_LGenBase_TcMgmt_EntityGrpDeclaratorList c_EPTF_ExecCtrl_CLITest_ExectrTesterEGrp :={{
    name := c_EPTF_ExecCtrl_CLITest_eGrpName,
    eType := c_EPTF_ExecCtrl_CLITest_eTypeName,
    eCount := 100
  }}

const EPTF_LGenBase_TcMgmt_Scenarios2GrpList c_EPTF_ExecCtrl_CLITest_Scenarios2GrpList := {
  {
    eGrpName := c_EPTF_ExecCtrl_CLITest_eGrpName,
    scenarioNames := {
      c_EPTF_ExecCtrl_CLITest_scNameNormal
    }
  },
  {
    eGrpName := c_EPTF_ExecCtrl_CLITest_eGrpName,
    scenarioNames := {
      c_EPTF_ExecCtrl_CLITest_scNameWeighted
    }
  }
}

const EPTF_ExecCtrl_ScenarioInstanceTypeList c_EPTF_ExecCtrl_CLITest_ScenarioInstanceTypeList := {
  {c_EPTF_ExecCtrl_CLITest_scNameNormal, c_EPTF_ExecCtrl_CLITest_eGrpName, c_EPTF_ExecCtrl_CLITest_scNameNormal},
  {c_EPTF_ExecCtrl_CLITest_scNameWeighted, c_EPTF_ExecCtrl_CLITest_eGrpName, c_EPTF_ExecCtrl_CLITest_scNameWeighted}
}

const EPTF_LGenBase_TcMgmt_tcTypeDeclarator2 c_EPTF_ExecCtrl_CLITest_simpleResponseTCType := {
  name := "simpleResponseTCType",
  fsmName := "simpleResponseFSM",
  entityType := c_EPTF_ExecCtrl_CLITest_eTypeName,
  customEntitySucc := ""
}

const EPTF_LGenBase_ScenarioTypeDeclarator c_EPTF_ExecCtrl_CLITest_normalScenario := {
  name := c_EPTF_ExecCtrl_CLITest_scNameNormal,
  tcList := {
    {
      tcName := c_EPTF_ExecCtrl_CLITest_tcNameTc1,
      tcParamsList := {
        {tcTypeName := "simpleResponseTCType"},
        {enableEntitiesAtStart := true},
        {enabledAtStart := true},
        {target := {cpsToReach := 10.0} }
      }
    },
    {
      tcName := c_EPTF_ExecCtrl_CLITest_tcNameTc2,
      tcParamsList := {
        {tcTypeName := "simpleResponseTCType"},
        {enableEntitiesAtStart := true},
        {enabledAtStart := true},
        {target := {cpsToReach := 10.0} }
      }
    }
  },
  scParamsList := {
    {enabled := true}
  }
}

const charstring c_EPTF_ExecCtrl_CLITest_PhaseListName := "phl1"
const EPTF_LGenBase_PhaseList_Declarators c_EPTF_ExecCtrl_CLITest_PhaseLists := {
  {c_EPTF_ExecCtrl_CLITest_PhaseListName, {{"load",true}}} 
}
const EPTF_LGenBase_ScenarioTypeDeclarator c_EPTF_ExecCtrl_CLITest_weightedScenario := {
  name := c_EPTF_ExecCtrl_CLITest_scNameWeighted,
  tcList := {
    {
      tcName := c_EPTF_ExecCtrl_CLITest_tcNameTc1,
      tcParamsList := {
        {tcTypeName := "simpleResponseTCType"},
        {enableEntitiesAtStart := true},
        {enabledAtStart := true},
        {target := {trafficWeight := 25.0} }
      }
    },
    {
      tcName := c_EPTF_ExecCtrl_CLITest_tcNameTc2,
      tcParamsList := {
        {tcTypeName := "simpleResponseTCType"},
        {enableEntitiesAtStart := true},
        {enabledAtStart := true},
        {target := {trafficWeight := 75.0} }
      }
    }
  },
  scParamsList := {
    {weightedScData := {
        cpsToReach := 10.0,
        lockCPS := false,
        deterministicMix := false,
        scheduler := omit
      }
    },
    {enabled := true}
  }
}

const EPTF_LGenBase_ScenarioTypeParamsList c_EPTF_ExecCtrl_CLITest_scPhaseParams := {
  {phaseListName := c_EPTF_ExecCtrl_CLITest_PhaseListName},
  {phaseFinishConditions := {}},
  {phaseStateChangeActions := {
      {
        phase := "load",
        state := RUNNING,
        actions := {
          {startScenario := c_EPTF_ExecCtrl_CLITest_scNameNormal},
          {startScenario := c_EPTF_ExecCtrl_CLITest_scNameWeighted}
        }
      },
      {
        phase := "load",
        state := STOPPING,
        actions := {
          {stopScenario := c_EPTF_ExecCtrl_CLITest_scNameNormal},
          {stopScenario := c_EPTF_ExecCtrl_CLITest_scNameWeighted}
        }
      }
    }
  }
  
}

const EPTF_ExecCtrl_ScenarioGroup_Declarators c_EPTF_ExecCtrl_CLITest_scGrps:={
  {
    name := "SCG0",
    execMode := MANUAL,
    scenarioNames := {c_EPTF_ExecCtrl_CLITest_scNameNormal, c_EPTF_ExecCtrl_CLITest_scNameWeighted},
    phaseListName := c_EPTF_ExecCtrl_CLITest_PhaseListName
  }
}
type component EPTF_ExecCtrl_CLITest_LGen_CT extends EPTF_ExecCtrlClient_CT{
}

type component EPTF_ExecCtrl_CLITest_Terminal_CT{
  port TELNETasp_PT CLI_PCO;
}
const integer c_CLITest_tcNorm1 := 0
const integer c_CLITest_tcNorm2 := 1
const integer c_CLITest_tcWeighted1 := 2
const integer c_CLITest_tcWeighted2 := 3
//FIXME EPTF_CLI_CT must be removed when it's extended by the ExecCtrl
type component EPTF_ExecCtrl_CLITest_Main_CT extends EPTF_UIHandler_CLI_CT, EPTF_ExecCtrl_UIHandler_CT, EPTF_ExecCtrl_CT, EPTF_Base_CT{
  var EPTF_ExecCtrlClient_CT v_LGen
  var EPTF_CharstringList v_stateOfTcs := {
    c_EPTF_LGenBase_tcStateNameIdle,
    c_EPTF_LGenBase_tcStateNameIdle,
    c_EPTF_LGenBase_tcStateNameIdle,
    c_EPTF_LGenBase_tcStateNameIdle}
var charstring v_EPTF_ExecCtrlCLICommand_startExecSC := ""// "startExec sc" 
var charstring v_EPTF_ExecCtrlCLICommand_startExecTC := ""// "startExec tc" 
var charstring v_EPTF_ExecCtrlCLICommand_startExecSCG := ""// "startExec scGroup" 
var charstring v_EPTF_ExecCtrlCLICommand_startExecAll := ""// "startExec all" 
var charstring v_EPTF_ExecCtrlCLICommand_stopExecSC := ""// "stopExec sc" 
var charstring v_EPTF_ExecCtrlCLICommand_stopExecTC := ""// "stopExec tc" 
var charstring v_EPTF_ExecCtrlCLICommand_stopExecSCG := ""// "stopExec scGroup" 
var charstring v_EPTF_ExecCtrlCLICommand_stopExecAll := ""// "stopExec all" 
var charstring v_EPTF_ExecCtrlCLICommand_setTargetCpsSC := ""// "setCPS sc" 
var charstring v_EPTF_ExecCtrlCLICommand_setTargetCpsTC := ""// "setCPS tc" 
var charstring v_EPTF_ExecCtrlCLICommand_getTargetCpsSC := ""// "getTargetCPS sc" 
var charstring v_EPTF_ExecCtrlCLICommand_getTargetCpsTC := ""// "getTargetCPS tc" 
var charstring v_EPTF_ExecCtrlCLICommand_getCurrentCpsTC := ""// "getCurrentCPS tc" 
var charstring v_EPTF_ExecCtrlCLICommand_getCurrentCpsSC := ""// "getCurrentCPS sc" 
var charstring v_EPTF_ExecCtrlCLICommand_getTargetWeight := ""// "getTargetWeight tc" 
var charstring v_EPTF_ExecCtrlCLICommand_setTargetWeight := ""// "setWeight tc" 
}

}  // end of module
