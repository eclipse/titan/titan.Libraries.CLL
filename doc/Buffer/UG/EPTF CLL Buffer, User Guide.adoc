---
Author: János Zoltán Sváner
Version: 30/198 17-CNL 113 512, Rev. A
Date: 2010-09-24

---
= EPTF CLL Buffer, User Guide
:author: János Zoltán Sváner
:revnumber: 30/198 17-CNL 113 512, Rev. A
:revdate: 2010-09-24
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Buffer of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>. This document should be read together with the Function Description of the Buffer feature <<_5, ‎[5]>>. For more information on the TitanSim CLL consult the User's Guide <<_4, ‎[4]>> and the Function Specification ‎<<_3, [3]>> of the TitanSim.

= System Requirements

In order to use the Buffer feature the system requirements listed in TitanSim CLL User Guide ‎<<_4, [4]>> should be fulfilled.

= Buffer

== Overview

The EPTF Buffer component provides TTCN3 function access to the native buffer of Titan (<<_7, ‎[7]>>, chapter 3.1.2 - TTCN Buffer).

This component

* stores an octetstring and allows of querying, adding to, extending, completing it.
* supports the graceful termination feature of CLL Base in TitanSim ‎<<_8, [8]>>.

To be able to use EPTF Buffer, the user component should extend the `EPTF_Buffer_CT` component.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Buffer API includes the following files:

* Buffer
** __EPTF_CLL_Buffer_Definitions.ttcn__ - This TTCN-3 module contains common type definitions used by the Buffer Component.
** __EPTF_CLL_Buffer_Functions.ttcn__ - This TTCN-3 module contains the implementation of Buffer functions.
** __EPTF_CLL_Buffer_ExternalFunctions.cc__ - This TTCN-3 module contains external functions for EPTF Buffer.

[[description_of_required_files_from_other_features]]
== Description of Required Files from Other Features

The EPTF Buffer feature depends on the following features of EPTF:

* `Base`

== Installation

Since `EPTF_CLL_Buffer` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, ‎[2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_Buffer`:

* Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files from Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Buffer test or write your own application using EPTF Buffer.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of ‎<<_2, ‎[2]>>.
* Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see ‎<<_2, [2]>>.

The EPTF Buffer feature does not define module parameters.

= Examples

The "test" directory of the deliverable contains the following examples:

* __EPTF_Buffer_Test_Testcases.ttcn__

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, ‎[1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Buffer, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]

[[_7]]
[7] Programmer's Guide - API Technical Reference for TITAN TTCN-3 Test Executor

[[_8]]
[8] EPTF CLL Base Function Description
