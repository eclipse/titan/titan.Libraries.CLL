---
Author: Bence Molnár
Version: 28/198 17-CNL 113 512, Rev. A
Date: 2009-02-20

---
= EPTF CLL Random N Array, User Guide
:author: Bence Molnár
:revnumber: 28/198 17-CNL 113 512, Rev. A
:revdate: 2009-02-20
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Random N Array of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>. This document should be read together with the Function Description of the Random N Array feature <<_5, ‎[5]>>. For more information on the TitanSim CLL, consult the Users Guide <<_4, ‎[4]>> and the Function Specification ‎<<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the Random N Array feature the system requirements listed in TitanSim CLL User Guide <<_4, ‎[4]>> should be fulfilled.

= Random N Array

== Overview

The EPTF CLL Random N Array component is a fundamental component providing an implementation of linked lists. It is used, among others, for dynamic memory allocation in a load test environment for the TTCN-3 language and sorting records of complex data structures without actually moving the data.

RNA contains N `IntegerList`. An `IntegerList` can represent a state (free, busy, invalid etc.). Each `IntegerList` stores a specified number of elements. There is an `IdxMap`, that contains Integer pairs (`IntegerLists`) for each elements where the first integer of the pairs points to the List where the element is stored and the second stores the index of the element inside that `IntegerList`.

The advantage of this structure is that an element can be selected randomly from the lists.

RFBA is a specific RNA. It supports two IntegerLists or two states a Free and a Busy state, that can contain elements. The advantage of RFBA is that it integrates well into the current CLL code, since the FBQ has a similar interface. RNA on the other hand is more generic with its N states. Figure below shows a simple RFBA.

image:images/RFBA.png[alt]

[[desc_files_this_feat]]
== Description of Files in This Feature

The EPTF CLL Random N Array API includes the following files:

* Random N Array
** __EPTF_CLL_RNA_Definitions.ttcn__ - This TTCN-3 module contains common type definitions for Random N Array type linked list management in general and for Random Free Busy Array in particular.
** __EPTF_CLL_RNA_Functions.ttcnpp__ - This TTCN-3 module contains the implementation of Random N Array type linked list management in general, and the Random Free Busy Array management in particular.

== Description of Required Files From Other Features

The Random N Array feature does not depend on files from other features.

== Installation

Since EPTF CLL Random N Array is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, ‎[2]>>.

If not otherwise noted in the respective sections, the following are needed to use EPTF CLL Random N Array:

* Copy the files listed in section <<desc_files_this_feat, Description of Files in This Feature>> to the directory of the test suite or create symbolic links to them.
* Import the Random N Array demo or write your own application using Random N Array.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, ‎[2]>>.
* Edit the config file according to your needs; see following section <<config, Configuration>>.

[[config]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, ‎[2]>>.

The EPTF CLL Random N Array feature defines TTCN-3 module parameters as defined in <<_2, ‎[2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

The Random N Array feature defines the following module parameters:

`tsp_debug_RNA`

This boolean type module parameter is defined in module `EPTF_CLL_RNA_Definitions`, with a default value of `_false_`. Setting it to `_true_` enables logging of debug messages.

= Error Messages

NOTE: Besides the below described error messages, error messages shown in <<_2, ‎[2]>> or those of other used features or product may also appear.

The Random N Array feature may log one of the following error messages (and stop execution):

`*Error: f_EPTF_RNA_initRNA: Size of a list should be at least 0*`

`*Error: f_EPTF_RNA_initRNA: Number of lists should be at least 1*`

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, ‎[2]>> or those of other used features or product may also appear.

`*Warning: f_EPTF_RNA_getRndSlot: Invalid number of lists*`

`*Warning: f_EPTF_RNA_getRndOrCreateSlot: Invalid number of lists*`

`*Warning: f_EPTF_RNA_createSlots: Invalid list number*`

`*Warning: f_EPTF_RNA_createSlots: Invalid number of new slots*`

`*Warning: f_EPTF_RNA_moveSlot: Invalid `list to' number*`

`*Warning: f_EPTF_RNA_moveSlot: The size of RNA is smaller than this element index*`

`*Warning: f_EPTF_RNA_moveSlot: This element index cannot be moved to the same list! Do nothing*`

`*Warning: f_EPTF_RNA_getList: The size of RNA is smaller than this element index*`

`*Warning: f_EPTF_RNA_getLengthOfList: Invalid list number*`

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_RNA.cfg__
* __EPTF_RNA_Demo.prj__
* __EPTF_RNA_Demo.ttcn__

= Terminology

*TitanSim Core (Load) Library(CLL)* is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, ‎[1]>>.

FBQ:: Free Busy Queue

RNA:: Random N Array

RFBA:: Random Free Busy Array

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Random N Array, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
