---
Author: Gábor Tatárka
Version: 5/155 16-CNL 113 512, Rev. C
Date: 2010-02-09

---
= EPTF CLL Common, Function Description
:author: Gábor Tatárka
:revnumber: 5/155 16-CNL 113 512, Rev. C
:revdate: 2010-02-09
:toc:

== How to Read This Document

This is the Function Description for the Common of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 ‎<<_1, [1]>> Toolset with TITAN <<_2, [2]>>.

== Scope

This document is to specify the content and functionality of the Common feature of the TitanSim CLL.

== Recommended Way of Reading

The readers are supposed to get familiar with the concept and functionalities of TitanSim CLL <<_3, ‎[3]>>. They should get familiar with the list of <<terminology, acronyms>> and the <<abbreviations, glossary>>.

= General Description

This document specifies the Common feature of the TitanSim CLL.

The EPTF Common feature contains type and function definitions commonly used by other EPTF features.

= Common Types

The Common feature mainly serves as a type definition module for commonly used types. This chapter describes these types.

== Default List

The type

`EPTF_DefaultList`

provides a container for arbitrary number of default. Its type is a 'record of integer'.

== Integer List

The type

`EPTF_IntegerList`

provides a container for arbitrary number of integers. Its type is 'record of default'.

== Integer Array 2D

The type

`EPTF_IntegerArray2D`

provides a container for arbitrary number of integers in a 2D array. Its type is record of `EPTF_IntegerList`.

== Charstring List

The type

`EPTF_CharstringList`

provides a container for arbitrary number of charstrings. Its type is 'record of charstring'.

== Float List

The type

`EPTF_FloatList`

provides a container for arbitrary number of floats. Its type is 'record of float'.

== Boolean List

The type

`EPTF_BooleanList`

provides a container for arbitrary number of booleans . Its type is 'record of boolean'.

== LED Colors

The type

`EPTF_LEDColors`

is an enumerated type for the known LED colors on the user interface.

Possible values:

* `_led_blue_`
* `_led_black_`
* `_led_yellow_`
* `_led_green_`
* `_led_red_`

== Status LED

The type

`EPTF_StatusLED`

is a record type for status LEDS with text.

Elements of the status led:

* `EPTF_LedColors` color – the color of the status led
* charstring text – optional text for the status led

== Parameter Range Descriptor

The type

`EPTF_ParamRangeDescriptor`

is a record type defining an integer parameter range.

Its elements are:

* charstring `name` – the name of the range
* integer `baseOffset` – the first value of the range
* integer `count` – the size of the range
* integer `iterator` – the actual value

== Parameter Range Descriptor List

The type

`EPTF_ParamRangeDescriptorList`

provides a container for arbitrary number of parameter range descriptors. Its type is record of `EPTF_ParamRangeDescriptor`.

== RndValues Component

The type

`EPTF_Common_RndValues_CT`

provides a container for the pre generated random numbers.

== Debug Switch

The constant

`c_EPTF_Common_debugSwitch`

can be used to guard debug code or logging in an if statement. The code guarded this way will be excluded from the compiled executable if `–DEPTF_DEBUG` is not added to `CPPFLAGS_TTCN3` and `–O2` is added to `CXXFLAGS` in the _Makefile_.

== IndexArray

The type

`EPTF_Common_IndexArray`

provides a record type to help quick storage and retrieval of indexes from a list. This type with the associated functions is practical when the count of the elements to be handled is less or about 10-20. The value of the indexes can be any nonnegative integer number or `_-1_`. The index value of `_-1_` means that the given index is not used (or free). If the index value is bigger than `_-1_`, the element is used or valid (or busy). First busy index can be used to get the first valid element in the list. The constant `c_EPTF_emptyCommon_IndexArray` can be used to create a free IndexArray.

The elements of IndexArray are:

* `EPTF_IntegerList` values – list of indexes and their values
* integer firstBusy - first busy index in the list above

== Summary Table of All Types in Common

[cols=",",options="header",]
|================================================================
|Type name |Description
|`EPTF_DefaultList` |record of default
|`EPTF_IntegerList` |record of integer
|`EPTF_IntegerArray2D` |record of EPTF_IntegerList
|`EPTF_CharstringList` |record of charstring
|`EPTF_FloatList` |record of float
|`EPTF_BooleanList` |record of boolean
|`EPTF_LEDColors` |enumerated LED colors
|`EPTF_StatusLED` |status led record
|`EPTF_ParamRangeDescriptor` |integer parameter range
|`EPTF_ParamRangeDescriptorList` |record of integer parameter range
|`EPTF_Common_RndValues_CT` |component
|`c_EPTF_Common_debugSwitch` |boolean constant
|`EPTF_Common_IndexArray` |record of free or busy indexes
|================================================================

= Functional Interface

Apart from this description a cross-linked reference guide for the TitanSim CLL Functions can be reached for on-line reading ‎<<_4, [4]>>.

== Naming Conventions

All functions have the prefix `f_EPTF_Common_`.

== Public Functions

=== Initializing

To initialize the Common feature call the function

`f_EPTF_Common_init()`

=== Reset Range Parameter

`f_EPTF_Common_resetParamRanges(pl_rangeList)`

This function can be used to reset range iterators to the base offset of the ranges.

=== Incrementing Range Iterators

`f_EPTF_Common_incParamRanges(pl_rangeList)`

This function can be used to increment the least significant range iterator. It handles carry if necessary.

=== Increasing the Least Significant Range

`f_EPTF_Common_fillWeightedBuckets(pl_weightList, pl_nrOfElements, pl_result)`

This function can be used to increment the least significant range iterator. It handles carry if necessary.

=== Initialize the RndValues Component

`f_EPTF_Common_RndValues_init_CT()`

This function can be used to initialize the RndValues component.

=== Get the Next Generated Random Number

`f_EPTF_Common_RndValues_getNextRndValue_CT()`

This function can be used to get the next pre generated random number. The component has to be initialized with the `f_EPTF_Common_RndValues_init_CT` function before to use of this function.

=== IndexArray Functions

==== Obtaining a Free Index

`f_EPTF_Common_IndexArray_getOrCreateFreeSlot()`

This function can be used to get a free index (an index which content is -1) from the IndexArray record. If there is no free index, the function creates a new free index and returns it.

==== Get the Content of an Index

`f_EPTF_Common_IndexArray_getElement()`

This function can be used to retrieve the content of the specified index from the IndexArray record. If the index is not found, the returned value will be `_-1_`.

==== Set the Content of an Index

`f_EPTF_Common_IndexArray_setElement()`

This function can be used to set the content of the specified index in the IndexArray record. If any other free indexes are created their content will be `_-1_`.

==== Obtaining a Free Index and Set its Content

`f_EPTF_Common_IndexArray_setNewElement()`

This function can be used to get a free index (an index which value is `_-1_`) from the IndexArray record. If there is no free index, the function creates a new free index. The content of the index will be the data value given in the function. If no data is given the content will be `_-1_`.

==== Free the Content of an Index

`f_EPTF_Common_IndexArray_freeElement`

This function can be used to free the given index. The content of the given index will be `_-1_`.

==== Checking the Emptiness of IndexArray Record

`f_EPTF_Common_IndexArray_arrayIsEmpty()`

This function can be used to check whether the IndexArray record is empty (containing only `_-1_` values).

==== Get the First Busy Index

`f_EPTF_Common_IndexArray_getFirstBusyIdx()`

This function can be used to get the first index in the record which is busy (containing non-negative value).

==== Get the Next Busy Index

`f_EPTF_Common_IndexArray_getNextBusyIdx()`

This function can be used to get the index of the first busy element which is bigger than the given index.

=== Logging

[[logging-an-error-warning-user-messages]]
==== Logging Error/Warning/User Messages

To log an error/warning or user message call the appropriate function:

`f_EPTF_Common_error(pl_message)`

`f_EPTF_Common_warning(pl_message)`

`f_EPTF_Common_user(pl_message)`

==== Determine the Number of Errors

The number of errors occurred is returned by the function.

`f_EPTF_Common_nofErrorMsgs()`

==== To Retrieve a Certain Error Message

A given error message text can be retrieved by the function.

`f_EPTF_ Common_getErrorMsg(pl_errorNum)`

where `pl_errorNum` gives the id of the error message. The id if the error message starts from `_zero_`, i.e. it is zero for the first error, `_one_` for the second an so on.

==== To Check if an Error Message Matches with a Given Pattern

To check if an error message matches with a given pattern, the function

`f_EPTF_ Common_checkExpectedError(pl_expectedError, pl_errorNum)`

can be used. The `pl_expectedError` is the error pattern that is matched with the occurred error with id `pl_errorNum`. The id of the errors starts from zero. The function returns true if the pattern matches and false if it does not.

See summary table of all public functions for EPTF Common below:

[width="100%",cols="50%,50%",options="header",]
|=================================================================================
|Function name |Description
|`f_EPTF_Common_resetParamRanges` |reset range iterators
|`f_EPTF_Common_incParamRanges` |increment range iterator
|`f_EPTF_Common_fillWeightedBuckets` |increasing the least significant range
|`f_EPTF_Common_RndValues_init_CT` |initialize the RndValues component
|`f_EPTF_Common_RndValues_getNextRndValue` |get the next pre generated random number
|=================================================================================

See summary of Common IndexArray functions below:

[width="100%",cols="50%,50%",options="header",]
|==================================================================================
|Function name |Description
|`f_EPTF_Common_IndexArray_getOrCreateFreeSlot` |obtaining a free index
|`f_EPTF_Common_IndexArray_getElement` |get the content of an index
|`f_EPTF_Common_IndexArray_setElement` |set the content of an index
|`f_EPTF_Common_IndexArray_setNewElement` |obtaining a free index and set its content
|`f_EPTF_Common_IndexArray_freeElement` |free the content of an index
|`f_EPTF_Common_IndexArray_arrayIsEmpty` |checking the emptiness of IndexArray record
|`f_EPTF_Common_IndexArray_getFirstBusyIdx` |get the first busy index
|`f_EPTF_Common_IndexArray_getNextBusyIdx` |get the next busy index
|==================================================================================

[[terminology]]
= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson

[[abbreviations]]
= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
