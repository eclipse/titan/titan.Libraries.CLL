///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: ExecCtrl_Demo
// 
//  Purpose:
//    This module demonstrates the usage of the ExecCtrl feature
// 
//  Module Parameters:
//    tsp_numEntities - integer
//    tsp_testDuration - float
//    tsp_nrOfLGens - integer
//    tsp_LGname_prefix - charstrin
// 
//  Module depends on:
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_LGenBase_Functions>
//    <EPTF_CLL_LGenBase_ConfigFunctions>
//    <AppLib_A_Definitions>
//    <AppLib_A_Functions>
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <EPTF_CLL_ExecCtrl_Functions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandler_WidgetFunctions>
//    <EPTF_CLL_UIHandler_XTDPTemplateDefinitions>
//    <XTDP_PDU_Defs language>
//    <EPTF_CLL_UIHandlerClient_Functions>
//    <EPTF_CLL_RBTScheduler_Functions>
//
//  Current Owner:
//    ethzto
// 
//  Last Review Date:
//    2007-12-06
// 
// 
///////////////////////////////////////////////////////////

module ExecCtrl_Demo
{
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;

import from AppLib_A_Definitions all;
import from AppLib_A_Functions all;
//import from AppLib_B_Definitions all;
//import from AppLib_B_Functions all;

import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_ExecCtrl_Functions all;
import from EPTF_CLL_ExecCtrlClient_Functions all;

import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_UIHandler_XTDPTemplateDefinitions all;
import from EPTF_CLL_UIHandler_XULFunctions all;

import from XTDP_PDU_Defs language "ASN.1:2002" all;
import from EPTF_CLL_UIHandlerClient_Functions all;

import from EPTF_CLL_RBTScheduler_Functions all;

modulepar integer tsp_numEntities := 10;
modulepar float tsp_testDuration := 30.0;
modulepar integer tsp_nrOfLGens := 2;
modulepar charstring tsp_LGname_prefix := "LG_";

type component ExecCtrl_Demo_CT 
extends EPTF_LGenBase_CT, EPTF_ExecCtrlClient_CT, LGen_A_CT //, LGen_B_CT 
{
  var integer v_myBIdx;
}

const charstring c_appl_stepNameInitTc0 := "Initialize behavior contexts for tc0"
const integer c_appl_stepIdxInitTc0 := 0;
function f_step_initTc0(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on ExecCtrl_Demo_CT { 
  var integer bCtx;
  bCtx:= f_EPTF_LGenBase_getBehaviorCtxItem(pl_ptr.eIdx,c_A_bIdx,0);
  v_A_contexts[bCtx].fooMessage := "Entity " & f_EPTF_LGenBase_getEntityName(pl_ptr.eIdx) &": "& v_A_contexts[bCtx].fooMessage ;
  v_A_contexts[bCtx].byeMessage := "Entity " & f_EPTF_LGenBase_getEntityName(pl_ptr.eIdx) &": "& v_A_contexts[bCtx].byeMessage ;
  //  bCtx:= v_LGenBase_entities[pl_ptr.eIdx].bCtxList[c_B_bIdx][0]
  //  v_B_contexts[bCtx].firstMessage := "Entity " & v_LGenBase_entities[pl_ptr.eIdx].name
  //  & " sends its first message acting as \"B\" in testcase#0";
  // v_B_contexts[bCtx].secondMessage := "Entity " & v_LGenBase_entities[pl_ptr.eIdx].name
  //  & " sends its second message acting as \"B\" in testcase#0"
  //  f_LGenBase_testStepIsOver(pl_ptr)
}



//init the "application level virtual library"
const charstring c_myVirtualAppLib_behaviorType := "LGen demo application"; // ethzto
function f_init_myVirtualAppLib() runs on ExecCtrl_Demo_CT {
  var integer i;
  
  v_myBIdx:=f_EPTF_LGenBase_declareBehaviorType(c_myVirtualAppLib_behaviorType, 100, null, null, null);
  log(%definitionId,": myBIdx is ", v_myBIdx);
  i:=f_EPTF_LGenBase_declareStep(c_myVirtualAppLib_behaviorType,{c_appl_stepNameInitTc0,refers(f_step_initTc0)});
  //  i:=f_EPTF_LGenBase_declareStep(c_myVirtualAppLib_behaviorType,{c_appl_stepNameInitTc1,refers(f_step_initTc1)});
  
}

// note the applib init function call seq. must be consistent 
// with these c_xxx_bIdx declarations 
const integer c_appl_bIdx := 1;
const integer c_A_bIdx := 2;
const integer c_B_bIdx := 3;

function f_LGen_Demo_init(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef) runs on ExecCtrl_Demo_CT {
  
  f_EPTF_LGenBase_init(pl_selfName, 0, "DemoEntity#");
  
  //note the applib init function call seq. must be consistent with ...bIdx declarations 
  f_init_myVirtualAppLib() ;      //behavior type index==0 
  f_LGen_A_init(tsp_numEntities); //behavior type index==1
  //  f_LGen_B_init(tsp_numEntities); //behavior type index==2


  var integer vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_A", {c_A_behaviorType});
  vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_B", {c_myVirtualAppLib_behaviorType});
  vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_AB", {c_A_behaviorType, c_myVirtualAppLib_behaviorType});
  
  connect(self:port_A,self:port_A);  
  
  f_EPTF_ExecCtrlClient_init_CT(pl_selfName, pl_ExecCtrlRef);   // Only after library init(s)
  f_EPTF_LGenBase_log();

}

function f_LGen_Demo_behavior(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef) runs on ExecCtrl_Demo_CT
{
  log(%definitionId,": started...");

  f_LGen_Demo_init(pl_selfName, pl_ExecCtrlRef); 

  timer t;
  t.start(tsp_testDuration);

  log(%definitionId,": entering main EH...")
  alt {
    [] as_LibA() 
    //    []as_LibB() 
    [] t.timeout {}
  }
  setverdict(pass);

//  f_cleanup...
  log(%definitionId,": finished...");
}

function f_ExecCtrl_Demo_initGui() runs on EPTF_UIHandler_CT{

  var XTDP_XML_Tag execCtrlMainWindow := {        
      xtdp_tabpanel := {      
        id := omit,
        maxheight := omit,
        orient := "vertical",
        widgets := {
          {
            hbox := {
              id := omit,
              flex := omit,
              disabled := omit,
              orient := "horizontal",
              widgets := {
                {
                  hbox := {
                    id := omit,
                    flex := omit,
                    disabled := omit,
                    orient := "vertical",
                    widgets := {
                      { 
                        tabbox := {
                          id := omit,
                          flex := omit,
                          disabled := omit,
                          layout := omit,
                          tabs := {
                            {
                              id := "Execution_Control.tab",
                              tablabel := "Execution Control" 
                            }
                          },
                          tabpanels := {
                          }
                        }
                      },
                      {
                        hbox := {
                          id := omit,
                          flex := omit,
                          disabled := omit,
                          orient := "horizontal",
                          widgets := {
                            {
                              hbox := {
                                id := omit,
                                flex := omit,
                                disabled := omit,
                                orient := "vertical",
                                widgets := {
                                  {
                                    textlabel := {
                                      id := "timeelapsed",
                                      flex := omit,
                                      disabled := omit,
                                      style := omit,
                                      textvalue := "Time elapsed since Test was started: 0.0" 
                                    }
                                  },
                                  {
                                    textlabel := {
                                      id := "listeningon",
                                      flex := omit,
                                      disabled := omit,
                                      style := omit,
                                      textvalue := "Listening on ." 
                                    }
                                  }                      
                                }
                              }
                            },
                            {
                              button := {
                                checked := omit,
                                disabled := omit,
                                id := "EPTF_snapshot_button",
                                flex := omit,
                                buttonlabel := "Snapshot",
                                buttontype := omit,
                                imageId := omit 
                              }                          
                            },
                            {
                              spacer := {
                                flex := 0.1,
                                id := omit
                              }
                            },
                            {
                              button := {
                                checked := omit,
                                disabled := omit,
                                id := "EPTF_exit_ttcn_button",
                                flex := omit,
                                buttonlabel := "Exit TTCN",
                                buttontype := omit,
                                imageId := omit 
                              }                          
                            }                        
                          }
                        }
                      }                    
                    }
                  } 
                }              
              }
            }                      
          }
        }      
      }  
    };
      
  var boolean widgetExist := false;
  log("execCtrlMainWindow: ", execCtrlMainWindow);
  if (not f_EPTF_UIHandler_addElementToGui(
    execCtrlMainWindow, 
    "",
    widgetExist)) {
    log(%definitionId,": InitGUI failed. Exiting...");
    f_EPTF_Base_stopAll();
    //stop; // this is not needed
  };
}

// Test config deployment manually
type component myMTC_CT extends EPTF_Base_CT, EPTF_UIHandler_CT {} 

// User functions for LGenPools
function f_EPTF_ExecCtrl_myExecCtrlBehavior(in charstring pl_selfName, in integer pl_nrOfClients)
runs on EPTF_ExecCtrl_CT
{
  var EPTF_ExecCtrl_LGenFunction_Entry_List vl_functions :=
  {
    {
      name := "userCreate",
      fn  := refers(f_EPTF_ExecCtrl_CreateLGen_userCreate)
    }
  }
  
  f_EPTF_ExecCtrl_loadConfig(pl_EPTF_ExecCtrl_LGenFunction_Entry_List := vl_functions);
  f_EPTF_ExecCtrl_behavior(pl_selfName, pl_nrOfClients);
}

function f_EPTF_ExecCtrl_CreateLGen_userCreate
  (
    in charstring pl_hostname,
    in charstring pl_componentName
  
  )
  runs on EPTF_ExecCtrl_CT
  return EPTF_ExecCtrlClient_CT
{
  // Create LGen instance
  var ExecCtrl_Demo_CT vc_client := ExecCtrl_Demo_CT.create(pl_hostname);
  
  // Connect and Start the LGen
  vc_client.start(f_LGen_Demo_behavior(tsp_LGname_prefix & pl_componentName, self));
  
  return vc_client;
}

testcase TC() runs on myMTC_CT
{
  log(%definitionId,": Creating UIHandler...");

  f_EPTF_UIHandler_init_CT("ExecCtrlDemo_UIHandler", true);
  f_EPTF_UIHandler_clearGUI();
  if(not f_EPTF_UIHandler_addWindow()) {
    log(%definitionId,": Cannot add main window. Exiting...");
    f_EPTF_Base_stopAll();
    //stop; this is not necessary
  };

  f_ExecCtrl_Demo_initGui();

  log(%definitionId,": Creating ExecCtrl...");
  f_EPTF_Base_init_CT("ExecCtrl_Demo");

  var EPTF_ExecCtrl_CT ExtCtrl := EPTF_ExecCtrl_CT.create;
  ExtCtrl.start(f_EPTF_ExecCtrl_myExecCtrlBehavior("ExecCtrl", tsp_nrOfLGens));

  log(%definitionId,": Creating LGens (ExecCtrlClients)...")
  var ExecCtrl_Demo_CT LG;
  for (var integer i:=0;i<tsp_nrOfLGens;i:=i+1) {
    LG := ExecCtrl_Demo_CT.create;
    LG.start(f_LGen_Demo_behavior(tsp_LGname_prefix & int2str(i), ExtCtrl));
  }

  timer t;
  t.start(tsp_testDuration);

  log(%definitionId,": myMTC_CT: entering main EH...")
  alt
  {

  [] t.timeout
    {
      log(%definitionId,": myMTC_CT: main EH: timeout...")
    }
  } // alt

  f_EPTF_Base_cleanup_CT();

  log(%definitionId,": finished")
}

control {
  execute(TC());
}

}  // end of module
