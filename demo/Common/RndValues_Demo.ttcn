///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: RndValues_Demo
// 
//  Purpose:
//    This module demonstrates the usage of the RndValues common feature
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//   <EPTF_CLL_Common_RndDefinitions>
//
//   <EPTF_CLL_Common_RndFunctions>
//
//  Current Owner:
//    ENORPIN
// 
//  Last Review Date:
//    2008-01-28
// 
// 
///////////////////////////////////////////////////////////
module RndValues_Demo {

import from EPTF_CLL_Common_RndDefinitions all;
import from EPTF_CLL_Common_RndFunctions all;

testcase tc_rnd() runs on EPTF_Common_RndValues_CT
{
  for(var integer j:=0; j<5; j:=j+1)
  {
    log("=================================");
    log("j = ", j);
    log("Before init v_EPTF_Common_RndValues_rndNumberList:", v_EPTF_Common_RndValues_rndNumberList);
    if( f_EPTF_Common_RndValues_init_CT() )
    {
      log("successful init");
    }
    else
    {
      setverdict(fail, "Initialization unsuccessful");
    }
    log("After init v_EPTF_Common_RndValues_rndNumberList:", v_EPTF_Common_RndValues_rndNumberList);
    for(var integer i:=0; i<tsp_EPTF_Common_RndValues_numberOfRndValues; i:=i+1)
    {
      log("i:",v_EPTF_Common_RndValues_actualIndex, "\tvalue:", f_EPTF_Common_RndValues_getNextRndValue( ));
    }
  }
  setverdict(pass);
}

control {
  execute( tc_rnd());
}

}
