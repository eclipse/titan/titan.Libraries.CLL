///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_RNA_Demo
//
// Purpose:
//   This module contains the demo of Random N Array
//
//  Module depends on:
//    <EPTF_CLL_RNA_Functions>
//    <EPTF_CLL_RNA_Definitions>
//
// Current Owner:
//   Bence Molnar (EBENMOL)
//
// Last Review Date:
//   2009-02-20
//
///////////////////////////////////////////////////////////

module EPTF_RNA_Demo
{

import from EPTF_CLL_RNA_Functions all;
import from EPTF_CLL_RNA_Definitions all

type component PCOComp {}

////////////////////////////////////////////////////////////
testcase tc_RNA_Demo() runs on PCOComp system PCOComp {

  var integer vl_idx;
  var integer vl_length;
  var boolean vl_result := false;  
  var EPTF_RNA_RandomNArray pl_RNA := c_EPTF_RNA_emptyRandomNArray;
  var EPTF_RNA_RandomNArray pl_RFBA := c_EPTF_RNA_emptyRandomNArray;
  setverdict(pass);

  f_EPTF_RNA_initRNA(10,2,pl_RNA);
  log("The RNA before the modifications: ",pl_RNA);

  vl_idx := f_EPTF_RNA_getRndOrCreateSlot(1,pl_RNA);
  log("A random slot in list Nr 1 is:",vl_idx);

  vl_idx := f_EPTF_RNA_getRndSlot(0,pl_RNA);
  log("A random slot in list Nr 0 is:",vl_idx);
  vl_idx := f_EPTF_RNA_getList(5,pl_RNA);
  log("Value 5 is in list Nr:",vl_idx);

  vl_result := f_EPTF_RNA_moveSlot(5,1,pl_RNA);
  if(not(vl_result)) {setverdict(fail);}  
  vl_result := f_EPTF_RNA_moveSlot(6,1,pl_RNA);
  if(not(vl_result)) {setverdict(fail);} 
  vl_result := f_EPTF_RNA_moveSlot(5,0,pl_RNA);  
  if(not(vl_result)) {setverdict(fail);} 
  log("The RNA after the modifications: ",pl_RNA); 
  
  vl_length := f_EPTF_RNA_getLengthOfList(0,pl_RNA);
  log("The length of list Nr. 0: ",vl_length);    
  if(vl_length == -1 ) {setverdict(fail);}


  f_EPTF_RNA_initRFBA(10,pl_RFBA);
  log("The RFBA before the modifications: ",pl_RFBA);


  vl_idx := f_EPTF_RNA_getRndFreeSlot(pl_RFBA);
  log("A random slot in the freeList is:",vl_idx);
  vl_idx := f_EPTF_RNA_getList(5,pl_RFBA);
  log("Value 5 is in list Nr:",vl_idx);

  vl_result := f_EPTF_RNA_moveFromFreeToBusySlot(5,pl_RFBA);
  if(not(vl_result)) {setverdict(fail);}  
  vl_result := f_EPTF_RNA_moveFromFreeToBusySlot(6,pl_RFBA);
  if(not(vl_result)) {setverdict(fail);}  
  vl_result := f_EPTF_RNA_moveFromBusyToFreeSlot(5,pl_RFBA);  
  if(not(vl_result)) {setverdict(fail);}
  vl_result := f_EPTF_RNA_moveFromFreeToBusySlot(9,pl_RFBA);
  if(not(vl_result)) {setverdict(fail);}    
  log("The RFBA after the modifications: ",pl_RFBA);

  vl_result := f_EPTF_RNA_createFreeSlots(10,pl_RFBA);
  if(not(vl_result)) {setverdict(fail);}  
  log("The RFBA after the modifications: ",pl_RFBA);

}


control{
  execute(tc_RNA_Demo());
} 

}  // end of module
