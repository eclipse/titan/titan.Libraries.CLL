function CViewModel_PictureLoader(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var base = new CViewModel_FileSelector(p_viewmodel, p_options);
    
    var text = "";
    
    base.filesSelected = function() {
        var selection = base.getSelection();
        if (selection.length == 1) {
            text = '<div></div><img src="' + selection[0].value + '">';
        }
    };
    
    base.fileSelectionCanceled = function() {};
    
    base.getHtml = function() {
        return text;
    }
    
    return base;
}

CViewModel_PictureLoader.getHelp = function() {
    return "A small viewmodel that can be used to load a picture file and get an img html element that points to it using the getHtml function.";
};

CViewModel_PictureLoader.providesInterface = function() {
    var interfaceList = CViewModel_FileSelector.providesInterface();
    interfaceList.push("getHtml");
    return interfaceList;
};

CViewModel_PictureLoader.getCustomDataSchema = function() {
    return CViewModel_FileSelector.getCustomDataSchema();
}

//# sourceURL=CustomizableContent\CustomizableApp\ViewModels\ViewModel_PictureLoader.js