///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBaseDemo_SiblingFsmFunctions
// 
//  Purpose:
//    This module provides functions for demonstrate the communication between
//    FSM tables
// 
//  Module Parameters:
//    -
// 
//  Current Owner:
//    ELSZSKU
// 
//  Last Review Date:
//    2010-10-26
// 
//  Detailed Comments:
///////////////////////////////////////////////////////////
module EPTF_LGenBaseDemo_SiblingFsmFunctions {

import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from EPTF_CLL_Base_Functions all;

import from EPTF_LGenBaseDemo_SiblingFsmDefinitions all;

function f_EPTF_LGenBaseDemo_step_startingSubFsm1(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("Starting SubFSM1.");
}

function f_EPTF_LGenBaseDemo_step_startingSubFsm2(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("Starting SubFSM2.");
}

function f_EPTF_LGenBaseDemo_step_subFsm1Finished(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("SubFSM1 Finished.");
}

function f_EPTF_LGenBaseDemo_step_subFsm2Finished(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("SubFSM2 Finished.");
}

const charstring c_EPTF_LGenBaseDemo_stepName_startingSubFsm1 := "startingSubFsm1";
const charstring c_EPTF_LGenBaseDemo_stepName_startingSubFsm2 := "startingSubFsm2";
const charstring c_EPTF_LGenBaseDemo_stepName_subFsm1Finished := "subFsm1Finished";
const charstring c_EPTF_LGenBaseDemo_stepName_subFsm2Finished := "subFsm2Finished";

const EPTF_LGenBase_FsmTableDeclarator c_LGenBaseDemo_MainFSM :=
{
  name := "MainFSM",
  fsmParams := {
    {stateList := {"idle"}}
  },
  table := {
    extendedTable := {
      {
        //The test scenario starts to the startTC event of the LGenBase
        events2Listen := {events := {
            { singleEvent := {
                c_EPTF_LGenBase_behavior,
                c_EPTF_LGenBase_inputName_testMgmt_startTC,
                fsm
              }}}},
        cellRow := { statedCellRow := {
            {
              inState := {state := "idle"},
              cell :=
              {{
                  //Log
                  {
                    c_EPTF_LGenBaseDemo_stepName_startingSubFsm1,
                    {stepContextArgs := {}}
                  },
                  //The MainFSm dispatches an event to the FSM
                  //having the sibling name "SubFSM1" in the traffic case
                  {
                    c_EPTF_LGenBase_stepName_dispatchEventToSibling,
                    {eventToSibling := {
                        siblingName := "SubFSM1",
                        behaviorName := "SubFSM",
                        inputName := "start",
                        argVarName := omit
                      }}}
                }, omit, omit}
            }
          }
        }
      },
      {
        //Waits for the finish of the first sub-FSM
        events2Listen := {siblingEvents := {
            singleEventFromSibling := {
              siblingName := "SubFSM1",
              iName := "finished",
              eventType := fsm}}},
        cellRow := { statedCellRow := {
            {
              inState := {state := "idle"},
              cell :=
              {{
                  //Log
                  {
                    c_EPTF_LGenBaseDemo_stepName_subFsm1Finished,
                    {stepContextArgs := {}}
                  },
                  {c_EPTF_LGenBaseDemo_stepName_startingSubFsm2,{stepContextArgs := {}}},
                  //Starts the second sub-FSM
                  {
                    c_EPTF_LGenBase_stepName_dispatchEventToSibling,
                    {eventToSibling := {
                        siblingName := "SubFSM2",
                        behaviorName := "SubFSM",
                        inputName := "start",
                        argVarName := omit
                      }}}
                }, omit, omit}
            }
          }
        }
      },
      {
        //Waits for the finish of the second sub-FSM
        events2Listen := {siblingEvents := {
            singleEventFromSibling := {
              siblingName := "SubFSM2",
              iName := "finished",
              eventType := fsm}}},
        cellRow := { statedCellRow := {
            {
              inState := {state := "idle"},
              cell :=
              {{
                  {c_EPTF_LGenBaseDemo_stepName_subFsm2Finished,{stepContextArgs := {}}}
                }, omit, omit}
            }
          }
        }
      }
    }
  }
}

const EPTF_LGenBase_FsmTableDeclarator c_LGenBaseDemo_SubFSM :=
{
  name := "SubFSM",
  fsmParams := {
    {stateList := {"idle"}},
    {timerList := {{"subTimer",0.6}}},
    {declareEvents := { 
        useDefaultEvents := false, 
        eventList := {"start", "finished"}} 
    }
  },
  table := {
    extendedTable := {
      {
        events2Listen := {events := {{ singleEvent := {"SubFSM","start",fsm}}}},
        cellRow := { statedCellRow := {
            {
              inState := {state := "idle"},
              cell :=
              {{
                  {c_EPTF_LGenBase_stepName_pushEventToStack, omit},
                  {c_EPTF_LGenBase_stepName_timerStart,{timerName := "subTimer"}}
                }, omit, omit}
            }
          }
        }
      },
      {
        events2Listen := {events := {{ singleEvent := {c_EPTF_LGenBase_specialBName_timerTimeout,"subTimer",fsm}}}},
        cellRow := { statedCellRow := {
            {
              inState := {state := "idle"},
              cell :=
              {{
                  {c_EPTF_LGenBase_stepName_dispatchReplyOwnEventToTopEventInStack,{eventOfFsm := {"finished",omit}}},
                  {c_EPTF_LGenBase_stepName_popEventFromStack, omit}
                }, omit, omit}
            }
          }
        }
      }
    }
  }
}

function f_LGenBaseDemo_triggerStartTcForFsm(
  in integer pl_eIdx,
  in integer pl_fCtxIdx)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  var EPTF_LGenBase_ReportedEventDescriptor vl_trigger := c_EPTF_LGenBase_emptyReportedEventDescriptor;
  vl_trigger.event.bIdx := c_EPTF_LGenBase_bIdx;
  vl_trigger.event.iIdx := c_EPTF_LGenBase_inputIdx_testMgmt_startTC;
  vl_trigger.event.target.eIdx := pl_eIdx;
  vl_trigger.event.target.fsmCtxIdx := pl_fCtxIdx;
  f_EPTF_LGenBase_dispatchEvent(vl_trigger);
}

testcase tc_LGenBaseDemo_siblingFsm()
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  f_EPTF_LGenBase_init("siblingFsmDemo");
  
  v_demoBehavior := f_EPTF_LGenBase_declareBehaviorType(c_demoBehaviorName, -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareFsmEvent(c_demoBehaviorName,"triggerSubFSM");
  
  v_dummyInt := f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_startingSubFsm1, refers(f_EPTF_LGenBaseDemo_step_startingSubFsm1)});
  v_dummyInt := f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_startingSubFsm2, refers(f_EPTF_LGenBaseDemo_step_startingSubFsm2)});
  v_dummyInt := f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_subFsm1Finished, refers(f_EPTF_LGenBaseDemo_step_subFsm1Finished)});
  v_dummyInt := f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_subFsm2Finished, refers(f_EPTF_LGenBaseDemo_step_subFsm2Finished)});
  
  v_dummyInt := f_EPTF_LGenBase_declareFSMTable(c_LGenBaseDemo_SubFSM);
  v_dummyInt := f_EPTF_LGenBase_declareFSMTable(c_LGenBaseDemo_MainFSM);
  
  v_dummyInt := f_EPTF_LGenBase_declareEntityType("et1", {c_demoBehaviorName});
  v_dummyInt := f_EPTF_LGenBase_createEntityGroup({"eg1", "et1", 3})
  v_dummyInt := f_EPTF_LGenBase_declareScenarioType3(
    {"SC1",
      {
        {"TC1",{
            {target := {cpsToReach := 0.0}},
            {siblingFsmList :=
              {
                {
                  siblingName := "MainFSM",
                  fsmType := "MainFSM"
                },{
                  siblingName := "SubFSM1",
                  fsmType := "SubFSM"
                },{
                  siblingName := "SubFSM2",
                  fsmType := "SubFSM"
                }
              }
            }
          }
        }
      },
      {}
    });
  f_EPTF_LGenBase_createScenario2EntityGroup({"eg1","SC1"},false);
  
  f_LGenBaseDemo_triggerStartTcForFsm(1, f_EPTF_LGenBase_fsmCtxIdxOfSibling(1, f_EPTF_LGenBase_trafficCaseId("eg1","SC1","TC1"), 0));
  
  timer t_wait := 2.5;
  t_wait.start;
  t_wait.timeout;
  
  f_EPTF_Base_stop(pass);
}

function f_LGenBaseDemo_declareFsmOfTc1(
  in charstring pl_name)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  var EPTF_LGenBase_FsmTableDeclarator vl_fsm :=
  {
    name := pl_name,
    fsmParams := {
      {stateList := {"idle"}}
    },
    table := {
      extendedTable := {
        {
          events2Listen := {events := {{ singleEvent := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_startTC,fsm}}}},
          cellRow := { statedCellRow := {
              {
                inState := {state := "idle"},
                cell :=
                {{
                    {c_EPTF_LGenBaseDemo_stepName_dispatchingEventToTc2,omit},
                    {c_EPTF_LGenBase_stepName_dispatchEventToTC,{eventToTC := {"TC2",c_demoBehaviorName,"eventToTC2", omit}}}
                  }, omit, omit}
              }
            }
          }
        },
        {
          events2Listen := {events := {{singleEvent := {c_demoBehaviorName,"fsmOfTC2finished",fsm}}}},
          cellRow := { statedCellRow := {
              {
                inState := {state := "idle"},
                cell :=
                {{
                    {c_EPTF_LGenBaseDemo_stepName_fsmOfTc2Finished,omit}
                  }, omit, omit}
              }
            }
          }
        }
      }
    }
  }
  v_dummyInt := f_EPTF_LGenBase_declareFSMTable(vl_fsm);
}

function f_LGenBaseDemo_declareFsmOfTc2(
  in charstring pl_name)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  var EPTF_LGenBase_FsmTableDeclarator vl_fsm :=
  {
    name := pl_name,
    fsmParams := {
      {stateList := {"idle"}},
      {timerList := {{"subTimer",0.6}}},
      {declareEvents := {false, {"finished"}} }
    },
    table := {
      extendedTable := {
        {
          events2Listen := {events := {{ singleEvent := {c_demoBehaviorName,"eventToTC2",fsm}}}},
          cellRow := { statedCellRow := {
              {
                inState := {state := "idle"},
                cell :=
                {{
                    {c_EPTF_LGenBase_stepName_pushEventToStack, omit},
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "subTimer"}}
                  }, omit, omit}
              }
            }
          }
        },
        {
          events2Listen := {events := {{ singleEvent := {c_EPTF_LGenBase_specialBName_timerTimeout,"subTimer",fsm}}}},
          cellRow := { statedCellRow := {
              {
                inState := {state := "idle"},
                cell :=
                {{
                    {c_EPTF_LGenBase_stepName_dispatchReplyToTopEventInStack,{replyEvent := {c_demoBehaviorName,"fsmOfTC2finished",omit}}}
                  }, omit, omit}
              }
            }
          }
        }
      }
    }
  }
  v_dummyInt := f_EPTF_LGenBase_declareFSMTable(vl_fsm);
}

function f_LGenBaseDemo_eventToTcInit(in charstring pl_selfName)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  f_EPTF_LGenBase_init(pl_selfName);
  
  v_demoBehavior := f_EPTF_LGenBase_declareBehaviorType(c_demoBehaviorName, -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareFsmEvent(c_demoBehaviorName,"eventToTC2");
  v_dummyInt := f_EPTF_LGenBase_declareFsmEvent(c_demoBehaviorName,"fsmOfTC2finished");
  
  f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_dispatchingEventToTc2, refers(f_EPTF_LGenBaseDemo_step_dispatchingEventToTc2)});
  f_EPTF_LGenBase_declareStep(c_demoBehaviorName, {c_EPTF_LGenBaseDemo_stepName_fsmOfTc2Finished, refers(f_EPTF_LGenBaseDemo_step_fsmOfTc2Finished)});
  
  f_LGenBaseDemo_declareFsmOfTc1("FSM1");
  f_LGenBaseDemo_declareFsmOfTc2("FSM2");
  
  v_dummyInt := f_EPTF_LGenBase_declareEntityType("et1", {c_demoBehaviorName});
  v_dummyInt := f_EPTF_LGenBase_createEntityGroup({"eg1", "et1", 3})
  v_dummyInt := f_EPTF_LGenBase_declareScenarioType3(
    {"SC1",
      {
        {"TC1",{
            {target := {cpsToReach := 0.0}},
            {fsmList :=
              {"FSM1"}
            }
          }
        },
        {"TC2",{
            {target := {cpsToReach := 0.0}},
            {fsmList :=
              {"FSM2"}
            }
          }
        }
      },
      {}
    });
  f_EPTF_LGenBase_createScenario2EntityGroup({"eg1","SC1"},false);
}

function f_EPTF_LGenBaseDemo_step_dispatchingEventToTc2(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("Starting FSM of TC2.");
}

function f_EPTF_LGenBaseDemo_step_fsmOfTc2Finished(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBaseDemo_SiblingFsm_CT {
  action("FSM of TC2 finished.");
}

const charstring c_EPTF_LGenBaseDemo_stepName_dispatchingEventToTc2 := "dispatchingEventToTc2";
const charstring c_EPTF_LGenBaseDemo_stepName_fsmOfTc2Finished := "fsmOfTc2Finished";

testcase tc_LGenBaseDemo_eventToTc()
runs on EPTF_LGenBaseDemo_SiblingFsm_CT
{
  f_LGenBaseDemo_eventToTcInit("siblingFsmDemo");
  
  f_LGenBaseDemo_triggerStartTcForFsm(1, 0);
  
  timer t_wait := 2.5;
  t_wait.start;
  t_wait.timeout;
  
  f_EPTF_Base_stop(pass);
}


} // end of module
