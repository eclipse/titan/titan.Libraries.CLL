///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBaseDemo_TransportDefinitions
//
//  Purpose:
//    This module contains data structures of the transport layer of the demo components
//
//  Module Parameters:
//    -
//
//  Module depends on:
//
//  Current Owner:
//    ELSZSKU
//
//  Last Review Date:
//    20 - -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
module EPTF_LGenBaseDemo_TransportDefinitions
{
import from EPTF_CLL_Base_Definitions all

type record LGenBaseDemo_Register{
  charstring msg
}
const LGenBaseDemo_Register c_LGenBaseDemo_emptyRegister := {""}

type record LGenBaseDemo_RegisterResp{
  boolean resp,
  integer msgId,
  charstring msgUserData
}
const LGenBaseDemo_RegisterResp c_LGenBaseDemo_emptyRegisterResp := {false, -1, ""}

type record LGenBaseDemo_ReRegister{
  charstring msg
}
const LGenBaseDemo_ReRegister c_LGenBaseDemo_emptyReRegister := {""}

type record LGenBaseDemo_ReRegisterResp{
  boolean resp,
  integer msgId,
  charstring msgUserData
}
const LGenBaseDemo_ReRegisterResp c_LGenBaseDemo_emptyReRegisterResp := {false, -1, ""}

type record LGenBaseDemo_DoMsg{
  charstring msg
}
const LGenBaseDemo_DoMsg c_LGenBaseDemo_emptyDoMsg := {""}

type record LGenBaseDemo_DoMsgResp{
  boolean resp,
  integer msgId,
  charstring msgUserData
}
const LGenBaseDemo_DoMsgResp c_LGenBaseDemo_emptyDoMsgResp := {false, -1, ""}

type record LGenBaseDemo_MsgAck{
  integer msgId,
  boolean resp
}
const LGenBaseDemo_MsgAck c_LGenBaseDemo_emptyMsgAck := {-1, false}

type union LGenBaseDemo_Msg{
  LGenBaseDemo_Register register,
  LGenBaseDemo_RegisterResp registerResp,
  LGenBaseDemo_ReRegister reRegister,
  LGenBaseDemo_ReRegisterResp reRegisterResp,
  LGenBaseDemo_DoMsg doMsg,
  LGenBaseDemo_DoMsgResp doMsgResp,
  LGenBaseDemo_MsgAck msgAck
}

type port LGenBaseDemo_PT message {
  inout LGenBaseDemo_Msg;
} with {extension "internal"}

type record LGenBaseDemo_Transport_MessageData{
  integer entityIdx,
  integer fsmCtxIdx
}

const LGenBaseDemo_Transport_MessageData c_LGenBaseDemo_Transport_emptyMessageData := {
  -1,-1
}

type component LGenBaseDemo_Transport_CT extends EPTF_Base_CT{
  private port LGenBaseDemo_PT LGenBaseDemoIf
  private var LGenBaseDemo_Transport_CT v_LGenBaseDemo_Transport_connectedTo := null
  private var LGenBaseDemo_Transport_msgReceived_FT v_LGenBaseDemo_Transport_receiveCallbackFn := null
  private var default v_LGenBaseDemo_Transport_default := null
  private var LGenBaseDemo_Msg v_LGenBaseDemo_Transport_lastMsg
  //private var integer v_LGenBaseDemo_Transport_myBehavId := -1
  private var integer v_LGenBaseDemo_Transport_responderTcIdx := -1
}

type enumerated LGenBaseDemo_Transport_transportType {caller, responder}

type function LGenBaseDemo_Transport_msgReceived_FT(in LGenBaseDemo_Msg pl_msg)
runs on self

//const charstring c_LGenBaseDemo_Transport_BehaviorName := "Common"

}  // end of module
