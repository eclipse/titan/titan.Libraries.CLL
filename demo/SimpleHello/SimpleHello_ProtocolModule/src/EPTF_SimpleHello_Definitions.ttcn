///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_SimpleHello_Definitions
// 
//  Purpose:
//    This module provides common data types for SimpleHello protocol.
// 
//  Module depends on:
//    -
// 
//  Current Owner:
//    Tamas Buti (ETAMBUT)
// 
//  Last Review Date:
//    2007-11-28 (egbozie)
//
//  Detailed Comments:
//    Provide definitions to SimpleHello protocol.
//    Defines types of request and response with automatically generated
//    encoder/decoder functions by TITAN.
//
//    SimpleHello client sends a PDU_SimpleHello_Request to a SimpleHello server.
//    The server sends a PDU_SimpleHello_Response to the specified SimpleHello client.
//    The server determines its answer from the header of the request.
//    The transaction ID, sender ID, data and optional payload is echoed back to the client.
//
///////////////////////////////////////////////////////////////
module EPTF_SimpleHello_Definitions
{

///////////////////////////////////////////////////////////
// Group: EPTF_SimpleHello_message
//
// Description:
//   Types for SimpleHello messages
//
// Detailed Comments:
//   There are two types of SimpleHello messages:
//
//     - <PDU_SimpleHello_Request> : request sent by a client.
//      The header part of the request (SimpleHello_header) tells
//      the server how to process the message.
//
//     - <PDU_SimpleHello_Response> : response sent by the server.
//     The transaction ID, sender ID, data and optional payload is
//     echoed back to the client.
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_message
{
  ///////////////////////////////////////////////////////////
  // Type: SimpleHello_header
  //
  // Purpose:
  //   The header of the request determines the response of the server.
  //
  // Elements:
  //   lossP - *float* - the probability of simulated message lost
  //
  //   nackP - *float* - the probability of negative acknowledgement
  //
  //   delay - *float* - the interval of simulated processing delay
  //
  ///////////////////////////////////////////////////////////
  type record SimpleHello_header
  {
    float  lossP,
    float  nackP,
    float  delay
  }
  
  ///////////////////////////////////////////////////////////
  // Type: PDU_SimpleHello_Request
  //
  // Purpose:
  //   Request sent by a client to the server.
  //
  // Elements:
  //   header - <SimpleHello_header> - information of request processing
  //
  //   transId - *integer* -  transaction ID of request
  //
  //   senderId - *integer* - sender ID of request
  //
  //   msg - *charstring* - message data as charstring
  //
  //   payload - *octetstring* - message payload as octetstring (optional)
  //
  ///////////////////////////////////////////////////////////
  type record PDU_SimpleHello_Request
  {
    SimpleHello_header      header,
    integer        transId,
    integer        senderId,
    charstring     msg,
    octetstring    payload optional
  } with {
    variant (transId) "FIELDLENGTH(32)";
    variant (senderId) "FIELDLENGTH(32)"
  }
  
  ///////////////////////////////////////////////////////////
  // Constant: c_SimpleHello_SenderIdOffset4Request
  //
  // Purpose:
  //   Named constant for referencing offset of the sender 
  //   ID in SimpleHello requests
  //
  ///////////////////////////////////////////////////////////
  const integer  c_SimpleHello_SenderIdOffset4Request := 28;
  
  ///////////////////////////////////////////////////////////
  // Type: PDU_SimpleHello_Response
  //
  // Purpose:
  //   Response sent by the server to the client.
  //
  // Elements:
  //   transId - *integer* -  transaction ID of request
  //
  //   senderId - *integer* - sender ID of request
  //
  //   resultCode - <SimpleHello_resultCode> - the result code of processing
  //
  //   msg - *charstring* - message data as charstring
  //
  //   payload - *octetstring* - message payload as octetstring (optional)
  //
  ///////////////////////////////////////////////////////////
  type record PDU_SimpleHello_Response
  {
    integer                 transId,
    integer                 senderId,
    SimpleHello_resultCode  resultCode,
    charstring              msg,
    octetstring             payload optional
  } with {
    variant (transId) "FIELDLENGTH(32)";
    variant (senderId) "FIELDLENGTH(32)"
  }
  
  ///////////////////////////////////////////////////////////
  // Constant: c_SimpleHello_SenderIdOffset4Response
  //
  // Purpose:
  //   Named constant for referencing offset of the sender 
  //   ID in SimpleHello responses
  //
  ///////////////////////////////////////////////////////////
  const integer   c_SimpleHello_SenderIdOffset4Response := 4;
  
  ///////////////////////////////////////////////////////////
  // Type: SimpleHello_resultCode
  //
  // Purpose:
  //   Provides the result of message processing by the server.
  //
  // Elements:
  //   ACK - sign a positiv answer
  //
  //   NACK - sign a negativ answer
  //
  //   TIMEOUT - sign a timout event
  //
  ///////////////////////////////////////////////////////////
  type enumerated SimpleHello_resultCode { ACK(0), NACK(1), TIMEOUT(2) };

} with { encode "RAW"}


///////////////////////////////////////////////////////////
// Group: EPTF_SimpleHello_EncDec
//
// Description:
//   Encoder/decoder functions for automatic generation by TITAN.
//
// Detailed Comments:
//   TITAN generates encoder/decoder functions for simple types
//   automatically.
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_EncDec
{
  /////////////////////////////////////////////////////////
  // Function: f_enc_SimpleHello_Request
  //
  // Purpose:
  //  Function to encode SimpleHello Request.
  //
  // Parameters:
  //   pdu - *in* <PDU_SimpleHello_Request> - the SimpleHello Request to encode
  //
  // Return Value:
  //   *octetstring*
  //
  ///////////////////////////////////////////////////////////
  external function f_enc_SimpleHello_Request(in PDU_SimpleHello_Request pdu) return octetstring
  with { extension "prototype(convert) encode(RAW)"}
  
  ///////////////////////////////////////////////////////////
  // Function: f_dec_SimpleHello_Request
  //
  // Purpose:
  //  Function to decode SimpleHello Request.
  //
  // Parameters:
  //   str - *in* *octetstring* - the octetstring stream to decode
  //
  // Return Value:
  //   <PDU_SimpleHello_Request>
  //
  ///////////////////////////////////////////////////////////
  external function f_dec_SimpleHello_Request(in octetstring str) return PDU_SimpleHello_Request
  with { extension "prototype(convert) decode(RAW)"}
  
  ///////////////////////////////////////////////////////////
  // Function: f_enc_SimpleHello_Response
  //
  // Purpose:
  //  Function to encode SimpleHello Response.
  //
  // Parameters:
  //   pdu - *in* <PDU_SimpleHello_Response> - the SimpleHello Response to encode
  //
  // Return Value:
  //   *octetstring*
  //
  ///////////////////////////////////////////////////////////
  external function f_enc_SimpleHello_Response(in PDU_SimpleHello_Response pdu) return octetstring
  with { extension "prototype(convert) encode(RAW)"}
  
  ///////////////////////////////////////////////////////////
  // Function: f_dec_SimpleHello_Response
  //
  // Purpose:
  //  Function to decode SimpleHello Response.
  //
  // Parameters:
  //   str - *in* *octetstring* - the octetstring stream to decode
  //
  // Return Value:
  //   <PDU_SimpleHello_Response>
  //
  ///////////////////////////////////////////////////////////
  external function f_dec_SimpleHello_Response(in octetstring str) return PDU_SimpleHello_Response
  with { extension "prototype(convert) decode(RAW)"}

}

} //module
