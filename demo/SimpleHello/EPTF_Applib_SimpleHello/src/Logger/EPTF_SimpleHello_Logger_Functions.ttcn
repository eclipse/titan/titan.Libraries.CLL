///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_SimpleHello_Logger_Functions
//
// Purpose:
//   This module contains the functions for EPTF_SimpleHello_Logger_CT component
//
// Module Parameters:
//     See <EPTF_SimpleHello_Logger_Definitions>
//
//  Module depends on:
//    <EPTF_SimpleHello_Logger_Definitions>
//    <EPTF_SimpleHello_Definitions>
// 
// Current Owner:
//    Attila Fulop(EFLOATT), Bence Molnar(EBENMOL)
// 
// Last Review Date:
//    2009-04-03
//
// Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_SimpleHello_Logger_Functions {

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_SimpleHello_Logger_Definitions all;
import from EPTF_SimpleHello_Definitions all;

///////////////////////////////////////////////////////////
//  Group: LoggerClient
// 
//  Purpose:
//    Functions of the LoggerClient component
//
///////////////////////////////////////////////////////////
group LoggerClient  {
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_SimpleHello_LoggerClient_init_CT
  // 
  //  Purpose:
  //    Function to init the LoggerClient component
  //
  //  Parameters:
  //    pl_functionRef - *in* - <f_EPTF_SimpleHello_LoggerClient_FT> - The send function of the logging
  //
  //  Return Value:
  //    - integer - if the component initialize already the return value will be -1
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////  
  function f_EPTF_SimpleHello_LoggerClient_init_CT(
    in f_EPTF_SimpleHello_LoggerClient_FT  pl_functionRef) 
  runs on EPTF_SimpleHello_LoggerClient_CT 
  return integer 
  {
    if (v_EPTF_SimpleHello_LoggerClient_initialized) {   
      return -1;
    }
    v_EPTF_SimpleHello_LoggerClient_initialized := true;
  
    vf_EPTF_SimpleHello_LoggerClient_sendFunction := pl_functionRef;
    vc_EPTF_SimpleHello_LoggerClient_loggerComponent := EPTF_SimpleHello_Logger_CT.create;
    connect(vc_EPTF_SimpleHello_LoggerClient_loggerComponent:SimpleHello_Logger_PCO,self:SimpleHello_LoggerClient_PCO);
    vc_EPTF_SimpleHello_LoggerClient_loggerComponent.start(f_EPTF_SimpleHello_Logger_Behavior());  
  
    return 1;
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_SimpleHello_LoggerClient_cleanup_CT
  // 
  //  Purpose:
  //    Function to clean the LoggerClient component
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////  
  function f_EPTF_SimpleHello_LoggerClient_cleanup_CT() 
  runs on EPTF_SimpleHello_LoggerClient_CT {

    if (not v_EPTF_SimpleHello_LoggerClient_initialized) { return }
    v_EPTF_SimpleHello_LoggerClient_initialized := false;
     
    vc_EPTF_SimpleHello_LoggerClient_loggerComponent.stop;
    disconnect(vc_EPTF_SimpleHello_LoggerClient_loggerComponent:SimpleHello_Logger_PCO,self:SimpleHello_LoggerClient_PCO);  
  }
  
} // group LoggerClient

///////////////////////////////////////////////////////////
//  Group: Logger
// 
//  Purpose:
//    Functions of the Logger component
//
///////////////////////////////////////////////////////////
group Logger  {

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_SimpleHello_Logger_Behavior
  // 
  //  Purpose:
  //    The main function of the logger component
  // 
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  // 
  //  Errors:
  //    -
  // 
  ///////////////////////////////////////////////////////////
  function f_EPTF_SimpleHello_Logger_Behavior() 
  runs on EPTF_SimpleHello_Logger_CT
  {
    var PDU_SimpleHello_Request vl_EPTF_SimpleHello_Logger_Request;
    var PDU_SimpleHello_Response vl_EPTF_SimpleHello_Logger_Response;
  
    alt {
      [] SimpleHello_Logger_PCO.receive(EPTF_SimpleHello_LoggerMessage:?) -> value v_EPTF_SimpleHello_Logger_Msg 
         {
	   if(tsp_EPTF_SimpleHello_Logging_Decode) {
             if (v_EPTF_SimpleHello_Logger_Msg.isRequest)
             {
               vl_EPTF_SimpleHello_Logger_Request := f_dec_SimpleHello_Request(v_EPTF_SimpleHello_Logger_Msg.octetMessage);
               log("The received Message is: ",vl_EPTF_SimpleHello_Logger_Request);
	     
	     } else {	     
               vl_EPTF_SimpleHello_Logger_Response := f_dec_SimpleHello_Response(v_EPTF_SimpleHello_Logger_Msg.octetMessage);
               log("The received Message is: ",vl_EPTF_SimpleHello_Logger_Response);

	     }
	   } else {
	     log("The received octetstring Message is: ",v_EPTF_SimpleHello_Logger_Msg.octetMessage);
	   }
           repeat;	  
        }
     }
  }

} // group Logger
} // end of module
