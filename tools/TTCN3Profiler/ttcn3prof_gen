#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
#Generates profiler info to TTCN3Profiler.log
#and prints out only the overall information.
if [ "$*" ]
then
echo "ttcn3prof_gen version 1.0"
echo "Report generator for TTCN3Profiler"
echo "Usage: Run the script without arguments in the bin directoy."
echo ""
echo "Prints out short summary of the TTCN3 profiler information."
echo "Generates detailed profiler info to TTCN3Profiler.log"
echo "Detailed report on unused functions is written to TTCN3Profiler_unused.log"
echo ""
echo "All profiler information produced by the executable test suite are"
echo "merged into TTCN3Profiler.prof"
exit
fi

TTCN3PROF_FNS=""
if [ -r "TTCN3Profiler.fns" ]
then
TTCN3PROF_FNS="TTCN3Profiler.fns"
fi

#if [ "$(ls *.prof 2> /dev/null)" == "" ]
#then
#touch xx.prof
#echo "###### TTCN3Profiler is reset #######"
#sleep 1
#exit
#fi

echo "Merging profiler results started at $(date)"
TTCN3PROF_INPUTFILES=$(echo *.cov *.prof | xargs ls 2> /dev/null)
TTCN3PROF_COVFILES=$(echo *.cov | xargs ls 2> /dev/null)

TTCN3PROF_OUTPUTFILE="/dev/null"
if [ "${TTCN3PROF_INPUTFILES}" ]
then
TTCN3PROF_OUTPUTFILE="TTCN3Profiler.log"
fi

#echo "###### TTCN3Profiler Summary #######"

: | echo ${TTCN3PROF_FNS} ${TTCN3PROF_INPUTFILES} | xargs $(dirname $0)/ttcn3prof > tmp.prof
: | $(dirname $0)/ttcn3prof ${TTCN3PROF_FNS} ${TTCN3PROF_COVFILES} tmp.prof | tee ${TTCN3PROF_OUTPUTFILE} | grep "coverage\|All calls\|All function calls\|Overall"

# remove all input files and use the generated log to store data:
rm tmp.prof
if [ "${TTCN3PROF_INPUTFILES}" ]
then
echo *.prof | xargs rm 2> /dev/null;cp TTCN3Profiler.log TTCN3Profiler.prof
fi

if [ "${TTCN3PROF_INPUTFILES}" ]
then
TTCN3PROF_OUTPUTFILE="TTCN3Profiler_unused.log"
fi

:|$(dirname $0)/ttcn3prof_unusedFns | tee ${TTCN3PROF_OUTPUTFILE} | grep "Functions not used"

echo "Detailed TTCN3 profiler info is available in TTCN3Profiler.log and TTCN3Profiler_unused.log"
echo "Merging profiler results finished at $(date)"
#echo "###################################"
